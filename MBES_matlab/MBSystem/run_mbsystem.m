% Apply tide compensation for 2013_45 data; work file by file to reduce space problems.

% Data location
%cd ../../work/

filelist = dir('*.all');

for ii = 1:length(filelist)
  % Parse filename
  [dummy, filename, extension] = fileparts(filelist(ii).name);
  
%   % First command in MB System sequence
%   cmd = ['ls -1 ' filelist(ii).name ' > templist56'];
%   system(cmd);
% 
%   % Second command in MB System sequence
%   cmd = 'mbdatalist -F-1 -I templist56 > datalist56-1';
%   system(cmd);
% 
%   % Third command in MB System sequence
%   cmd = 'mbm_copy -F57 -I datalist56-1';
%   system(cmd);
% 
%   % Fourth command in MB System sequence
%   cmd = ['ls -1 ' filename '.mb57 > templist57'];
%   system(cmd);
% 
%   % Fifth command in MB System sequence
%   cmd = 'mbdatalist -F-1 -I templist57 > datalist57-1';
%   system(cmd);
% 
%   % Sixth command in MB System sequence
%   cmd = 'mbset -F-1 -I datalist57-1 -PTide_NES_22052018_mbsystemFormat.txt -PTIDEFORMAT:2';
%   system(cmd);
% 
%   % Seventh command in MB System sequence
%   cmd = ['ls -1 ' filename '.mb57 > datalist.mb-1'];
%   system(cmd)
%   
%   % Eighth command in MB System sequence
%   cmd = 'mbprocess -F-1 -I datalist.mb-1';
%   system(cmd);

  % Ninth command in MB System sequence
  cmd = ['mblist -I ' filename 'p.mb57 -MA -ONXYZBGgcH.sj.d >' filelist(ii).name '.out'];
  system(cmd);

%   % Tenth command
%   cmd = ['rm *mb57*'];
%   system(cmd);
% 
%   % Eleventh command
%   cmd = ['rm *list*'];
%   system(cmd);

end
%cd ../matlab/2_mbsystem
