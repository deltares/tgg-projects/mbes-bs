%% Script to convert MB-system output to ASCII data
close all;
clear all;
clc;

addpath ../matlabFun/generallib
tic
% Access folder
datadir = 'D:\karaouli\Desktop\Projects\backscattered\DataAmeland2017\all_files\add\';
% Define data input
filelist = dir([datadir '*.out']);
% Run for-loop
DATA =[];
for ii = 1:length(filelist)
  DATA = load([datadir filelist(ii).name]);
  save('-V7.3', [datadir filelist(ii).name '.mat'], 'DATA'); % Variable name 'data' is crucial for further processing
  disp(['Converted ' filelist(ii).name])
  delete ([datadir filelist(ii).name])
end


toc