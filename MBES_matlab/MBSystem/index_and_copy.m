% In this script MBES data recorded in 2013, 2014, and 2015 are indexed,
% and copied to the work directory.
close all; clear all; clc;


addpath ../matlabFun/generallib
addpath ../matlabFun/mbeslib

% Week 45 in 2013
datadir = 'D:\karaouli\Desktop\Projects\backscattered\DataAmeland2017\all_files\add\';
outdir = 'D:\karaouli\Desktop\Projects\backscattered\DataAmeland2017\all_files\add\';
% if(~isunix)
%     inds = find(datadir == '/');
%     datadir(inds) = '\';
%     inds = find(outdir == '/');
%     outdir(inds) = '\';
% end
% if(~exist(outdir))
%     mkdir% 
(outdir)
% end
tic
filelist = dir([datadir '*.all']);

% Scan files and make indices (skip if index files already exist)
for ii =1:length(filelist)
  [a,b,c] = fileparts([datadir filelist(ii).name]);
  if(~exist(fullfile(a,[b '_idx.mat'])))
    disp(['Scanning ' datadir filelist(ii).name]);
    scanallfile(fullfile(a,[b c]),'l');
  else
    disp(['Found index file of ' datadir filelist(ii).name]);
  end
end
toc
% Copy files to the work directory using OS command line calls. Make
% necessary / or \ adjustments.
% for ii = 1:1%3:length(filelist)
%     [a,b,c] = fileparts([datadir filelist(ii).name]);
%     if(isunix)
%         %system(['cp ' a '/' b c ' ' outdir b c]);
%         system(['cp ' a '/' b '_idx' '.mat' ' ' outdir b '_idx' '.mat']);
%     else
%         %system(['copy ' a '\' b c ' ' outdir b c]);
%         system(['copy ' a '\' b '_idx' '.mat' ' ' outdir b '_idx' '.mat']);
%     end
% end


