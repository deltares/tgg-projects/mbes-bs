function datagram = datagram49h(datafile,offset,endianess);

fid = fopen(datafile,'r',endianess);

fseek(fid,offset,0);

% Get start identifier (always 02h).
datagram.STX = fread(fid,1,'uint8');

% Read type of datagram. Should be 44.
datagram.Type = fread(fid,1,'uint8');

% Relevant identifiers (review this), readable as number.
identsn = ['WLZ' 'SMH' 'HUN' 'HUT' ...
           'S1Z' 'S1X' 'S1Y' 'S1H' 'S1R' 'S1P' ... 
           'S2Z' 'S2Y' 'S2Y' 'S2H' 'S2R' 'S2P' ...
           'S1S' 'S2S' 'GO1' 'GO2' 'OBO' 'FGD' ...
           'DSX' 'DSY' 'DSZ' 'DSO' 'DSF' ...
           'APS' 'P1M' 'P1T' 'P1Z' 'P1X' 'P1Y' 'P1D' ...
           'P2M' 'P2T' 'P2Z' 'P2X' 'P2Y' 'P2D' ...
           'P3M' 'P3T' 'P3Z' 'P3X' 'P3Y' 'P3D' 'P3S' ...
           'MSZ' 'MSX' 'MSY' 'MSR' 'MSP' 'MSG' ...
           'NSZ' 'NSX' 'NSY' 'NSR' 'NSP' 'NSG' ...
           'GCG' 'MAS' 'SHC' 'AHS' 'ARO' 'API' 'AHE' 'CLS' 'CLO' ...
           'VSN' 'VSU' 'VSE'];
          
% Relevant identifiers, readable as switches.
identss = ['DSH' 'MRP' 'NRP' 'CPR'];
   
% Relevant identifiers, readable as ascii.
identsa = ['S1N' 'S2N' 'TSV' 'RSV' 'BSV' 'PSV' ...
           'DDS' 'OSV' 'DSV' 'DSD' 'P1G' 'P2G' 'P3G' ...
           'MSD' 'NSD' 'VSI' 'VSM' 'MCA' 'MCU' 'MCI' 'MCP' ...
           'CPR' 'ROP' 'SID' 'PLL' 'COM'];

% Read EM model number.
datagram.Model = fread(fid,1,'uint16');

% Read Date.
datum = num2str(fread(fid,1,'uint32'),'%8d');

datagram.Jday = date2jday(str2num(datum(7:8)),str2num(datum(5:6)),str2num(datum(1:4)));
datagram.Year = str2num(datum(1:4));

% Read Time.
datagram.Time_ms = fread(fid,1,'uint32');
datagram.Time = datagram.Time_ms / (1000 * 60 * 60);

% Read Survey Line number.
datagram.SurveyLine = fread(fid,1,'uint16');

% Read system serial number.
datagram.SN = fread(fid,1,'uint16');

% Read serial number of second sonar head.
datagram.SN2 = fread(fid,1,'uint16');

% Now some evaluating voodoo!
while(1)
  e_str = [];
  e_char = fread(fid,1,'char');
  if(e_char == 3)
    break
  end
  while(e_char ~= 44)
    e_str = [e_str e_char];
    e_char = fread(fid,1,'char');
  end

  try
      if(~isempty(findstr(identsn,e_str(1:3))) & (mod(findstr(identsn,e_str(1:3))-1,3)==0) )
        eval(['datagram.' char(e_str) ';']);
      elseif(~isempty(findstr(identss,e_str(1:3))) & (mod(findstr(identss,e_str(1:3))-1,3)==0) )
        eval(['datagram.' char(e_str(1:4)) '''' char(e_str(5:end)) '''' ';']);
      elseif(~isempty(findstr(identsa,e_str(1:3))) & (mod(findstr(identsa,e_str(1:3))-1,3)==0) )
        eval(['datagram.' char(e_str(1:4)) '''' char(e_str(5:end)) '''' ';']);
      end
  catch
      break
  end

end
%keyboard

fclose(fid);
