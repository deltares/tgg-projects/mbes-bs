function datagram = datagram58h(datafile,offset,endianess);

fid = fopen(datafile,'r',endianess);

fseek(fid,offset,0);

% Get start identifier (always 02h).
datagram.STX = fread(fid,1,'uint8');

% Read type of datagram. Should be 58h.
datagram.Type = fread(fid,1,'uint8');

% Read EM model number.
datagram.Model = fread(fid,1,'uint16');

% Read Date.
datum = num2str(fread(fid,1,'uint32'),'%8d');

datagram.Jday = date2jday(str2num(datum(7:8)),str2num(datum(5:6)),str2num(datum(1:4)));
datagram.Year = str2num(datum(1:4));

% Read Time.
datagram.Time_ms = fread(fid,1,'uint32');
datagram.Time = datagram.Time_ms / (1000 * 60 * 60);

% Read Ping counter.
datagram.Counter = fread(fid,1,'uint16');

% Read system serial number.
datagram.SN = fread(fid,1,'uint16');

% Read vessel heading in degrees.
datagram.Hdg = fread(fid,1,'uint16')/100;

% Read sound speed at transducer in m/s.
datagram.CsT = fread(fid,1,'uint16')/10;

% Transmit transducer depth re water level in ??.
datagram.TD = fread(fid,1,'float32');

% Read no. of beams in datagram
datagram.NB = fread(fid,1,'uint16');

% Read number of valid detections.
datagram.N = fread(fid,1,'uint16');

% Read sampling frequency in Hz.
datagram.Zres = fread(fid,1,'float32');

% Spare.
fread(fid,1,'int32');

% Read NB entries of:
for counter = 1:datagram.NB
    % Depth (z) from transducer.
    datagram.beam(counter).Z = (fread(fid,1,'float32'));
    % Acrosstrack distance (y).
    datagram.beam(counter).Y = (fread(fid,1,'float32'));
    % Alongtrack distance (x).
    datagram.beam(counter).X = (fread(fid,1,'float32'));
    % Length of detection window (samples).
    datagram.beam(counter).WL = (fread(fid,1,'uint16'));
    % Quality factor.
    datagram.beam(counter).Qf = (fread(fid,1,'uint8'));
    % Beam incidence angle adjustment in degrees.
    datagram.beam(counter).AA = (fread(fid,1,'int8')/10);
    % Detection information.
    datagram.beam(counter).DI = (fread(fid,1,'uint8'));
    % Real time cleaning information.
    datagram.beam(counter).DA = (fread(fid,1,'int8'));
    % Reflectivity (BS).
    datagram.beam(counter).R = (fread(fid,1,'int16')/2);
end

% Spare.
fread(fid,1,'uint8');

% Last byte. Always 03h.
datagram.ETX = fread(fid,1,'uint8');

fclose(fid);
