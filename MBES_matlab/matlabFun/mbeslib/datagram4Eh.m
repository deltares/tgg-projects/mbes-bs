function datagram = datagram4Eh(datafile,offset,endianess)

fid = fopen(datafile,'r',endianess);

fseek(fid,offset,0);

% Get start identifier (always 02h).
datagram.STX = fread(fid,1,'uint8');

% Read type of datagram. Should be 4Eh.
datagram.Type = fread(fid,1,'uint8');

% Read EM model number.
datagram.Model = fread(fid,1,'uint16');

% Read Date.
%datum = num2str(fread(fid,1,'uint32'));
datum = num2str(fread(fid,1,'uint32'),'%8d');

datagram.Jday = date2jday(str2num(datum(7:8)),str2num(datum(5:6)),str2num(datum(1:4)));
datagram.Year = str2num(datum(1:4));

% Read Time.
datagram.Time_ms = fread(fid,1,'uint32');
datagram.Time = datagram.Time_ms / (1000 * 60 * 60);

% Read Ping counter.
datagram.Counter = fread(fid,1,'uint16');

% Read system serial number.
datagram.SN = fread(fid,1,'uint16');

% Read sound speed in 0.1 m/s
datagram.C = fread(fid,1,'uint16');

% Read number of transmit sectors
datagram.Ntx = fread(fid,1,'uint16');

% Read number of receive beams in datagram.
datagram.Nrx = fread(fid,1,'uint16');

% Read number of valid detections
datagram.Nv = fread(fid,1,'uint16');

% Read sampling frequency.
datagram.F = fread(fid,1,'float32');

% Read doppler scaling.
datagram.Dscale = fread(fid,1,'uint32');

% Read Ntx entries of:
for counter = 1:datagram.Ntx
    % Tilt angle in 0.01 deg
    datagram.tx(counter).TA = fread(fid,1,'int16');
    % Focus range in 0.1 m
    datagram.tx(counter).FR = fread(fid,1,'uint16');
    % Signal length in s.
    datagram.tx(counter).SL = fread(fid,1,'float32');
    % Sector Transmit delay re. 1rst TX pulse in s.
    datagram.tx(counter).TTD = fread(fid,1,'float32');
    % Center frequency in Hz
    datagram.tx(counter).CF = fread(fid,1,'float32');
    % Mean absorption coeff. in .01 dB/km.
    datagram.tx(counter).AC = fread(fid,1,'uint16');
    % Signal waveform identifier
    datagram.tx(counter).WF = fread(fid,1,'uint8');
    % Transmit sector number
    datagram.tx(counter).TS = fread(fid,1,'uint8');
    % Bandwidth in Hz
    datagram.tx(counter).BW = fread(fid,1,'float32');
end

% Read Nrx entries of:
for counter = 1:datagram.Nrx
    % Beam pointing angle re RX array in 0.01 degrees
    datagram.rx(counter).BA = fread(fid,1,'int16');
    % Transmit sector number
    datagram.rx(counter).TSN = fread(fid,1,'uint8');
    % Detection info
    datagram.rx(counter).DI = fread(fid,1,'uint8');
    % Detection window length in samples
    datagram.rx(counter).WL = fread(fid,1,'uint16');
    % Quality factor
    datagram.rx(counter).Q = fread(fid,1,'uint8');
    % Doppler correction.
    datagram.rx(counter).DC = fread(fid,1,'int8');
    % Two way travel time in s.
    datagram.rx(counter).TT = fread(fid,1,'float32');
    % Reflectivity (BS) in 0.1 dB
    datagram.rx(counter).R = fread(fid,1,'int16');
    % Real time cleaning info.
    datagram.rx(counter).CI = fread(fid,1,'int8');
    % spare
    fread(fid,1,'uint8');
end    
    
% Read last byte?
datagram.ETX = fread(fid,1,'uint8');

if datagram.ETX == 0
    datagram.ETX = fread(fid,1,'uint8');
end
ftell(fid)
fclose(fid);
