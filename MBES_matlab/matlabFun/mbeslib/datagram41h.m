function datagram = datagram41h(datafile,offset,endianess);

% fid = fopen(datafile,'r','b');
fid = fopen(datafile,'r',endianess);

fseek(fid,offset,0);

% Get start identifier (always 02h).
datagram.STX = fread(fid,1,'uint8');

% Read type of datagram. Should be 41.
datagram.Type = fread(fid,1,'uint8');

% Read EM model number.
datagram.Model = fread(fid,1,'uint16');

% Read Date.
datum = num2str(fread(fid,1,'uint32'),'%8d');
datagram.Jday = date2jday(str2num(datum(7:8)),str2num(datum(5:6)),str2num(datum(1:4)));
datagram.Year = str2num(datum(1:4));

% Read Time.
datagram.Time_ms = fread(fid,1,'uint32');
datagram.Time = datagram.Time_ms / (1000 * 60 * 60);

% Read Attitude counter.
datagram.Counter = fread(fid,1,'uint16');

% Read system serial number.
datagram.SN = fread(fid,1,'uint16');

% Read number of entries.
datagram.N = fread(fid,1,'uint16');

% Read N entries of:
for counter = 1:datagram.N
    % Time since record start (ms).
    datagram.attitude(counter).T = (fread(fid,1,'uint16'));
    % Sensor status.
    datagram.attitude(counter).Status = (fread(fid,1,'uint16'));
    % Roll in 0.01 degrees.
    datagram.attitude(counter).Roll = (fread(fid,1,'int16'));
    % Pitch in 0.01 degrees.
    datagram.attitude(counter).Pitch = (fread(fid,1,'int16'));
    % Heave in cm.
    datagram.attitude(counter).Heave = (fread(fid,1,'int16'));
    % Heading in 0.01 degrees.
    datagram.attitude(counter).Heading = (fread(fid,1,'uint16'));
end

% Skip a byte.
dummy = fread(fid,1,'int8');

% Last byte. Always 03h.
datagram.ETX = fread(fid,1,'uint8');

fclose(fid);

% end of program
