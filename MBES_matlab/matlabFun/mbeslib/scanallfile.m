function res = scanallfile(filename,endianess)

fileinfo = dir(filename);
fid = fopen(filename, 'r', endianess);

counter = 0;	% counts datagrams
dsize = 1;	% indicates presence of 'length' field (4 bytes)

while (ftell(fid) < (fileinfo.bytes - 2))
  datagramsize = fread(fid,dsize,'uint32');	% Four bytes, but not included in size

  counter = counter + 1;
    
  datagramlocation(counter) = ftell(fid);

  startidentifier = fread(fid,1,'int8');	% One byte
  
  dtype(counter) = fread(fid,1,'int8');	% One byte
  
  fseek(fid,10,0);				% Ten bytes

  pingcounter(counter) = fread(fid,1,'uint16');% Two bytes
  
  fseek(fid,datagramsize-(1+1+10+2),0);
%keyboard
end
    
fclose(fid);

[a,b,c] = fileparts(filename);

save('-V6',fullfile(a,[b '_idx.mat']),'datagramlocation','dtype','pingcounter');
