function datagram = datagram46h(datafile,offset,endianess);

fid = fopen(datafile,'r',endianess);
% fid = fopen('0022_-_tno__5160_-_0004_(MBE_LOGGER).raw','r','b');
% fid = fopen('0026_-_tno__4680_-_0003_(MBE_LOGGER).raw','r','b');

fseek(fid,offset,0);

% Get start identifier (always 02h).
datagram.STX = fread(fid,1,'uint8');

% Read type of datagram.
datagram.Type = fread(fid,1,'uint8');

% Read EM model number.
datagram.Model = fread(fid,1,'uint16');

% Read Date.
%datagram.Date = fread(fid,1,'uint32');
% Read Date.
datum = num2str(fread(fid,1,'uint32'),'%8d');

datagram.Jday = date2jday(str2num(datum(7:8)),str2num(datum(5:6)),str2num(datum(1:4)));
datagram.Year = str2num(datum(1:4));

% Read Time.
datagram.Time_ms = fread(fid,1,'uint32');
datagram.Time = datagram.Time_ms / (1000 * 60 * 60);

% Read Ping counter.
datagram.Counter = fread(fid,1,'uint16');

% Read system serial number.
datagram.SN = fread(fid,1,'uint16');

% Read maximum number of beams
datagram.NB = fread(fid,1,'uint8');

% Read number of valid beams.
datagram.N = fread(fid,1,'uint8');

% Read sound speed.
datagram.Cs = fread(fid,1,'uint16')/10;

for counter = 1:datagram.N
    % Read beam pointing angle.
    datagram.beam(counter).BA = fread(fid,1,'int16')/100;
    
    % Read transmit tilt angle.
    datagram.beam(counter).TA = fread(fid,1,'uint16')/100;

    % Read two-way range.
    datagram.beam(counter).T = fread(fid,1,'uint16');
    
    % Read reflectivity (BS).
    datagram.beam(counter).R = fread(fid,1,'int8')/2;
    
    % Read beam number.
    datagram.beam(counter).BN = fread(fid,1,'uint8');

end
 
% Read last byte.
datagram.Spare = fread(fid,1,'uint8');

datagram.ETX = fread(fid,1,'uint8');

fclose(fid);
