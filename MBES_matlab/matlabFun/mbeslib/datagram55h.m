function datagram = datagram55h(datafile,offset,endianess)

fid = fopen(datafile,'r',endianess);

fseek(fid,offset,0);

% Get start identifier (always 02h).
datagram.STX = fread(fid,1,'uint8');

% Read type of datagram.
datagram.Type = fread(fid,1,'uint8');

% Read EM model number.
datagram.Model = fread(fid,1,'uint16');

% Read Date.
%datum = num2str(fread(fid,1,'uint32'));
datum = num2str(fread(fid,1,'uint32'),'%8d');

datagram.Jday = date2jday(str2num(datum(7:8)),str2num(datum(5:6)),str2num(datum(1:4)));
datagram.Year = str2num(datum(1:4));

% Read Time.
datagram.Time_ms = fread(fid,1,'uint32');
datagram.Time = datagram.Time_ms / (1000 * 60 * 60);

% Read Ping counter.
datagram.Counter = fread(fid,1,'uint16');

% Read system serial number.
datagram.SN = fread(fid,1,'uint16');

% Read date of profile
datagram.dp = fread(fid,1,'uint32');

% Read time of profile
datagram.tp = fread(fid,1,'uint32');

% Read number of entries
datagram.N = fread(fid,1,'uint16');

% Read depth resolution in cm
datagram.dres = fread(fid,1,'uint16');

% Read N entries of:
for counter = 1:datagram.N
    % Tilt angle in 0.01 deg
    datagram.C(counter).Z = fread(fid,1,'int32');
    % Focus range in 0.1 m
    datagram.C(counter).c = fread(fid,1,'uint32');
end

% Read last byte?
datagram.ETX = fread(fid,1,'uint8');

if datagram.ETX == 0
    datagram.ETX = fread(fid,1,'uint8');
end
%ftell(fid)
fclose(fid);
