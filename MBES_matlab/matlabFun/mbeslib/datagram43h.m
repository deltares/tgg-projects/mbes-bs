function datagram = datagram43h(datafile,offset,endianess);

fid = fopen(datafile,'r',endianess);
% fid = fopen('0022_-_tno__5160_-_0004_(MBE_LOGGER).raw','r','b');
% fid = fopen('0026_-_tno__4680_-_0003_(MBE_LOGGER).raw','r','b');

fseek(fid,offset,0);

% Get start identifier (always 02h).
datagram.STX = fread(fid,1,'uint8');

% Read type of datagram.
datagram.Type = fread(fid,1,'uint8');

% Read EM model number.
datagram.Model = fread(fid,1,'uint16');

% Read Date.
datagram.Date = fread(fid,1,'uint32');

% Read Time.
datagram.Time_ms = fread(fid,1,'uint32');
datagram.Time = datagram.Time_ms / (1000 * 60 * 60);

% Read clock counter.
datagram.ClockCounter = fread(fid,1,'uint16');

% Read system serial number.
datagram.SN = fread(fid,1,'uint16');

% Read external Date.
datagram.ExtDate = fread(fid,1,'uint32');

% Read external Time.
datagram.ExtTime = (fread(fid,1,'uint32') / (1000 * 60 * 60));

% Read 1 PPS active
datagram.PPS = fread(fid,1,'uint8');

datagram.ETX = fread(fid,1,'uint8');

fclose(fid);
