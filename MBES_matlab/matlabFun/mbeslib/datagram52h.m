function datagram = datagram52h(datafile,offset,endianess)

fid = fopen(datafile,'r',endianess);

fseek(fid,offset,0);

% Get start identifier (always 02h).
datagram.STX = fread(fid,1,'uint8');

% Read type of datagram.
datagram.Type = fread(fid,1,'uint8');

% Read EM model number.
datagram.Model = fread(fid,1,'uint16');

% Read Date.
%datum = num2str(fread(fid,1,'uint32'));
datum = num2str(fread(fid,1,'uint32'),'%8d');

datagram.Jday = date2jday(str2num(datum(7:8)),str2num(datum(5:6)),str2num(datum(1:4)));
datagram.Year = str2num(datum(1:4));

% Read Time.
datagram.Time_ms = fread(fid,1,'uint32');
datagram.Time = datagram.Time_ms / (1000 * 60 * 60);

% Read Ping counter.
datagram.Counter = fread(fid,1,'uint16');

% Read system serial number.
datagram.SN = fread(fid,1,'uint16');

% Read Operator Station status.
datagram.OS = fread(fid,1,'uint8');

% Read Processing Unit status.
datagram.PU = fread(fid,1,'uint8');

% Read BSP status.
datagram.BSP = fread(fid,1,'uint8');

% Read Sonar Head or Transceiver status.
datagram.SHT = fread(fid,1,'uint8');

% Read Mode.
datagram.Mode = fread(fid,1,'uint8');

% Read Filter identifier.
datagram.Filter = fread(fid,1,'uint8');

% Read Minimum depth in m.
datagram.MinDepth = fread(fid,1,'uint16');

% Read Maximum depth in m.
datagram.MaxDepth = fread(fid,1,'uint16');

% Read Absorption Coefficient.
datagram.AbsCoef = fread(fid,1,'uint16') / 100;

% Read Transmit Pulse Length in us.
datagram.T = fread(fid,1,'uint16');

% Read Transmit beamwidth in degrees.
datagram.TxBW = fread(fid,1,'uint16')/10;

% Read Transmit power re maximum in dB.
datagram.TxP = fread(fid,1,'int8');

% Read Receive beamwidth in degrees.
datagram.RxBeamW = fread(fid,1,'uint8')/10;

% Read Receive bandwidth in Hz.
datagram.RxBandW = fread(fid,1,'uint8')*50;

% Read Receiver "setting".
datagram.RxS = fread(fid,1,'uint8');

% Read TVG law crossover angle in degrees.
datagram.TVG = fread(fid,1,'uint8');

% Read Source of sound speed at transducer.
datagram.csource = fread(fid,1,'uint8');

% Read Maximum port swath width in m.
datagram.PortSwathWidthMeters = fread(fid,1,'uint16');

% Read Beam spacing.
datagram.BeamSpacing = fread(fid,1,'uint8');

% Read Maximum port coverage in degrees.
datagram.PortCoverageDegrees = fread(fid,1,'uint8');

% Read Yaw and pitch stabilization mode.
datagram.YawPitchStabMode = fread(fid,1,'uint8');

% Read Maximum starboard coverage in degrees.
datagram.StarboardCoverageDegrees = fread(fid,1,'uint8');

% Read Maximum starboard swath width in m.
datagram.StarboardSwathWidthMeters = fread(fid,1,'uint16');

% Read Transmit along tilt value in degrees.
datagram.TransmitTiltDegrees = fread(fid,1,'int16')/10;

% Read Filter identifier 2 -- detect mode
datagram.DetectMode = fread(fid,1,'uint8');

% Read last byte?
datagram.ETX = fread(fid,1,'uint8');

if datagram.ETX == 0
    datagram.ETX = fread(fid,1,'uint8');
end
%ftell(fid)
fclose(fid);
