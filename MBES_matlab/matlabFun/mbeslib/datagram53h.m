function datagram = datagram53h(datafile,offset,endianess);

fid = fopen(datafile,'r',endianess);

fseek(fid,offset,0);

% Get start identifier (always 02h).
datagram.STX = fread(fid,1,'uint8');

% Read type of datagram.
datagram.Type = fread(fid,1,'uint8');

% Read EM model number.
datagram.Model = fread(fid,1,'uint16');

% Read Date.
%datum = num2str(fread(fid,1,'uint32'));
datum = num2str(fread(fid,1,'uint32'),'%8d');

datagram.Jday = date2jday(str2num(datum(7:8)),str2num(datum(5:6)),str2num(datum(1:4)));
datagram.Year = str2num(datum(1:4));

% Read Time.
datagram.Time_ms = fread(fid,1,'uint32');
datagram.Time = datagram.Time_ms / (1000 * 60 * 60);

% Read Ping counter.
datagram.Counter = fread(fid,1,'uint16');

% Read system serial number.
datagram.SN = fread(fid,1,'uint16');

% Read mean absorption coefficient in 0.01 dB/km
datagram.N1 = fread(fid,1,'uint16');

% Read pulse length in us.
datagram.T = fread(fid,1,'uint16');

% Read Range to normal incidence used to correct sample amplitudes
datagram.C1 = fread(fid,1,'uint16');

% Read start sample of TVG ramp.
datagram.TVG1 = fread(fid,1,'uint16');

% Read stop sample of TVG ramp.
datagram.TVG2 = fread(fid,1,'uint16');

% Read normal incidence BS in dB.
datagram.BSN = fread(fid,1,'int8');

% Read oblique BS in dB.
datagram.BSO = fread(fid,1,'int8');

% Read TX beamwidth in degrees.
datagram.TXBW = fread(fid,1,'uint16')/10;

% Read TVG cross over angle.
datagram.C2 = fread(fid,1,'uint8');

% Read number of valid beams.
datagram.N = fread(fid,1,'uint8');

% Read N entries of:
for counter = 1:datagram.N
    % Beam index number.
    datagram.beam(counter).BeamIndex = fread(fid,1,'uint8');
    % Sorting direction.
    datagram.beam(counter).SortingDirection = fread(fid,1,'int8');
    % Number of samples per beam.
    datagram.beam(counter).NS = fread(fid,1,'uint16');
    % Centre sample number
    datagram.beam(counter).CSN = fread(fid,1,'int16');
end
%keyboard
datagram.Amplitude = zeros(datagram.N,max([datagram.beam(1:datagram.N).NS]));
% Read time series for each beam.
for counter = 1:datagram.N
    datagram.Amplitude(counter,1:datagram.beam(counter).NS) = fread(fid,datagram.beam(counter).NS,'int8')'/2;
end
    
% Read last byte?
datagram.ETX = fread(fid,1,'uint8');

if datagram.ETX == 0
    datagram.ETX = fread(fid,1,'uint8');
end

fclose(fid);
