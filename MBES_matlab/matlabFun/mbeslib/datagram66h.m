function datagram = datagram66h(datafile,offset,endianess)

fid = fopen(datafile,'r',endianess);

fseek(fid,offset,0);

% Get start identifier (always 02h).
datagram.STX = fread(fid,1,'uint8');

% Read type of datagram.
datagram.Type = fread(fid,1,'uint8');

% Read EM model number.
datagram.Model = fread(fid,1,'uint16');

% Read Date.
%datum = num2str(fread(fid,1,'uint32'));
datum = num2str(fread(fid,1,'uint32'),'%8d');

datagram.Jday = date2jday(str2num(datum(7:8)),str2num(datum(5:6)),str2num(datum(1:4)));
datagram.Year = str2num(datum(1:4));

% Read Time.
datagram.Time_ms = fread(fid,1,'uint32');
datagram.Time = datagram.Time_ms / (1000 * 60 * 60);

% Read Ping counter.
datagram.Counter = fread(fid,1,'uint16');

% Read system serial number.
datagram.SN = fread(fid,1,'uint16');

% Read number of transmit sectors
datagram.Ntx = fread(fid,1,'uint16');

% Read number of valid receive beams
datagram.N = fread(fid,1,'uint16');

% Read sampling frequency in 0.01 Hz
datagram.F = fread(fid,1,'uint32');

% Read ROV depth in 0.01 m
datagram.ROVD = fread(fid,1,'int32');

% Read sound speed in 0.1 m/s
datagram.C = fread(fid,1,'uint16');

% Read maximum number of beams
datagram.Nmax = fread(fid,1,'uint16');

% Spare 1
fread(fid,1,'uint16');

% Spare 2
fread(fid,1,'uint16');

% Read Ntx entries of:
for counter = 1:datagram.Ntx
    % Tilt angle in 0.01 deg
    datagram.tx(counter).TA = fread(fid,1,'int16');
    % Focus range in 0.1 m
    datagram.tx(counter).FR = fread(fid,1,'uint16');
    % Signal length in us.
    datagram.tx(counter).SL = fread(fid,1,'uint32');
    % Transmit time offset in us
    datagram.tx(counter).TTO = fread(fid,1,'uint32');
    % Center frequency in Hz
    datagram.tx(counter).CF = fread(fid,1,'uint32');
    % Bandwidth in 10 Hz
    datagram.tx(counter).BW = fread(fid,1,'uint16');
    % Signal waveform identifier
    datagram.tx(counter).WF = fread(fid,1,'uint8');
    % Transmit sector numberdatagram.tx(counter).TTO = fread(fid,1,'uint32');
    datagram.tx(counter).TS = fread(fid,1,'uint8');
end

% Read N entries of:
for counter = 1:datagram.N
    % Beam pointing angle ref array in 0.01 degrees
    datagram.rx(counter).BA = fread(fid,1,'int16');
    % Range in 0.25 samples (R)
    datagram.rx(counter).R = fread(fid,1,'uint16');
    % Transmit sector number
    datagram.rx(counter).TSN = fread(fid,1,'uint8');
    % Reflectivity (BS) in dB
    datagram.rx(counter).BS = fread(fid,1,'int8')/2;
    % Quality factor
    datagram.rx(counter).Q = fread(fid,1,'uint8');
    % Detection window length in samples (/4 if phase)
    datagram.rx(counter).WL = fread(fid,1,'uint8');
    % Beam number
    datagram.rx(counter).BN = fread(fid,1,'int16');
    % spare
    fread(fid,1,'uint16');
end    
    
% Read last byte?
datagram.ETX = fread(fid,1,'uint8');

if datagram.ETX == 0
    datagram.ETX = fread(fid,1,'uint8');
end
%ftell(fid)
fclose(fid);
