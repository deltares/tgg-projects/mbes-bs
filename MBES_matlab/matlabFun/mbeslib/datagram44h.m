function datagram = datagram44h(datafile,offset,endianess);

fid = fopen(datafile,'r',endianess);

fseek(fid,offset,0);

% Get start identifier (always 02h).
datagram.STX = fread(fid,1,'uint8');

% Read type of datagram. Should be 44.
datagram.Type = fread(fid,1,'uint8');

% Read EM model number.
datagram.Model = fread(fid,1,'uint16');

% Read Date.
datum = num2str(fread(fid,1,'uint32'),'%8d');

datagram.Jday = date2jday(str2num(datum(7:8)),str2num(datum(5:6)),str2num(datum(1:4)));
datagram.Year = str2num(datum(1:4));

% Read Time.
datagram.Time_ms = fread(fid,1,'uint32');
datagram.Time = datagram.Time_ms / (1000 * 60 * 60);

% Read Ping counter.
datagram.Counter = fread(fid,1,'uint16');

% Read system serial number.
datagram.SN = fread(fid,1,'uint16');

% Read vessel heading in degrees.
datagram.Hdg = fread(fid,1,'uint16')/100;

% Read sound speed at transducer in m/s.
datagram.CsT = fread(fid,1,'uint16')/10;

% Transmit transducer depth re water level in m.
datagram.TD = fread(fid,1,'uint16')/100;

% Read max. no. of beams
datagram.NB = fread(fid,1,'uint8');

% Read number of valid beams.
datagram.N = fread(fid,1,'uint8');

% Read z resolution in m.
datagram.Zres = fread(fid,1,'uint8')/100;

% Read x and y resolution in m.
datagram.XYres = fread(fid,1,'uint8')/100;

% Read depth difference between sonar heads.
datagram.dD = fread(fid,1,'int16');

% Read N entries of:
for counter = 1:datagram.N
    % Depth (z).
    datagram.beam(counter).Z = (fread(fid,1,'int16'));
    % Acrosstrack distance (y).
    datagram.beam(counter).Y = (fread(fid,1,'int16'));
    % Alongtrack distance (x).
    datagram.beam(counter).X = (fread(fid,1,'int16'));
    % Beam depression angle in degrees.
    datagram.beam(counter).DA = (fread(fid,1,'int16')/100);
    % Beam azimuth angle in degrees.
    datagram.beam(counter).AA = (fread(fid,1,'uint16')/100);
    % Range (one-way travel time).
    datagram.beam(counter).T = (fread(fid,1,'uint16'));
    % Quality factor.
    datagram.beam(counter).Qf = (fread(fid,1,'uint8'));
    % Length of detection window (samples/4).
    datagram.beam(counter).WL = (fread(fid,1,'uint8'));
    % Reflectivity (BS).
    datagram.beam(counter).BS = (fread(fid,1,'int8')/2);
    % Beam number.
    datagram.beam(counter).BN = (fread(fid,1,'uint8'));
end

% Read transducer depth offset multiplier.
datagram.OffsetMultiplier = fread(fid,1,'int8');

% Last byte. Always 03h.
datagram.ETX = fread(fid,1,'uint8');

fclose(fid);
