function datagram = datagram59h(datafile,offset,endianess);

fid = fopen(datafile,'r',endianess);

fseek(fid,offset,0);

% Get start identifier (always 02h).
datagram.STX = fread(fid,1,'uint8');

% Read type of datagram.
datagram.Type = fread(fid,1,'uint8');

% Read EM model number.
datagram.Model = fread(fid,1,'uint16');

% Read Date.
%datum = num2str(fread(fid,1,'uint32'));
datum = num2str(fread(fid,1,'uint32'),'%8d');

datagram.Jday = date2jday(str2num(datum(7:8)),str2num(datum(5:6)),str2num(datum(1:4)));
datagram.Year = str2num(datum(1:4));

% Read Time.
datagram.Time_ms = fread(fid,1,'uint32');
datagram.Time = datagram.Time_ms / (1000 * 60 * 60);

% Read Ping counter.
datagram.Counter = fread(fid,1,'uint16');

% Read system serial number.
datagram.SN = fread(fid,1,'uint16');

% Read sampling frequency in Hz.
datagram.FS = fread(fid,1,'float32');

% Read range to normal incidence and used to correct sample amplitudes in no. of samples.
datagram.C1 = fread(fid,1,'uint16');

% Read normal incidence backscatter in 0.1 dB.
datagram.BSN = fread(fid,1,'int16');

% Read oblique BS in 0.1 dB.
datagram.BSO = fread(fid,1,'int16');

% Read TX beamwidth in degrees.
datagram.TXBW = fread(fid,1,'uint16')/10;

% Read TVG law crossover angle in 0.1 degrees.
datagram.TVG = fread(fid,1,'uint16');

% Read number of valid beams.
datagram.N = fread(fid,1,'uint16');

% Read N entries of:
for counter = 1:datagram.N
    % Sorting direction.
    datagram.beam(counter).SortingDirection = fread(fid,1,'int8');
    % Detection info.
    datagram.beam(counter).DI = fread(fid,1,'uint8');
    % Number of samples per beam.
    datagram.beam(counter).NS = fread(fid,1,'uint16');
    % Centre sample number
    datagram.beam(counter).CSN = fread(fid,1,'uint16');
end
%keyboard
datagram.Amplitude = zeros(datagram.N,max([datagram.beam(1:datagram.N).NS]));
% Read 'time series' for each beam in dB.
for counter = 1:datagram.N
    datagram.Amplitude(counter,1:datagram.beam(counter).NS) = fread(fid,datagram.beam(counter).NS,'int16')'/10;
end
    
% Read last byte?
datagram.ETX = fread(fid,1,'uint8');

if datagram.ETX == 0
    datagram.ETX = fread(fid,1,'uint8');
end

fclose(fid);
