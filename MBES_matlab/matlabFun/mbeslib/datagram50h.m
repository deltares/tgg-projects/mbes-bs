function datagram = datagram50h(datafile,offset,endianess);

% fid = fopen(datafile,'r','b');
fid = fopen(datafile,'r',endianess);

fseek(fid,offset,0);

% Get start identifier (always 02h).
datagram.STX = fread(fid,1,'uint8');

% Read type of datagram. Should be 50h.
datagram.Type = fread(fid,1,'uint8');

% Read EM model number.
datagram.Model = fread(fid,1,'uint16');

% Read Date.
datum = num2str(fread(fid,1,'uint32'),'%8d');
datagram.Jday = date2jday(str2num(datum(7:8)),str2num(datum(5:6)),str2num(datum(1:4)));
datagram.Year = str2num(datum(1:4));

% Read Time.
datagram.Time_ms = fread(fid,1,'uint32');
datagram.Time = datagram.Time_ms / (1000 * 60 * 60);

% Read postition counter.
datagram.Counter = fread(fid,1,'uint16');

% Read system serial number.
datagram.SN = fread(fid,1,'uint16');

% Read latitude in decimal degrees.
datagram.Lat = fread(fid,1,'int32')/20000000;

% Read longitude in decimal degrees.
datagram.Lon = fread(fid,1,'int32')/10000000;

% Read position quality in cm.
datagram.Q = fread(fid,1,'uint16');

% Read speed over ground in cm/s.
datagram.V = fread(fid,1,'uint16');

% Read course over ground in 0.01 degrees.
datagram.COG = fread(fid,1,'uint16');

% Read heading in 0.01 degrees.
datagram.Heading = fread(fid,1,'uint16');

% Read position system descriptor.
datagram.PSD = fread(fid,1,'uint8');

% Read number of bytes in input datagram.
N = fread(fid,1,'uint8');

% Read input datagram.
datagram.DataIn = fread(fid,N,'char');

% Adjust for odd length of datagram.
if(rem(N,2)==0)
  fread(fid,1,'uint8');
end

% Last byte. Always 03h.
datagram.ETX = fread(fid,1,'uint8');

fclose(fid);

% end of program
