%--------------------------------------------------------------------------
% function [s, faz] = wgs84dist(lat1, lon1, lat2, lon2)
%
% This code is part of the geodetic tool kit, which can be downloaded
% from the National Oceanic and Atmospheric Administration (NOAA)
% National Geodetic Survey (ngs) website at:
% http://www.ngs.noaa.gov/index.shtml.
%
% input description
%
% lat1 initial latitude (rad or deg)
% lon1 initial longitude (rad or deg)
% lat2 final latitude (rad or deg)
% lon2 final longitude (rad or deg)
%
% output description
%
% s surface distance along ellipsoid (m)
% faz forward azimuth (rad or deg)
%--------------------------------------------------------------------------

%--------------------------------------------------------------------------
% Original source code comments:
%
% Solution of the geodetic direct problem after T.Vincenty
% modified Rainsford's method with Helmert's elliptical terms
% effective in any azimuth and at any distance short of antipodal
% standpoint/forepoint must not be the geographic pole
%
% a is the semi-major axis of the reference ellipsoid
% f is the flattening of the reference ellipsoid
% latitudes and longitudes in radians positive north and east
% forward azimuths at both points returned in radians from north
%
% programmed for cdc-6600 by LCDR L.Pfeifer NGS Rockville MD 18FEB75
% modified for system 360 by John G Gergen NGS Rockville MD 7507
%--------------------------------------------------------------------------

%--------------------------------------------------------------------------
% S.P. van IJsselmuide, January 2007
% Modified function so it can handle vectors/matrixes and fixed input to be
% in degrees.
%--------------------------------------------------------------------------

function [s_out, faz_out] = wgs84dist(lat1, lon1, lat2, lon2)
%-------------------------------------------------------------------------------
% Reshape inputs
%-------------------------------------------------------------------------------
keepsize    = size(lat1);
lat1        = lat1(:);
lon1        = lon1(:);
lat2        = lat2(:);
lon2        = lon2(:);

%------------------------------------------------------------------------------
% WGS-84 defining parameters.
%------------------------------------------------------------------------------
a           = 6378137.0;
f           = 1.0 / 298.257223563;

%------------------------------------------------------------------------------
% Miscellaneous parameters.
%------------------------------------------------------------------------------
rad2deg     = 180 / pi;
deg2rad     = 1.0 / rad2deg;

zero = 0.0; one = 1.0; two = 2.0;three = 3.0; four = 4.0; six = 6.0;
three_eighths = 3.0 / 8.0; sixteen = 16.0;

%------------------------------------------------------------------------------
% Input conversions.
%------------------------------------------------------------------------------
lat1        = lat1 * deg2rad;
lon1        = lon1 * deg2rad;
lat2        = lat2 * deg2rad;
lon2        = lon2 * deg2rad;

%------------------------------------------------------------------------------
% Find lat/lon pairs that are identical - remove them from the calculations.
% The surface distance, forward azimuth and backward azimuth for these points
% will be set to zero.
%------------------------------------------------------------------------------
equal       = ( abs(lat1 - lat2) < eps & abs(lon1 - lon2) < eps);

s_out       = zeros(size(lat1));
faz_out     = zeros(size(lat1));
baz_out     = zeros(size(lat1));

lat1        = lat1(~equal);
lon1        = lon1(~equal);
lat2        = lat2(~equal);
lon2        = lon2(~equal);

%------------------------------------------------------------------------------
% Main routine.
%------------------------------------------------------------------------------
eps0        = 0.5e-13;
r           = one - f;
tu1         = r * sin(lat1) ./ cos(lat1);
tu2         = r * sin(lat2) ./ cos(lat2);
cu1         = one ./ sqrt(tu1 .* tu1 + one);
su1         = cu1 .* tu1;
cu2         = one ./ sqrt(tu2 .* tu2 + one);
s           = cu1 .* cu2;
baz         = s .* tu2;
faz         = baz .* tu1;
x           = lon2 - lon1;

repeat      = 1;

while repeat == 1,
    sx      = sin(x);
    cx      = cos(x);
    tu1     = cu2 .* sx;
    tu2     = baz - su1 .* cu2 .* cx;
    sy      = sqrt(tu1 .* tu1 + tu2 .* tu2);
    cy      = s .* cx + faz;
    y       = atan2(sy, cy);
    sa      = s .* sx ./ sy;
    c2a     = -sa .* sa + one;
    cz      = faz + faz;

    ii      = c2a > 0;

    cz(ii)  = -cz(ii) ./ c2a(ii) + cy(ii);

    e       = cz .* cz * two - one;
    c       = ((-three * c2a + four) * f + four) .* c2a * f / sixteen;
    d       = x;
    x       = ((e .* cy .* c + cz) .* sy .* c + y) .* sa;
    x       = (one - c) .* x * f + lon2 - lon1;

    if all(abs(d-x) <= eps0),
        break
    end

end

faz         = atan2(tu1,tu2);
baz         = atan2(cu1 .* sx, baz .* cx - su1 .* cu2) + pi;
x           = sqrt((one / r / r - one) .* c2a + one) + one;
x           = (x - two) ./ x;
c           = one - x;
c           = (x .* x / four + one) ./ c;
d           = (three_eighths * x .* x - one) .* x;
x           = e .* cy;
s           = one - e - e;
s           = ((((sy .* sy * four - three) .* s .* cz .* (d / six) - x) .* ...
                (d / four) + cz) .* sy .* d + y) .* c * a * r;

%------------------------------------------------------------------------------
% Map to outputs.
%------------------------------------------------------------------------------
s_out(~equal)   = s;
faz_out(~equal) = faz;

%------------------------------------------------------------------------------
% Output conversions.
%------------------------------------------------------------------------------
faz_out     = faz_out * rad2deg;
faz_out     = mod(faz_out,360);
faz_out     = reshape(faz_out,keepsize);
s_out       = reshape(s_out,keepsize);

