%   LLC_LL_TO_RB   
%
%   Find relative (range,bearing) of two geographical positions.
%   [RANGE,BEARING] = LLC_LL_TO_RB(LAT1,LON1,COURSE1,LAT2,LON2) determines
%   the RANGE and BEARING of geographical position (LAT2,LON2) with respect to
%   geographical position (LAT1,LON1) and direction COURSE1. RANGE is
%   measured in nautical miles. BEARING is measured clockwise from COURSE1 and
%   in the interval [-180 180] degrees. LAT1 and LAT2 are the latitude in 
%   degrees North (in the interval [-90 90]), LON1 and LON2 are the 
%   longitudes in degrees East (in the interval [-180 180]).
%
%   The formulae are taken from 'Orbital Motion', A.E. Roy, 3rd 
%   edition, 1988.
%
%   Modified to use wgs84 earth.
%
%   Pascal de Theije, v1.0, 11 October 2000
%   Sander van IJsselmuide, v1.1 January 2007
%   Copyright (c) 2003, TNO-D&V
%   All Rights Reserved
%
%   See also LLC_RB_TO_LL

function [RANGE,BEARING] = llc_ll_to_rb(LAT1,LON1,COURSE1,LAT2,LON2)

% impove accuracy since distances can be small
LAT1 = double(LAT1);
LON1 = double(LON1);
LAT2 = double(LAT2);
LON2 = double(LON2);

% make sure sizes of LAT/LON1 and LAT/LON2 are the same
if ((numel(LAT1)==1)&&(numel(LAT2)>1))
   LAT1 = repmat(LAT1,size(LAT2)); 
   LON1 = repmat(LON1,size(LON2)); 
end

% compute range/bearing
[RANGE, AZIMUTH] = wgs84dist(LAT1, LON1, LAT2, LON2);
RANGE            = RANGE/1852;
BEARING          = AZIMUTH-COURSE1;
BEARING          = mod(BEARING-180,360) - 180;
