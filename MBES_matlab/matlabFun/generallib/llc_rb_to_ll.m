%   LLC_RB_TO_LL   
% 
%   Find geographical position from (range,bearing) to other
%   geographical position.
%   [LAT2,LON2] = LLC_RB_TO_LL(LAT1,LON1,COURSE1,RANGE,BEARING) finds the
%   geographical position (LAT2,LON2) that is separated from geographical
%   position (LAT1,LON1) by a distance RANGE (in nautical miles) and an angle
%   BEARING. BEARING is measured clockwise from COURSE and in the interval
%   [-180 180] degrees. LAT1 and LAT2 are the latitude in degrees North
%   (in the interval [-90 90]), LON1 and LON2 are the longitudes in degrees
%   East (in the interval [-180 180]).
%
%   The formulae are taken from 'Orbital Motion', A.E. Roy, 3rd
%   edition, 1988.
%
%   Modified to use wgs84 earth.
%
%   Pascal de Theije, v1.0, 26 November 1999
%   Sander van IJsselmuide, v1.1 January 2007
%   Copyright (c) 2006, TNO-D&V
%   All Rights Reserved
%
%   See also LLC_LL_TO_RB

function [LAT2,LON2] = llc_rb_to_ll(LAT1,LON1,COURSE1,RANGE,BEARING)

% impove accuracy since distances can be small
LAT1    = double(LAT1);
LON1    = double(LON1);
RANGE   = double(RANGE);
AZIMUTH = double(COURSE1+BEARING);
AZIMUTH = mod(AZIMUTH,360);

% make sure sizes of LAT/LON1 and RANGE/AZIMUTH are the same
if ((numel(LAT1)==1)&&(numel(RANGE)>1))
   LAT1 = repmat(LAT1,size(RANGE)); 
   LON1 = repmat(LON1,size(RANGE)); 
end

% compute new position
[LAT2,LON2] = wgs84invdist(LAT1, LON1, AZIMUTH, RANGE*1852);
    