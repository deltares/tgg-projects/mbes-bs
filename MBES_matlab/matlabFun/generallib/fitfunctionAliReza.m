function f = fitfunction(lam,Data)
%FITFUNCTION  Used to return errors in fitting data to a function.
%   FITFUNCTION(lam,Data,Plothandle) returns the 
%   error between the data and the values computed by the current
%   function of lam.  FITFUNCTION assumes a function of the form
%
%     y =  c(1)*exp(-lam(1,1)*(t - lam(1,2))^2) + ... + c(n)*exp(-lam(n,1)*(t - lam(n,2))^2)
%
%   with n linear parameters and 2n nonlinear parameters.
%
%   To solve for the linear parameters c, we build a matrix A
%   where the j-th column of A is exp(-((t - lam(j,2)).^2)/(2*lam(j,1)^2));
%   (t is a vector).
%   Then we solve A*c = y for the linear least-squares solution c.

global c

t = Data(:,1); y = Data(:,2);           % separate Data matrix into t and y
A = zeros(length(t),length(lam(:,1)));  % build A matrix
for j = 1:length(lam(:,1))
   A(:,j) = exp(-((t - lam(j,2)).^2)/(2*lam(j,1)^2));
end

% c = abs(A\y);                           % solve A*c = y for linear parameters c
% yhat = A*c;                           
% f = (yhat-y)./sqrt(y);
% c = inv(A'*A)*A'*y;                           
% yhat = A*c;                           
% ehat = (yhat-y);
% f= ehat./sqrt(y);

W = diag(1./(y));
A = sqrt(W)*A;
y = sqrt(W)*y;
c = nnls(A'*A, A'*y);                           
yhat = A*c;                           
ehat = yhat-y;
f = ehat;

return
% end of function





% Function for folving a non-negative least squares solution
function x = nnls(N,u)
mu0=-u; x0=zeros(length(u),1);
xtest=x0+1; x=x0;
while norm(x-xtest)>1e-18
    for k=1:length(u)
        x(k,1)=max(0,x0(k)-mu0(k)/N(k,k));
        mu0 = mu0+(x(k)-x0(k))*N(:,k);
    end
    xtest=x0; x0=x;
end
return
% end of function


