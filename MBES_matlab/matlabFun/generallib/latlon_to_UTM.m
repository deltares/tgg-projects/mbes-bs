function [N,E] = latlon_to_UTM(phi,labda)

% Program to convert WGS84 lat,lon coordinates to WGS84 UTM coordinates
% The equations are taken from publication by ir.
% G.L. Strang van Hees, "Globale en lokale geodetische systemen", 4th
% edition!
%
% SEE PDF DOCUMENT:  http://www.ncg.knaw.nl/Publicaties/Groen/pdf/30Strang.pdf
%

a = 6378137;         % page 42
e2 = 0.00669438000;  % page 42
k = 0.99960000;      % page 42

A = 100505250179e-11; % A = 1 + (3/4)*e2 + (45/64)*e2^2 + (175/256)*e2^3 + (11025/16384)*e2^4;      % page 42+43
B = 253155430e-11; % B = (3/8)*e2 + (15/32)*e2^2 + (525/1024)*e2^3 + (2205/4096)*e2^4;              % page 42+43 
C = 265689e-11; % C = (15/256)*e2^2 + (105/1024)*e2^3 + (2204/16384)*e2^4;                          % page 42+43
D = 347e-11; % D = (35/3072)*e2^3 + (105/4096)*e2^4;

x = a*(1-e2)*(A*phi*pi/180 - B*sin(2*phi*pi/180) + C*sin(4*phi*pi/180) - D*sin(6*phi*pi/180));

% central meridian
labda0 = fix(labda/6)*6 + 3;
%labda0 = 3;
d_labda = (labda-labda0).*cos(phi*pi/180)*pi/180;
t = tan(phi*pi/180);
n2 = e2*(cos(phi*pi/180)).^2/(1-e2);
R = a./sqrt(1-e2*(sin(phi*pi/180)).^2);

p = (1/12)*(5 - t.^2 + 9*n2 + 4*n2.^2);                             % page 43
q = (1/360)*(61 - 58*t.^2 + t.^4 + 270*n2 - 330*t.^2.*n2);          % page 43
r = (1/6)*(1 - t.^2 + n2);                                          % page 43
s = (1/120)*(5 - 18*t.^2 + t.^4 + 14*n2 - 58*t.^2.*n2);             % page 43
u = (1/5040)*(61 - 479*t.^2 + 179*t.^4 - t.^6);                     % page 43

d_x = 0.5*(d_labda).^2.*R.*t.*(1 + p.*d_labda.^2 + q.*d_labda.^4); % page 43  eq. 23
y = d_labda.*R.*(1 + r.*(d_labda).^2 + s.*d_labda.^4 + u.*d_labda.^6);  % page 43  eq. 23

N = k*(x + d_x);
E = k*y + 500000;

% end of program