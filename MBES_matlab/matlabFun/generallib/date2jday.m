function jday = date2jday(dy, mn, yr);

jdays = [0 31 59 90 120 151 181 212 243 273 304 334 365];
ljdays = [0 31 60 91 121 152 182 213 244 274 305 335 366];

if (nargin == 3)
    if (mod(yr,4)==0)
        if (mod(yr,100)==0)
            if(mod(yr,400)==0)
                days = ljdays;
            else
                days = jdays;
            end
        else
            days = ljdays;
        end
    else
        days = jdays;
    end
else
    days = jdays;
end

jday = days(mn) + dy;