% MEDIAN4, a moving median filter.
%
% USAGE: MEDIAN4(X,W) applies a MEDIAN operation along vector X
% within a moving window of W elements. The output is a vector
% of the same length as X. Note that the first and last
% FLOOR(W/2) values are NaNs.
%
% Keep an eye on memory usage!
%
% Play Zork!
%
% Created 2001-01-31 by Jeroen Janmaat.
% Updated 2001-02-07: preserve vector orientation.

function Y = median4(X,W)

% Test W.
if (mod(W,2))==0
   disp('Warning: windowsize should be an odd number.')
   Y='';
   return
end

% Determine vector orientation.
a = size(X);
if(a(2)==1)
   h = 0;
else
   h = 1;
end

% Prepare a vector with NaNs on both ends.
a_side = floor(W/2);
temp(1:a_side) = NaN;
temp(a_side+1:a_side+length(X)) = X;
temp(a_side+length(X)+1:a_side+length(X)+a_side) = NaN;

instructie='';

% Construct a string of shifted vectors.
for teller = 1:W
   instructie=[instructie 'temp(' num2str(teller) ':' num2str(length(X)+teller-1) ');'];
end

instructie = [ '[' instructie ']' ];

Y=median(eval(instructie),1);

% Restore vector orientation.
if(h==0)
   Y=Y';
end
