function r = jday2date(julday,yr);

% JDAY2DATE Convert Julian date to traditional date.
%    D = JDAY2DATE(JD,YR) converts the Julian day JD in year YR to month
%    D.MONTH and day D.DAY, with leap year taken into account. D.LEAPYEAR
%    will be set to 1 for a leap year and to 0 for non-leap years.
%
%    D = JDAY2DATE(JD) does the same, but it is assumed that the year is
%    not a leap year.
%
%    In this function, a day is assumed to begin at 00:00:00 hours. The
%    first Julian day coincides with January 1st.
%
%    (nc) J. Janmaat, TNO-FEL - no copyright in this case.
%    First version on 2004-10-08.

jdays = [0 31 59 90 120 151 181 212 243 273 304 334 365];
ljdays = [0 31 60 91 121 152 182 213 244 274 305 335 366];

if (nargin == 2)
    if (mod(yr,4)==0)
        if (mod(yr,100)==0)
            if(mod(yr,400)==0)
                days = ljdays;
                r.leapyear = 1;
            else
                days = jdays;
                r.leapyear = 0;
            end
        else
            days = ljdays;
            r.leapyear = 1;
        end
    else
        days = jdays;
        r.leapyear = 0;
    end
else
    days = jdays;
    r.leapyear = 0;
end

r.month = min(find(julday <= days)) - 1;
r.day = julday - days(r.month);

% deelbaar door 4, behalve deelbaar door 100, behalve deelbaar door 400


%    if ( (mod(yr,4)==0) & (mod(yr,100)>0) & (mod(yr,400)==0) )
