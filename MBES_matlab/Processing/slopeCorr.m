function     [DATA] = slopeCorr(DATA,flag3,flag4,flagBS,pings_num,delta,Ox,Oy,frequency,int_f,PL,T,int_ping,int_lon,int_lat,int_depth,int_BS,int_beam,int_trans,int_head,int_tx,int_ty,int_sl,int_bp,int_c,int_PL,int_Rn,int_R,int_bn,int_bsn,slope_up,c_up,beammax2,BS_filter,BS_low,BS_up)

if flag3 == 1 
    
    
    
%     
% idx=find(DATA(:,1)==4838 |DATA(:,1)==4839 | DATA(:,1)==51002 | DATA(:,1)==51003);
% DATA(idx,:)=[];
% Initialize
DATA(:,int_tx:int_bn) = NaN*ones(length(DATA(:,1)),length(int_tx:int_bn));
pings_counter = 1;          % counter of pings
last_ping_not_reached = 1;  % used to stop while loop
num_Meas1 = length(DATA(:,1));

% index DATA
DATA(:,end)=1:length(DATA(:,int_ping));

% Identify pings
pings= unique(DATA(:,int_ping));

kkkk=1; 
while last_ping_not_reached == 1
      f = find(DATA(:,int_ping)==pings(pings_counter));
      if isempty(f)
          pings_counter = pings_counter + 1;
          if pings_counter>=length(pings)
             last_ping_not_reached = 0;
          end 
          continue
      end

      ping_start = pings(pings_counter);
      ping_end = pings(pings_counter) + pings_num - 1;
      pings_cut = ping_start:1:ping_end;
      idx = ismember(DATA(:,int_ping),pings_cut);
      data_slope = DATA(idx,:);            
      
       %read data from selected pings
        E=data_slope(:,int_lat); N=data_slope(:,int_lon);
        Z=data_slope(:,int_depth); BS=data_slope(:,int_BS);
        

       % Rotation matrix. Using ship heading to transform from global into
       % local coordinates
         alpha = mean(unwrap(data_slope(:,int_head)*pi/180));
         Mat = [cos(alpha) -sin(alpha) ; sin(alpha) cos(alpha)];
         ENp = Mat*[E N]'; 

         
        % Check heading for grid calculation
        
        if mean(data_slope(:,int_head))> 270 || mean(data_slope(:,int_head)) < 90
            sig = 1;
        else
            sig = -1;
        end
                 
         
       % coordinates along-track (Xp) and across-track (Yp)
         Xp = sig.*ENp(2,:)';  Yp = sig.*ENp(1,:)';
         


      % Data control
      if isempty(Z)
          pings_counter = pings_counter + 1;
          if pings_counter>=length(pings)
             last_ping_not_reached = 0;
          end 
          continue
      end
    
    % Calculate grid parameters
    xmin = min(Xp); xmax = max(Xp);
    ymin = min(Yp); ymax = max(Yp);
    x_idx=xmin+delta/2:delta:xmax-delta/2;
    y_idx=ymin+delta/2:delta:ymax-delta/2;

    % Data control
      if length(x_idx) < 3 | length(y_idx) < 3
          pings_counter = pings_counter + 1;
          if pings_counter>=length(pings)
             last_ping_not_reached = 0;
          end 
          continue
      end
        
    % Initialize
    gridc = zeros(length(x_idx),length(y_idx));     % depth
    grid2c = NaN*ones(length(x_idx),length(y_idx)); % slope
    grid3c = NaN*ones(length(x_idx),length(y_idx)); % tx (slope in x-direction)
    grid4c = NaN*ones(length(x_idx),length(y_idx)); % ty (SLOPE IN Y-DIRECTION) 
    kk = 0;
    [m n] = size(gridc);
    counter = NaN*ones(m*n,600);
    
    % Loop to grid depth and backscatter
    for ii=1:length(x_idx)        
       for jj=1:length(y_idx) 
           kk = kk +1;           
           dummy1 = find(Xp >= x_idx(ii)-delta/2 & Xp < x_idx(ii)+delta/2 & Yp >= y_idx(jj)-delta/2 & Yp < y_idx(jj)+delta/2)';
           counter(kk,1:length(dummy1)) = dummy1;
           % Z-score filter for data points within grid cell (Z-score ref(Rousseeuw and Hubert, 2011))
           Z_filter = Z(dummy1);
           idx_filter = find(abs((Z_filter-median(Z_filter))/(1.483*median(abs(Z_filter-median(Z_filter))))) > 3.5);
           Z_filter(idx_filter) = [];
           
           gridc(ii,jj) = mean(Z_filter);         
       end
    end

    % Loop to calculate slope from gridded depth
    for ii=2:length(x_idx)-1       
        for jj=2:length(y_idx)-1            
            dy = -1*(((gridc(ii-1,jj-1) + 2*gridc(ii,jj-1) + gridc(ii+1,jj-1)...
                -gridc(ii-1,jj+1) - 2*gridc(ii,jj+1) - gridc(ii+1,jj+1)))...
                /(8*delta));
            dx = -1*(((gridc(ii+1,jj-1) + 2*gridc(ii+1,jj) + gridc(ii+1,jj+1)...
                -gridc(ii-1,jj-1) - 2*gridc(ii-1,jj) - gridc(ii-1,jj+1)))...
                /(8*delta));
            % Calculate slope and slope in x and y direction
            grid3c(ii,jj)  = (180/pi)*atan(dx);
            grid4c(ii,jj)  = (180/pi)*atan(dy);
            grid2c(ii,jj)  = (180/pi)*atan(sqrt((dx^2)+(dy^2)));
            
        end
    end
    
%      subplot(1,2,1)
%      h=pcolor(gridc);
%      set(h,'EdgeColor','None')
%      colormap jet
%      colorbar
%      subplot(1,2,2)
%      h=pcolor(grid2c);
%      set(h,'EdgeColor','None')
%     colorbar
%     pause
    % Convert matrix into vector
    dummy2 = grid2c';
    slope = dummy2(:); 
    dummy2 = grid3c';
    tx = dummy2(:);    
    dummy2 = grid4c';
    ty = dummy2(:);
    
    % Data control
     if length(find(isnan(slope)== 1)) < 1;
        pings_counter = pings_counter + 1;
        if pings_counter>=length(pings)
            last_ping_not_reached = 0;
        end 
        continue
     end
     
    % Save calculated slope, tx, ty into original DATA matrix
    for kk=1:length(counter(:,1))
        idx2 = find(counter(kk,:) > 0);
        idx3 = counter(kk,idx2);
        idx4 = data_slope(idx3,end);    
        DATA(idx4,int_tx) = tx(kk); 
        DATA(idx4,int_ty) = ty(kk);  
        DATA(idx4,int_sl) = slope(kk);   
    end
    
     % Reduce pings_counter by 10 pings to account for lost of data points
    % due to gridding
    pings_counter = pings_counter + (pings_num - 10);
    
    % If last ping is reached, break up while loop
     if pings_counter>=length(pings)
                last_ping_not_reached = 0;
     end
kkkk=kkkk+1;     
end
        
        
% Interpolate value-cells (slope, tx and ty) closer than 2*delta to non-value cells to reduce rejected data points due to
% gridding. Using 2*delta still resonable interpolation within the spatial resolution of delta 
% Find non-value cellse and value-cells
dummy = isnan(DATA(:,int_tx));
IDX2 = find(dummy == 0);
IDX3 = find(dummy ~= 0);
X = [DATA(IDX2,2) DATA(IDX2,3) IDX2];
Y = [DATA(IDX3,2) DATA(IDX3,3) IDX3];  
clear dummy
[IDX,D]=knnsearch(X(:,1:2),Y(:,1:2),'K',8);

for i =1:length(IDX(:,1))
    idx = find(D(i,:) < 2*delta);
    % check for NaN values
    dummy = isnan(DATA(X(IDX(i,idx),3),int_sl));
    idx(dummy) = [];    
    if isempty(idx) == 0
    DATA(Y(i,3),int_tx)  = mean(DATA(X(IDX(i,idx),3),int_tx));
    DATA(Y(i,3),int_ty)  = mean(DATA(X(IDX(i,idx),3),int_ty));
    DATA(Y(i,3),int_sl)  = mean(DATA(X(IDX(i,idx),3),int_sl));
    else
    continue
    end
end

% Reject data points with no slope correction value
dummy = isnan(DATA(:,int_ty));
DATA(dummy,:) = [];

%% Slope correction: Correction for footprint (and grazining angle)
%% Using tx and ty (slope in local (ship coordinates) x and y direction)

    % Find starboard and port tack side
    sign = zeros(length(DATA(:,1)),1);
    idx1 = find(DATA(:,int_beam) < 0);
    sign(idx1) = -1;   % negative beams
    idx2 = find(DATA(:,int_beam) > 0);
    sign(idx2) = 1;   % positive beams
    % Consider heading
    meaint_heading = mean(DATA(:,int_head));
    if meaint_heading > 90 && meaint_heading < 270
        head = -1;
    else
        head = 1;
    end
    
    % Define parameters
    teta = abs(DATA(:,int_beam)); % calculate incident angles
    ty = DATA(:,int_ty); % slope in acrosstrack track direction
    tx = DATA(:,int_tx); % slope in alongtrack track direction
    BS = DATA(:,int_BS); % uncorr backscatter
    beam_po = abs(DATA(:,int_bp)); % beam pointing angle      
    c = DATA(:,int_c); % Sound velocity at transducer head
    
    if flag4 == 1
        RI = DATA(:,int_Rn); % Range to normal incidence
        R = DATA(:,int_R); % Range to seabead
        PL = mode(DATA(:,int_PL))*T; %pulse length
    else
        R = (abs(DATA(:,int_depth))-DATA(:,int_trans))./(sin((90-teta)*(pi/180)));
        PL = PL.*T.*ones(length(DATA(:,1)),1);
    end

    
    ax_all=tan(tx.*(pi/180)); ay_all=tan(ty.*(pi/180));

    % True incident angle
    grz = 90-teta;
    Argcos = (sin(grz.*(pi/180)) + sig.*sign.*cos(grz.*(pi/180)).*ay_all) ./ sqrt(ax_all.^2+ay_all.^2 + 1);
    beam_ang = (acos(Argcos).*(180/pi)).*sign; % corrected incident (beam) angle     

        %% Calculate footprints with respect to true grazing/incident angle
        % make sure that no complex number exist due to RI > R
        if flag4 == 1
            idx = find(RI>=R);
            R(idx)=RI(idx)+RI(idx).*0.001;
        end
        
        % Account for frequency shift between different sonar heads
         dx_array = (c./(frequency*1000))./(Ox);
         dy_array = (c./(frequency*1000))./(Oy);
         Ox = (c./DATA(:,int_f))./dx_array;
         Oy = (c./DATA(:,int_f))./dy_array;        
        % Consider wideing of acrosstrack beamwidth with increasing beam
        Oy_beam = Oy.*1./(cosd(beam_po));   
        A_beam= (Ox.*Oy_beam.*R.^2);
        if flag4 == 1
            A_signal_old = (c.*PL.*R.*Ox)./(2.*sqrt(1-((RI.^2)./(R.^2))));
        else            
            A_signal_old = (c.*PL.*R.*Ox)./(2*sin(abs(teta.*pi/180)));
        end
        A_signal_new = (c.*PL.*R.*Ox)./(2*sin(abs((teta - sig.*sign.*ty)).*pi/180).*cos(tx.*pi/180));

        % CORRECTION OF BACKSCATTER VALUES
        C1 = 10.*log10(A_signal_old) - 10.*log10(A_signal_new);
        BS1 = BS + C1;       
        C2 =  10.*log10(A_signal_old) - 10.*log10((R.^2 .* Ox .* Oy_beam)./(cos(tx.*(pi/180)).*cos(ty.*(pi/180))));
        BS2 = BS + C2;      
        C3 = 10.*log10(cos(tx.*pi/180) .* cos(ty.*pi/180));
        BS3 = BS +C3;      
        C4 = 10.*log10(A_beam) - 10.*log10(A_signal_new);
        BS4 = BS + C4;


idxx1 = find(A_signal_old < A_beam & A_signal_new < A_beam);
idxx2 = find(A_signal_old < A_beam & A_signal_new >= A_beam);
idxx3 = find(A_signal_old >= A_beam & A_signal_new >= A_beam);
idxx4 = find(A_signal_old >= A_beam & A_signal_new < A_beam);
BS(idxx1) = BS1(idxx1);
BS(idxx2) = BS2(idxx2);
BS(idxx3) = BS3(idxx3);
BS(idxx4) = BS4(idxx4);
C = zeros(length(BS),1);
C(idxx1) = C1(idxx1);
C(idxx2) = C2(idxx2);
C(idxx3) = C3(idxx3);
C(idxx4) = C4(idxx4);

% Store data
  DATA(:,int_bsn) = BS;
  DATA(:,int_bn) = beam_ang;
%  DATA(:,23) = C;
%   DATA(:,24) = A_signal_new;
%   DATA(:,25) = A_signal_old;  
%   DATA(:,26) = A_beam;

%% Remove soundings with false BS correction term
   II = find(abs(C) > c_up);
   DATA(II,:)=[];
   clear II     

%% Remove soundings with false slope
   II = find(DATA(:,int_sl) > slope_up);
   DATA(II,:)=[];
  clear II       
%% Remove soundings with false corrected beam
   II = find(abs(DATA(:,int_bn)) > beammax2);
   DATA(II,:) = [];
   clear II           
 
 % Remove soundings with false BS (Z-Score filter or threshold filter)        
    if flagBS == 1
        II = find(abs((DATA(:,int_bsn)-median(DATA(:,int_bsn)))/(1.483*median(abs(DATA(:,int_bsn)-median(DATA(:,int_bsn)))))) > BS_filter);
        DATA(II,:) = [];
        clear II
    else
        II = find(DATA(:,int_bsn) < BS_low | DATA(:,int_bsn) > BS_up);
        DATA(II,:)=[];
        clear II
     end  

dummy = isnan(DATA(:,int_bsn));
DATA(dummy,:) = [];

 %Calculate reduction of data points
  num_Meas2 = length(DATA(:,1));
  perc_remMeas = (1-(num_Meas2/num_Meas1))*100;
  disp([num2str(perc_remMeas) ' % Data points are removed due to slope correction']);
end

