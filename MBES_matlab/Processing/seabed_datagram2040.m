function [corr_59h,FS_59h,ping_59h,SN_59h] = seabed_datagram2040(datadirs,filename)

  disp(['Processing ' filename]);
 
%   data_attitude.Roll    = [];
%   data_attitude.Pitch   = [];
%   data_attitude.Heave   = [];
%   data_attitude.Heading = [];
%   data_attitude.Time_ms = [];
%   data_attitude.Jday    = [];
%   data_attitude.Year    = [];


  load([datadirs filename(1:end-4) '_idx.mat']); 

  % Find datagrams of 'attitude' type in the current data file
  ind59 = find(dtype == 89);
  L_59h = length(ind59);
  position_59h = datagramlocation(ind59);
  datafile = [datadirs filename];
  
  
     % Initialize
        ping_59h = NaN*ones(L_59h,1);
        time_59h = NaN*ones(L_59h,1);
        FS_59h = NaN*ones(L_59h,1);
        BSN_59h = NaN*ones(L_59h,1);
        BSO_59h = NaN*ones(L_59h,1);
        TXBW_59h = NaN*ones(L_59h,1);
        TVG_59h = NaN*ones(L_59h,1);
        BeamIndex_59h = NaN*ones(L_59h,254);
        SortingDirection_59h = NaN*ones(L_59h,254);
        NS_59h = NaN*ones(L_59h,254);
        CSN_59h = NaN*ones(L_59h,254);
        corr_59h = NaN*ones(L_59h,1);
        N_59h = NaN*ones(L_59h,1);
        SN_59h = NaN*ones(L_59h,1);
        counter2 = 0;
        
    
  
  for counter = 1:L_59h
    seabed_datagram = datagram59h(datafile,position_59h(counter),'l');
    counter;

%     data_attitude.Roll    = [data_attitude.Roll [data41.attitude.Roll]];
%     data_attitude.Pitch   = [data_attitude.Pitch [data41.attitude.Pitch]];
%     data_attitude.Heave   = [data_attitude.Heave [data41.attitude.Heave]];
%     data_attitude.Heading = [data_attitude.Heading [data41.attitude.Heading]];
%     data_attitude.Time_ms = [data_attitude.Time_ms data41.Time_ms + [data41.attitude.T]];
%     data_attitude.Jday    = [data_attitude.Jday (ones(1,data41.N) * [data41.Jday])];
%     data_attitude.Year    = [data_attitude.Year (ones(1,data41.N) * [data41.Year])];
 
    ping_59h(counter) = [seabed_datagram.Counter];
    time_59h(counter) = [seabed_datagram.Time_ms];
    FS_59h(counter) = [seabed_datagram.FS];          % Sampling frequency
    corr_59h(counter) = [seabed_datagram.C1];       % Read range to normal incidence in samples.
    BSN_59h(counter) = [seabed_datagram.BSN];       % Read normal incidence BS in 0.1 dB.
    BSO_59h(counter) = [seabed_datagram.BSO];       % Read oblique BS in 0.1 dB.
    TXBW_59h(counter) = [seabed_datagram.TXBW];     % Read TX beamwidth in degrees.
    TVG_59h(counter) = [seabed_datagram.TVG];       % Read TVG law cross over angle in 0.1 degree.  
    N_59h(counter) = [seabed_datagram.N];           % Read number of valid beams.
    SN_59h(counter) = [seabed_datagram.SN];           % Read serial number.
    
%     for kk=1:N_59h(counter)   
%     BeamIndex_59h(counter,kk) = [seabed_datagram.beam(kk).BeamIndex];     % Beam index number.
%     SortingDirection_59h(counter,kk) = [seabed_datagram.beam(kk).SortingDirection];     % Sorting direction.
%     NS_59h(counter,kk) = [seabed_datagram.beam(kk).NS];       % Number of samples per beam.
%     CSN_59h(counter,kk) = [seabed_datagram.beam(kk).CSN];       % Centre sample number
%     end
%     if counter == 1
%     dummy(1,:) = zeros(1,NS_59h(1,1));
%     end


% reads backscatter timeseries from seabed datagram into a 3D matrix
% (beams, timeseries per beam, ping). This 3D matrix is necessary for backscatter
% corrections.
% for ii = 1:N_59h(counter)
%     Amp_3D(ii,1:NS_59h(counter,ii),counter) = seabed_datagram.Amplitude(ii,1:NS_59h(counter,ii)); 
% end    


 end



