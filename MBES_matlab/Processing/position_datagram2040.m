function [Time_50h,Lat_50h,Lon_50h] = position_datagram2040(datadirs,filename);

    load([datadirs filename(1:end-4) '_idx.mat']); 
    %idxname = [flist(counter).name '_idx.mat'];
   % load(idxname); clear pingcounter
    pidx_50h = find(dtype == 80);
    positions_50h = datagramlocation(pidx_50h);
    L_50h = length(pidx_50h);
    %disp(' ');
    %disp(['Number of depth datagrams: ',num2str(58)]);
    for counter1 = 1:L_50h        
        position_datagram = datagram50h(datadirs,filename,positions_50h(counter1),'l');
        Time_50h(counter1) = [position_datagram.Time_ms];
        Lat_50h(counter1) = [position_datagram.Lat];
        Lon_50h(counter1) = [position_datagram.Lon];
    end
     
