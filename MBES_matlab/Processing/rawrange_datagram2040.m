function [CsT_4eh,DI_4eh,TT_4eh,Counter_4eh,AC_4eh,BA_4eh,SL_4eh,CF_4eh,SN_4eh] = rawrange_datagram2040(datadirs,filename)

    load([datadirs filename(1:end-4) '_idx.mat']); 
    %idxname = [flist(counter).name '_idx.mat'];
   % load(idxname); clear pingcounter
    pidx_4eh = find(dtype == 78);
    positions_4eh = datagramlocation(pidx_4eh);
    L_4eh = length(pidx_4eh);
    disp(' ');
    disp(['Number of depth datagrams: ',num2str(L_4eh)]);
    
    
    Counter_4eh=zeros(L_4eh,1);
    SN_4eh=zeros(L_4eh,1);
    CsT_4eh=zeros(L_4eh,1);
    NTX_4eh=zeros(L_4eh,1);
    SL_4eh=zeros(L_4eh,1);
    CF_4eh=zeros(L_4eh,1);
    AC_4eh=zeros(L_4eh,1);
    NRX_4eh=zeros(L_4eh,1);
    BA_4eh=zeros(L_4eh,1);
    DI_4eh=zeros(L_4eh,1);
    TT_4eh=zeros(L_4eh,1);
    
    for counter1 = 1:L_4eh        
        range_datagram = datagram4Eh(fullfile(datadirs,filename),positions_4eh(counter1),'l');
        %Type_4eh(counter1) = [range_datagram.Type];
        %Model_4eh(counter1) = [range_datagram.Model];
        %Jday_4eh(counter1) = [range_datagram.Jday];
        %Year_4eh(counter1) = [range_datagram.Year];
        %Time_4eh(counter1) = [range_datagram.Time];
        Counter_4eh(counter1) = [range_datagram.Counter];
        SN_4eh(counter1) = [range_datagram.SN];
        CsT_4eh(counter1) = [range_datagram.C]/10;
        NTX_4eh(counter1) = [range_datagram.Ntx];
        NRX_4eh(counter1) = [range_datagram.Nrx];
        %NV_4eh(counter1) = [range_datagram.Nv];
        %F_4eh(counter1) = [range_datagram.F];
        %Dscale_4eh(counter1) = [range_datagram.Dscale];
        %TA_4eh(counter1,1:NTX_4eh) = [range_datagram.tx.TA]/100; 
        %FR_4eh(counter1,1:NTX_4eh) = [range_datagram.tx.FR]/10;
        SL_4eh(counter1,1:NTX_4eh) = [range_datagram.tx.SL];
        %TTD_4eh(counter1,1:NTX_4eh) =  [range_datagram.tx.TTD];
        CF_4eh(counter1,1:NTX_4eh) = [range_datagram.tx.CF]; 
        AC_4eh(counter1,1:NTX_4eh) = [range_datagram.tx.AC];
        %WF_4eh(counter1,1:NTX_4eh) = [range_datagram.tx.WF];
        %TS_4eh(counter1,1:NTX_4eh) = [range_datagram.tx.TS];
        %BW_4eh(counter1,1:NTX_4eh) = [range_datagram.tx.BW];
        BA_4eh(counter1,1:NRX_4eh) = [range_datagram.rx.BA]/100;
        %TSN_4eh(counter1,1:NRX_4eh) = [range_datagram.rx.TSN];
        DI_4eh(counter1,1:NRX_4eh) = [range_datagram.rx.DI];
        %WL_4eh(counter1,1:NRX_4eh) = [range_datagram.rx.WL];
        %Q_4eh(counter1,1:NRX_4eh) = [range_datagram.rx.Q]; 
        %DC_4eh(counter1,1:NRX_4eh) = [range_datagram.rx.DC];
        TT_4eh(counter1,1:NRX_4eh) = [range_datagram.rx.TT];
        %BS_4eh(counter1,1:NRX_4eh) = [range_datagram.rx.R];
        %CI_4eh(counter1,1:NRX_4eh) = [range_datagram.rx.CI];
     end

