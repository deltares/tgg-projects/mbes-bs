function  [DATA] = absorpCoeff(DATA,datadirs,filename2,flag5,flag4,freq_ab,int_depth,int_BS,int_f,int_ab,int_R,n_BS);
 if flag5 == 1 && flag4 == 1
    %find wrong absorption coefficient
    idx = find(DATA(:,int_ab)>1000);
    DATA(idx,int_ab)=DATA(idx,int_ab)./1000;
    
    %load absorption coefficient from SVP files (txt files is selfmade,
    %check for structure)
    for ii = 1:length(freq_ab)        
        data_ab = load([datadirs filename2 num2str(freq_ab(ii)) '.txt']);
        time = data_ab(2:end,1);
        depth = data_ab(1,2:end);
        absorp_ma = data_ab(2:end,2:end);
        %Calculate absoprtion coefficient dependent on dpeth and time of track
        %line and SVP profile  
        meaint_depth = abs(mean(DATA(:,int_depth)));
        mean_time = mean(DATA(:,13));
        [a idx1] = min(abs(time - mean_time));
        [b idx2] = min(abs(depth - meaint_depth));
        absorp(ii) = absorp_ma(idx1,idx2);
    end
    freq = unique(DATA(:,int_f))./1000;
    
    % account for different frequencies at sonar head    
    for kk = 1:length(freq)
            diff = freq_ab - freq(kk);
            idx_up = find(diff >= 0);
            [a idx_up2] = min(diff(idx_up));
            freq_up = freq_ab(idx_up(idx_up2));
            idx_low = find(diff < 0);
            [a idx_low2] = min(abs(diff(idx_low)));
            freq_low = freq_ab(idx_low(idx_low2));            
            diff_f = freq_up - freq_low;
            diff_ab = absorp(idx_up(idx_up2)) - absorp(idx_low(idx_low2));
            ab_step = diff_ab/diff_f;
            diff_freq =  freq(kk) - freq_low;
            absorp_coef = absorp(idx_low(idx_low2))+ ab_step*diff_freq;            
            % Correct for absorption
            idx_freq = find(DATA(:,int_f) == freq(kk)*1000);
            absorp_old = 2.* (DATA(idx_freq,int_ab)./1000).*DATA(idx_freq,int_R);
            absorp_new = 2.* (absorp_coef/1000).*DATA(idx_freq,int_R);
            DATA(idx_freq,int_BS) = DATA(idx_freq,int_BS) - absorp_old + absorp_new;
            DATA(idx_freq,int_ab) = absorp_coef;
    end
 elseif flag5 == 2 && flag4 == 1
           absorp_old = 2.* (DATA(:,int_ab)./1000).*DATA(:,int_R);
           absorp_new = 2.* (absorp/1000).*DATA(:,int_R);
           DATA(:,int_BS) = DATA(:,int_BS) - absorp_old + absorp_new; 
 end
             
    