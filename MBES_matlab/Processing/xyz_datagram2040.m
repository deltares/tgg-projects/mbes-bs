function [Time_58h,Counter_58h,SN_58h,Hdg_58h,TD_58h,Z_58h,Y_58h,X_58h,AA_58h] = xyz_datagram2040(datadirs,filename);

    load([datadirs filename(1:end-4) '_idx.mat']); 
    %idxname = [flist(counter).name '_idx.mat'];
   % load(idxname); clear pingcounter
    pidx_58h = find(dtype == 88);
    positions_58h = datagramlocation(pidx_58h);
    L_58h = length(pidx_58h);
    %disp(' ');
    %disp(['Number of depth datagrams: ',num2str(58)]);
    for counter1 = 1:L_58h        
        xyz_datagram = datagram58h(datadirs,filename,positions_58h(counter1),'l');
        %Type_4eh(counter1) = [range_datagram.Type];
        %Model_4eh(counter1) = [range_datagram.Model];
        %Jday_4eh(counter1) = [range_datagram.Jday];
        %Year_4eh(counter1) = [range_datagram.Year];
        Time_58h(counter1) = [xyz_datagram.Time_ms];
        Counter_58h(counter1) = [xyz_datagram.Counter];
        SN_58h(counter1) = [xyz_datagram.SN];
        Hdg_58h(counter1) = [xyz_datagram.Hdg];
        TD_58h(counter1) = [xyz_datagram.TD];       
        NB_58h(counter1) = [xyz_datagram.NB];
        Z_58h(counter1,1:NB_58h) = [xyz_datagram.beam.Z];
        Y_58h(counter1,1:NB_58h) = [xyz_datagram.beam.Y];
        X_58h(counter1,1:NB_58h) = [xyz_datagram.beam.X];
        AA_58h(counter1,1:NB_58h) = [xyz_datagram.beam.AA];
    end