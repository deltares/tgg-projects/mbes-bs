function [INDEX, residual] = computeslope(BAT_filter,y,coords,option);

m=length(y);
% Shift the coordinates from the old origin (0,0 of CCS) to the new origin.
% (0,0, now at the centre of the surface patch).
x1=(coords(:,1)-mean(coords(:,1)));
x2=(coords(:,2)-mean(coords(:,2)));
% Select whether a plane or a bi-quadratic should be fitted to the data
% A is the design matrix that will be used for m depths and 3 or 6 columns
% (quadratic)
if nargin == 3 % if there are two arguments in computeslope consider it linear
    A=[ones(m,1) x1 x2];
elseif nargin == 4 % if the option is given... (3 arguments)
    switch lower(option)
        case {1,'linear'}
            A=[ones(m,1) x1 x2]; 
        case {2,'quadratic'} % ?? + ?1? +? 2y + ?3x^2 + ?4y^2 + a5xy
            A=[ones(m,1) x1 x2 x1.*x1 x2.*x2 x1.*x2]; % 1 x y x^2 y^2 xy
        otherwise
            disp('Unknown option!')
            A=[ones(m,1) x1 x2];
    end
end
[xhat, sig2, INDEX] = datasnooping(A,y,BAT_filter);
A=A(INDEX,:); y=y(INDEX,:); x1=x1(INDEX,:); x2=x2(INDEX,:); coords=coords(INDEX,:);
residual = y - A*xhat;

% [m n] = size(A);
% if n==3
%     Apx1=[zeros(m,1) ones(m,1) zeros(m,1)];
%     Apx2=[zeros(m,1) zeros(m,1) ones(m,1)];
% else
%     Apx1=[zeros(m,1) ones(m,1) zeros(m,1) 2*x1 zeros(m,1) x2];
%     Apx2=[zeros(m,1) zeros(m,1) ones(m,1) zeros(m,1) 2*x2 x1];
% end
% Ninv=inv(A'*A); Qx=sig2*Ninv;
% 
% % Computing the mean slopes with their standard deviations
% xp1 = mean(Apx1*xhat); xp2 = mean(Apx2*xhat);
% vxp1 = trace(Apx1*Qx*Apx1')/m^2; vxp2 = trace(Apx2*Qx*Apx2')/m^2;
% T1=atan(xp1); T2=atan(xp2); %ANGLES
% ST1=1/(1+xp1^2)*sqrt(vxp1); ST2=1/(1+xp2^2)*sqrt(vxp2);
% T1=T1*180/pi;   T2=T2*180/pi;
% ST1=ST1*180/pi; ST2=ST2*180/pi;

return






function [xhat, sig2, INDEX]=datasnooping(A,y,BAT_filter)
% xhat: least squares solution of the 3 or 6 parameters, sig2: the estimated
% variance, INDEX : array of observations that are valid
% MORE REFINED FILTERING OF THE DEPTHS DATA BASED ON OBSERVATION TESTING (sigma unknown)
% data
[m n] = size(A); % m: depths n:unknowns 6 for quadratic option
INDEX=[1:m]';
alpha=BAT_filter; % 1-a=confidence interval
factor=tinv(1-alpha/2,m-n); % factor = critical value 
%factor=norminv(1-alpha/2);
while 1
    xhat=inv(A'*A)*A'*y;
    ehat=A*xhat-y;
    sig2=ehat'*ehat/(m-n);
    Qediag=sig2*(ones(m,1)-diag(A*inv(A'*A)*A'));
    Wtest=ehat./sqrt(Qediag);
    %
    idx=find(abs(Wtest)>factor); % COMPARE VALUE OF Wtest with factor
    %
    if length(idx)==0 
        break;
    end
    [value ii]=max(abs(Wtest(idx))); % first we delete the max value and then return again to make the checking   
    A(idx(ii),:)=[]; y(idx(ii),:)=[]; INDEX(idx(ii),:)=[];
    [m n] = size(A);
end

return
