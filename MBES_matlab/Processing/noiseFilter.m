function [DATA] = noiseFilter(DATA,flag1,int_ping,avg_pings,int_BS,b_BS)   
% Remove false pings (Especially due to water bubbles)
if flag1 == 1
            KK=[];
            num_Meas1 = length(DATA(:,1));
            ping = unique(DATA(:,int_ping));     
            for ii=ping(1):avg_pings:ping(end) 
                idx = ismember(DATA(:,int_ping),[(ii:ii+(avg_pings-1))]);
                II = find(idx);
                BS_average1 = mean(DATA(II,int_BS));
                clear II clear idx
                for kk=ii:ii+avg_pings-1
                    II = find(DATA(:,1)== kk);
                    BS_average2 = mean(DATA(II,int_BS));
                    if BS_average1-BS_average2 > b_BS
                        KK=[KK;kk];                
                    end
                    clear II             
                end
            end
            idx=ismember(DATA(:,int_ping),KK);
            II=find(idx);     
            DATA(II,:)=[]; clear II clear idx
            %Calculate reduction of data points
            num_Meas2 = length(DATA(:,1));
            perc_remMeas = (1-(num_Meas2/num_Meas1))*100;
            disp([num2str(perc_remMeas) ' % Data points are removed due to bad ping filter']);
 end    