% Programm for processing MBES data. Currenlty appicable for Kongsberg MBES and
% inparticular for EM 2040C. In case of other Kongsberg MBES adjust
% datagrams in function datagramm_reader. The programs performs, bad ping
% and spline filtering and moving grid slope corrections (slopeCorr).
% In addition the data is filtered regarding bad coordinates, depth,
% backscatter, beams
% Input data structure: extracted MBES data from MB-Sytems in ASCII format
% coulumn
% 1 = ping; 2 = longitude; 3 = latitude; 4 = depth; 5 = backscatter;
% 6 = beam; 7 = beam inclduing slope; 8= transducer depth; 
% 9 = heading; 10 = numb. scatterpixels; 11-14 =date
% Output data structure
% 2 = Northing(y); 3 = Easting(x); 7 = frequency of sonar head;  11 = sound speed; 12 = nominal Pulse lenght ; 
% 14 = beam pointing agnle; 15 = absoprtion
% 16 = range to normal incidence; 17 = Range ; 18 =tx slope; 19 = ty slope;
% 20 = total slope; 21 = corrected BS; 22 = corrected beam; 23 = BS
% correction term; 24 = footprint singal new; 25 = footprint singnal old; 26
% = footprint beam
% Written by Timo C. Gaida (14.12.2018)

clear all;
clc;
close all

addpath ../matlabFun/generallib

%% Folder
% datadirs = 'D:\karaouli\Desktop\Projects\backscattered\DataAmeland2017\mat_files\';
datadirs = 'D:\karaouli\Desktop\Projects\backscattered\DataAmeland2017\all_files\add\';

%%Files
flist = dir([datadirs '*.mat']);   

disp(['Number of files in total ' num2str(length(flist))])

%% Data structure
int_ping =1;   % Ping number
int_lon = 2; % longitude
int_lat = 3; % latitude
int_depth = 4; %depth
int_BS = 5; % BS
int_beam = 6 ; % beam
int_f = 7; % frequency of sonar head
int_trans = 8; % transducer depth
int_head = 9; % heading
int_c = 11; % Sound speed at sonar head
int_PL = 12; % nominal pulse length
int_bp = 14; % beam pointing angle
int_ab = 15; % absorption
int_Rn = 16; % Range to normal incidence
int_R = 17; % Range
int_tx = 18; % along slope
int_ty = 19; % across slope
int_sl = 20; % total slope
int_bsn = 21; % corrected backscatter
int_bn = 22; % true incident angle

%% Sonar Parameters for slope correction
PL = 145*10^-6;            % nominal pulse length [microseconds] (361)
T = 0.37;                  % correction term for pulse shading in [%] to recieve effective pulse length
Ox = 1.3*pi/180;           % along track beam width in [degree]
Oy = 1.3*pi/180;           % along track beam width in [degree]
frequency = 300;           % nominal center frequency (selectec in SIS)
%% Application of filter (on=1; off=0)

%%  Bad ping filter (designed for bad pings due to water bubbles causing false backscatter) 
flag1 = 1; 
avg_pings = 10; % pings used for averaging BS value to find outlier by differing from that avg value
b_BS = 1.8; % defines BS difference (single ping to averaged pings) for rejecting pings

%% Spline Filter (for adjusting parameters see spline.m)
flag2 = 2;      % 1 = 2D Spline filter (fast); 2 = 1D Spline filter; 0 = no spline filter
wF = 3.0;       %1D: weightening factor (the smaller the stronger the spline filter)
span = 0.03;    %1D: percentage of data points used for fitting
BAT_filter = 0.004;   % 2D: the larger the srtonger the filter (between 0.001 and 0.2)
delta2 = 3;        % patch size for fitting a 2D plane (the smaller the patch size the faster the algorithm); depends on sounding density and water depth (between 5 and 25 m)
ping_sp = 200;     % number of pings used for spline calculation

% Slope correction (Correcting for beam footprint)
flag3 = 1;
pings_num = 50; % number of pings used for variable grid calculation (considered variable ship heading)
delta = 1;      % Grid Cell size (spatial resolution); consider MBES characteristics and water depth

%% Real Time TVG correction
flag4 = 1;

% Absorption correction
flag5 = 0;  %1= consider several SVP profiles; 2 = define constant absorption coeff; 0 = no correction
absorp = 72.9;  %absorption coefficent: required in case flag5 is 2
filename2 = 'absorption'; % filename: required in case flag5 is 1
freq_ab = [250 300 350]; % calculated absorption for considered frequencies: required in case flag5 is 1

% Backscatter filter
flagBS = 1;     % Z-score filter = 1 / Threshold filter = 0
BS_filter = 3.5; % filter strength increases with decreasing value: Standard = 2.5 / reasonable = 3.5
BS_low = -50;   % lower threshold
BS_up = -5;     % upper threshold

%% Simple rejection of false coordinates im georahical coordinates (WGS84)
lonmin = 5.0; lonmax = 6.0;
latmin = 52.0; latmax = 55.0;

%% Simple rejection of false water depth (negative water depth in [m])
depthmin = -2;
depthmax = -35;

%% Simple rejection of false beam angles (uncorrected beam in [degree])
beammax = 75;

%% Simple rejection of false beam angles (corrected beam in [degree])
beammax2 = 80;

%% Simple rejection of false slope in [degree]
slope_up = 50;

%% Simple rejection of wrong backscatter correction term in [dB]
c_up = 10;

%% Define name of output file (number of ending)
leng = length(flist(1).name)-12;

%% Initialise
int_all=[];

%% Read files in directory
 for jj =1:length(flist)     
        %%load and adjust data
        load([datadirs flist(jj).name]);
        exist data;
        if ans == 1
            DATA =data;
            clear data
        end
        ix=find(DATA(:,1)==51002 | DATA(:,1)==51003 | DATA(:,1)==4838 | DATA(:,1)==4839);  
        
        DATA(ix,:)=[];
        %Extent data matrix
        DATA(:,int_ab:int_R) =NaN*ones(length(DATA(:,int_ping)),3);  
        %Display processed file
        disp(['Executing file ' num2str(jj) ': ' flist(jj).name])
        num_Meas = length(DATA(:,1));       
                    
        %%Read parameter from datagrams (e.g. Range, range to normal inciddence) for real time TVG-correction
        if flag4 == 1
%             filename = [flist(jj).name(1:leng+4) '.all'];
            filename='D:\karaouli\Desktop\Projects\backscattered\DataAmeland2017\all_files\add\0001_20170905_184833_ARCA.all'
            [DATA] = datagramReader(datadirs,filename,DATA,int_c,int_PL,int_f,int_bp,int_ab,int_Rn,int_R);
        end
     aa   
        %% Remove soundings with false coordinate
        num_Meas2 = length(DATA(:,1));
        II = find(DATA(:,int_lon)< lonmin | DATA(:,int_lon)> lonmax | DATA(:,int_lat)< latmin | DATA(:,int_lat) > latmax);
        DATA(II,:) = [];   clear II       
        
        %% Convert lat & lon to UTM
        [DATA(:,int_lat),DATA(:,int_lon), UTMzone] = deg2utm(DATA(:,int_lat),DATA(:,int_lon));

%         [XXX,YYY] = deg2utm(DATA(:,int_lat),DATA(:,int_lon));

        %% Remove soundings with false BS (Z-Score filter or threshold filter)        
        if flagBS == 1
            II = find(abs((DATA(:,int_BS)-median(DATA(:,int_BS)))/(1.483*median(abs(DATA(:,int_BS)-median(DATA(:,int_BS)))))) > BS_filter);
            DATA(II,:) = [];
            clear II
        else
            II = find(DATA(:,int_BS) < BS_low | DATA(:,int_BS) > BS_up);
            DATA(II,:)=[];
            clear II
        end  

        %% Remove beam angles
        II = find(abs(DATA(:,int_beam)) > beammax);
        DATA(II,:) = []; clear II                
        
        %Calculate reduction of data points
        num_Meas1 = length(DATA(:,1));
        perc_remMeas = (1-(num_Meas1/num_Meas2))*100;
        disp([num2str(perc_remMeas) ' % Data points are removed due to threshold filters']);        
        
        %% Remove false pings (Especially due to water bubbles)
        [DATA] = noiseFilter(DATA,flag1,int_ping,avg_pings,int_BS,b_BS);  
 
        %% Remove soundings with false depth
        II = find(DATA(:,int_depth) > depthmin | DATA(:,int_depth)< depthmax );
        DATA(II,:)=[]; clear II        
        
        %% Applying spline filter for removing bad bathymetry soundings
        [DATA] = splineFilter(DATA,jj,flag2,wF,int_ping,int_lon,int_lat,int_depth,span,delta2,ping_sp,BAT_filter);
         %% Apply absorption correction        
        [DATA] = absorpCoeff(DATA,datadirs,filename2,flag5,flag4,freq_ab,int_depth,int_BS,int_f,int_ab,int_R);
        
        %% Apply slope correction
        tic
         [DATA] = slopeCorr(DATA,flag3,flag4,flagBS,pings_num,delta,Ox,Oy,frequency,int_f,PL,T,int_ping,int_lon,int_lat,int_depth,int_BS,int_beam,int_trans,int_head,int_tx,int_ty,int_sl,int_bp,int_c,int_PL,int_Rn,int_R,int_bn,int_bsn,slope_up,c_up,beammax2,BS_filter,BS_low,BS_up);
        toc
        %Calculate reduction of data points
        num_Meas2=length(DATA(:,1));
        perc_remMeas = (1-(num_Meas2/num_Meas))*100;
        disp([num2str(perc_remMeas) ' % Data points are removed due to full processing']);
                       
        %% Store corrected data in individual track files
        save('-V7.3',[datadirs flist(jj).name(1:leng) 'filter_slope.mat'], 'DATA');        
        
 end