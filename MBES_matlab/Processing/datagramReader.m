function [DATA] = datagramReader(datadirs,filename,DATA,int_c,int_PL,int_f,int_bp,int_ab,int_Rn,int_R)
addpath '../matlabFun/mbeslib/'
addpath '../matlabFun/generallib/'

num_Meas1 = length(DATA(:,1));
% Call Raw range datagram
[CsT_4eh,DI_4eh,TT_4eh,Counter_4eh,AC_4eh,BA_4eh,SL_4eh,CF_4eh,SN_4eh] = rawrange_datagram2040(datadirs,filename);
% Call Seabed datagram
[corr_59h,FS_59h,ping_59h,SN_59h] = seabed_datagram2040(datadirs,filename);

[ping_Data idx] = sort(DATA(:,1));

[ping_59h idx] = sort(ping_59h);
corr_59h  = corr_59h(idx);
FS_59h = FS_59h(idx);
SN_59h = SN_59h(idx);
clear idx;
[Counter_4eh idx] = sort(Counter_4eh);
CsT_4eh = CsT_4eh(idx);
CF_4eh = CF_4eh(idx);
SN_4eh = SN_4eh(idx);
DI_4eh = DI_4eh(idx,:);
TT_4eh = TT_4eh(idx,:);
BA_4eh = BA_4eh(idx,:);
SL_4eh = SL_4eh(idx,:);
AC_4eh = AC_4eh(idx);
clear idx;


min_ping = min(ping_59h);
max_ping = max(ping_59h);     
ping = min_ping(1);
ppp=0;
ppp2=0;
ppp3 = 0;
ppp4 =0;

 while ping <= max_ping(1)
           
     idx1 = find(ping_59h==ping);
     idx2 = find(Counter_4eh==ping);
     idx3 = find(DATA(:,1)==ping);     
       
     if isempty(idx1) || isempty(idx2)|| isempty(idx3)         
         ping =ping+1;
         ppp=ppp+length(idx3);
         continue
     end     

    if length(idx1) == 2 & length(idx2) == 2
        kk = idx2(1); 
        kk2 = idx2(2); 
        ind1 = [find(DI_4eh(kk,:) == 0)  find(DI_4eh(kk,:) ==1)];
        ind1 = sort(ind1); 
        ind2 = [find(DI_4eh(kk2,:) == 0)  find(DI_4eh(kk2,:) ==1)];
        ind2 = sort(ind2);         
        if length(ind1)+length(ind2) ~= length(idx3)
             ping =ping+1;
             if length(idx3) > length(ind1)+length(ind2)
             ppp2 = ppp2 + abs(length(idx3)- (length(ind1)+length(ind2))) ;
             end
            continue
        end
        TTI1 = corr_59h(idx1(1)).*1./FS_59h(idx1(1))'; % Traveltime to normal incidence
        TTI2 = corr_59h(idx1(2)).*1./FS_59h(idx1(2))'; % Traveltime to normal incidence
        RI1 = (CsT_4eh(idx2(1)).*TTI1)./2; % Range to normal incidence
        RI2 = (CsT_4eh(idx2(2)).*TTI2)./2; % Range to normal incidence
        RI = [RI1*ones(length(ind1),1) ; RI2*ones(length(ind2),1)];
        R = (CsT_4eh(idx2(1)).*TT_4eh(idx2(1),ind1))./2;
        R2 = (CsT_4eh(idx2(2)).*TT_4eh(idx2(2),ind2))./2;
        R = [R' ; R2'];
        abso = AC_4eh(idx2(1))/100*ones(length(ind1),1);
        abso = [abso ; AC_4eh(idx2(2))/100*ones(length(ind2),1)];
        C = CsT_4eh(idx2(1))*ones(length(ind1),1);
        C = [C ; CsT_4eh(idx2(2))*ones(length(ind2),1)];   
        beam_po1 = BA_4eh(idx2(1),ind1);
        beam_po2 = BA_4eh(idx2(2),ind2);
        beam_po =[beam_po1' ; beam_po2'];         
        PL = [SL_4eh(idx2(1)).*ones(length(ind1),1) ; SL_4eh(idx2(2)).*ones(length(ind2),1)]; 
        F = [CF_4eh(idx2(1)).*ones(length(ind1),1) ; CF_4eh(idx2(2)).*ones(length(ind2),1)]; 
        DATA(idx3,int_c) = C;
        DATA(idx3,int_PL) = PL;
        DATA(idx3,int_f) = F;
        DATA(idx3,int_bp) = beam_po;
        DATA(idx3,int_ab) = abso;
        DATA(idx3,int_Rn) = RI;
        DATA(idx3,int_R) = R;
        clear R; clear RI1; clear RI2; clear R2 ;clear RI; clear PL; clear C; clear F;
        clear TTI; clear TT1; clear TT2; clear abso; clear beam_po; clear beam_po2
    else
        if length(idx2) == 2        
            idx =  ismember(SN_4eh(idx2), SN_59h(idx1));
            idx2 = idx2(idx);            
        elseif length(idx1) == 2   
            idx =  ismember(SN_59h(idx1), SN_4eh(idx2));
            idx1 = idx1(idx); 
        end
   
        ind1 = [find(DI_4eh(idx2,:) == 0)  find(DI_4eh(idx2,:) ==1)];            
        if  length(ind1) ~= length(idx3)
             ping =ping+1;  
             ppp3 = ppp3 + (abs(length(idx3)-length(ind1)));
            continue
        end 

        TTI = corr_59h(idx1).*1./FS_59h(idx1)'; % Traveltime to normal incidence
        RI = (CsT_4eh(idx2).*TTI)./2; % Range to normal incidence
        R = (CsT_4eh(idx2).*TT_4eh(idx2,ind1))./2;
        abso = AC_4eh(idx2)./100;
        beam_po = BA_4eh(idx2);
        PL = SL_4eh(idx2);
        F = CF_4eh(idx2);
        C = CsT_4eh(idx2);
        DATA(idx3,int_c)=C;
        DATA(idx3,int_PL)= PL;
        DATA(idx3,int_f)=F;
        DATA(idx3,int_bp)= beam_po;
        DATA(idx3,int_ab) = abso*ones(length(idx3),1);
        DATA(idx3,int_Rn) = RI*ones(length(idx3),1);
        DATA(idx3,int_R) = R;
        
        clear R; clear RI1; clear RI2; clear R2 ;clear RI; clear PL; clear C; clear F;
        clear TTI; clear TT1; clear TT2; clear abso; clear beam_po; clear beam_po2
    end
        ping=ping+1;
 end
       
DATA(isnan(DATA(:,int_ab)),:)=[];

%Calculate reduction of data points
 num_Meas2 = length(DATA(:,1));
 perc_remMeas = (1-(num_Meas2/num_Meas1))*100;
 disp([num2str(perc_remMeas) ' % Data points are removed due to datagramreader']);
end
 