function [DATA] = splineFilter(DATA,jj,flag2,wF,int_ping,int_lon,int_lat,int_depth,span,delta2,ping_sp,BAT_filter)

%% 1D-Spline
num_Meas2 = length(DATA(:,1));
if flag2 ==2            
            ping = unique(DATA(:,int_ping));                         
        for ii= min(ping):max(ping)                   
            count_1D = length(DATA(:,1));
            idx = find(DATA(:,int_ping) == ii);            
            if length(idx) < 5
                DATA(idx,:)=[];
                continue                
            end            
            x = 1:length(idx);
            y = DATA(idx,int_depth);         
            % Fit polynom to depth data
           yy = smooth(x,y,span,'rloess');
            % Calculate difference between polynominal fit and measured data
            diff = abs(yy-y);
            % Reject all data points depending on difference to polynominal
            % and standard deviation
            std_diff = std(diff);         
            idx2= find(diff> wF *std_diff);  
            II = min(idx)+idx2-1;
%             storage = storage+length(II);
            DATA(II,:)=[];
            clear II idx2 idx           
        end
        %Calculate reduction of data points
        num_Meas3 = length(DATA(:,1));
        perc_remMeas = (1-(num_Meas3/num_Meas2))*100;
        disp([num2str(perc_remMeas) ' % Data points are removed due to 1D-splinec filter']);
        disp(['1D Spline filter is finish for file ' num2str(jj)]);
        
%%2D-Spline      
elseif flag2 ==1     
        storage=[];
        ping = unique(DATA(:,int_ping));
        for ping_n= min(ping):ping_sp:max(ping) 
            ping_count = ping_n:ping_n+ping_sp-1;
            if max(ping)-max(ping_count) < ping_sp
                ping_count = min(ping_count):max(ping);
            end
            idx = ismember(DATA(:,int_ping),ping_count);
            count = find(idx==1);
        % Calculate grid parameters
        Xp = DATA(idx,int_lon);
        Yp = DATA(idx,int_lat);
        Zp = DATA(idx,int_depth);
        xmin = min(Xp); xmax = max(Xp);
        ymin = min(Yp); ymax = max(Yp);
        x_idx=xmin+delta2/2:delta2:xmax-delta2/2;
        y_idx=ymin+delta2/2:delta2:ymax-delta2/2;
        % Initialize         
        % Loop to grid depth and backscatter
        for ii=1:length(x_idx)               
            for ff=1:length(y_idx)                
                dummy1 = find(Xp >= x_idx(ii)-delta2/2 & Xp < x_idx(ii)+delta2/2 & Yp >= y_idx(ff)-delta2/2 & Yp < y_idx(ff)+delta2/2)';
                if length(dummy1) < 15                    
                    continue
                end
                [INDEX, residual] = computeslope(BAT_filter,Zp(dummy1),[Xp(dummy1) Yp(dummy1)],'quadratic');
               
                index = 1:length(dummy1);
                idx_filter = ismember(index,INDEX);
                idx_filter = find(idx_filter==0);
                storage = [storage ;count(dummy1(idx_filter))]; 
                
% %                %Visulaisation of 2D spline filter
%                  residual = abs(residual);
%                  Z_int = Zp(dummy1(INDEX)) - residual;
%                  scatter3(Xp(dummy1),Yp(dummy1),Zp(dummy1),'.b')
%                  hold on
%                  scatter3(Xp(dummy1(INDEX)),Yp(dummy1(INDEX)),Z_int,'.k')
%                  scatter3(Xp(dummy1(idx_filter)),Yp(dummy1(idx_filter)),Zp(dummy1(idx_filter)),'*r')
%                pause
                idx_filter=[];
               end
        end
        end
         DATA(storage,:)=[];
end

%Calculate reduction of data points
 num_Meas1 = length(DATA(:,1));
 perc_remMeas = (1-(num_Meas1/num_Meas2))*100;
 disp([num2str(perc_remMeas) ' % Data points are removed due to Spline filter']);