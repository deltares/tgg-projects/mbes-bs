%function [BSclassmean, BSmean, Pmean]=mainfit_step3_Constraint(dirMAP, refbeams, side, numclass, stdmin, stdmax, latlon, depth, varargin)
close all
clear all
clc

toplot = true;
disp('Step 3 has begun')
% This function assignes the backscatter strength of a multi-beam echosounder
% for a set of reference angles (a set of outer beams we can trust, according
% to the initial sparate classification in mainfit_step2_ClassifyCheck.m)
% to a fixed number of classes, as is estimated by mainfit_step1_NumberOfClasses.m.
%
% This function is similar to mainfit_step2_ClassifyCheck.m. However, in
% order to make the classification results comparable for the different beams,
% backscatter strength values at these beams are shifted. Further, constaints
% are added to the class mean values (position on the backscatter axis) to
% obtain similar classes.
% This requires to run mainfit_step2_ClassifyCheck.m for the
% angles under consitderation, first.
%
% INPUT parameters are:
% dirMAP: string, directory in which MAP files containing BS per angle and
%         side are stored, e.g.
%         dirMAP = 'D:\kherms\Documents\MBES\DATAKlaverbank2014\MAPfiles\';
% refbeams !!! (VECTOR) !!!: the beam angle in degrees, vector of interger values containing
%       those beams at which similar classification results are expected
%       (90 degrees at the horizontal, 0 degrees at nadir direction)
%       as in the filename of the BSmap_MEAN files
% side: 1=port(default), 2=starbord
% numclass: number of classes used in the current classification
%           (as determined from mainfit_step2_NumberOfClasses.m)
% stdmin: minimum standarddeviation used to create the fitting Gaussians
%         (as determined from mainfit_step2_NumberOfClasses.m)
% stdmax: minimum standarddeviation used to create the fitting Gaussians
%         (as determined from mainfit_step2_NumberOfClasses.m)
% depth: vector containing the POSITIVE(!) depth boundaries in the form
%        [depthmin depthmax]
%        (used to remove outliers)

% OUTPUT parameters are:
% BSclassmean: mean values of the classes at reference beams (one vector BSclassmean per side!!!)
% BSmean: mean of all BS values (in all classes) at reference beams (one BSmean per side!!!)
% Pmean: percentages of backscatter strength per class, averaged over the
% beams considered (one Pmean per side!!!)
% these parameters will be used in step 4, where the classification is done for 
% the remaining beams


%% Folders
dirMAP = 'G:\Ameland\2018\angle\';
dirMAP2 = 'G:\Ameland\2018\angle\step2\';
dirMAP3 = 'G:\Ameland\2018\angle\step3\';

%% Set parameters
%  refbeams = [54 56 58];      % Reference beam angle
%refbeams = [60 62 64];      % Reference beam angle
%   refbeams = [40 43 47];      % Reference beam angle
refbeams =[62 64];
side = [1];
numclass = 5;   % Optimal number of classes idenitfied in Step 1

%% decision on the upper and lower bound of the standard deviation %
%disp('The decision on the lower and upper bound of the standard deviation')
%disp('should be based on the actual standard deviation (green dots)')
%disp('displayed in the center plot in figure 1.')
stdmin = 0.5; 
stdmax = 2.5; 

%% Filter boundaries
scatter = 5;    %number of scatter pixel
BSMIN = -50;    % lower BS bound
BSMAX = -5;     % upper BS bound
depth = [3 45]; % depth boundaries

%% Histogramm definition (Depends on backscatter value range)
hist_int = 0.5; % bin interval
hist_filter = 0.01; % filters out all bins in the histogram with number of BS values lower than hist_filter (0.01 = 1%)     

%% initialize the percentages 
p = NaN(length(refbeams),numclass);

%% LOAD DATA 
M_all=[];
meanBS_all=[];
for k=1:length(refbeams)
    beam = refbeams(k);
    eval(['load ',dirMAP2,'Bmean_',num2str(side),'_',num2str(beam),'_',num2str(numclass),' M S B meanBS'])
    M_all = [M_all; M'];
    meanBS_all = [meanBS_all, meanBS];
end

if length(refbeams)>1 % more than 1 reference angle
    BSclassmean = (mean(M_all))'; % mean values of the classes at reference beams
    BSmean = mean(meanBS_all); %mean of all BS values (in all classes) at reference beams
else % only 1 reference angle
    BSclassmean = M_all';
    BSmean = meanBS_all;
end

for k=1:length(refbeams)
    beam = refbeams(k);
    eval(['load ',dirMAP,'MAPmean_',num2str(side),'_',num2str(beam)])
        BS_all=data(:,5);
        Z_all=data(:,4);
        lat_all=data(:,2);
        lon_all=data(:,3);
        No_all=data(:,6);
    %% Check if central limit theorem is valid
    I = find(No_all < scatter);
    BS_all(I)=[]; No_all(I) = []; Z_all(I) = []; lat_all(I) = []; lon_all(I) =  [];
    clear I
    %% accounting for depth boundaries
    I = find(Z_all < depth(1) |  Z_all > depth(2));
    BS_all(I)=[]; No_all(I) = []; Z_all(I) = []; lat_all(I) = []; lon_all(I) =  [];
    clear I
    %% accounting for backscatter boundaries
    I = find(BS_all>BSMIN | BS_all<BSMAX);
    BS_all = BS_all(I);
    No_all = No_all(I);
    Z_all = Z_all(I);
    lat_all = lat_all(I);
    lon_all = lon_all(I);
    clear I
    
    %% Shift of BS values
    if length(refbeams)>1
        BS_all = BS_all + BSmean - mean(BS_all);
        BSMIN = BSMIN + BSmean - mean(BS_all);
        BSMAX = BSMAX + BSmean - mean(BS_all);
    end
    
    %% Creat histograms and find bins with low amount of BS values in backscatter histogram ( > 0.01 = 1% of)         
    x = BSMIN:hist_int:BSMAX;
    n = hist(BS_all,x);    
    I = find(n > hist_filter*max(n));
    Data = [x(I)' n(I)'];
    t = Data(:,1);
    BSMIN = min(x(I)); 
    BSMAX = max(x(I));

    %% testing the goodness of fit between the histogram and ng Gaussians
    ng = numclass;
    if toplot
        figure(2)
        hold on
        %bar(x,n)
        errorbar(Data(:,1),Data(:,2),sqrt(Data(:,2)),'k')
        h = get(gca,'Children');
        set(h,'LineWidth',1)
        h = get(gcf,'Children');
        set(h,'LineWidth',1)
        set(h,'FontSize',14)
        xlabel('y_j [dB]')
        ylabel('n_j')
        grid
    end
    
    %% start solution non-linear parameters
    delta_BS = (BSMAX-BSMIN)/ng;
    BSm = BSMIN + 0.5*delta_BS:delta_BS: BSMAX - 0.5*delta_BS; % resolution BS data from MBES is 0.5 dB
    BSm = BSm';
    
    % initial solution with constraints
    lam0 = [1*ones(length(BSm),1) BSclassmean];
    % linear parameters
    global c;
    % search intervals with constraints
    lb = [stdmin*ones(length(BSm),1) BSclassmean-BSmean+mean(BS_all)-1];
    ub = [stdmax*ones(length(BSm),1) BSclassmean-BSmean+mean(BS_all)+1];
    
    % solve
    options = optimset('lsqnonlin');
    options = optimset(options,'Algorithm','levenberg-marquardt');
    options = optimset(options,'MaxFunEvals',300000);
    options = optimset(options,'MaxIter',30000000);
    options = optimset(options,'TolX',1e-4);
    [lam,resnorm,residual,exitflag,output]= lsqnonlin('fitfunctionAliReza',zeros(size(lam0)),lb,ub,options,Data);
    %[lam,resnorm,residual,exitflag,output]= lsqnonlin('fitfunction',lam0,lb,ub,[],Data);
    % assessment of fit (visual)
    y(:,1) = c(1)*exp(-((t - lam(1,2)).^2)/(2*lam(1,1)^2));
    yfit   = c(1)*exp(-((t - lam(1,2)).^2)/(2*lam(1,1)^2));
    for j = 2 : length(BSm)
        y(:,j) = c(j)*exp(-((t - lam(j,2)).^2)/(2*lam(j,1)^2));
        yfit = yfit + y(:,j);
    end
    
    if toplot
        figure(2)
        hold on
        plot(t,yfit,'-r','LineWidth',2)
        for j = 1 : length(BSm)
            hold on
            plot(t,y(:,j),'k','LineWidth',0.5)
        end
        hold off
        grid        
        lam
    end
    
    %% reduced chi-square
    nu = (length(Data(:,2))-3*length(BSm));    
    chi2r = sum( ((yfit - Data(:,2)).^2)./Data(:,2)  )/nu
    
    %% plot classes
    clear y; dt = 0.01; t = [min(t) : dt : max(t)]';
    if toplot
        figure(3);
        hold on;
    end
    for j = 1 : length(BSm)
        y(:,j) = (1/(sqrt(2*pi)*lam(j,1) ) )* exp(-((t - lam(j,2)).^2)/(2*lam(j,1)^2));
        if toplot
            plot(t,y(:,j),'-b')
        end
    end
    if toplot
        grid
        h = get(gca,'Children');
        set(h,'LineWidth',1.5)
        h = get(gcf,'Children');
        set(h,'LineWidth',1)
        set(h,'FontSize',14)
        xlabel('y_j [dB]')
        ylabel('n_j')
        grid
    end
    
    %% Determine class boundaries
    clear I
    if toplot
        figure(3);
        hold on
    end
    for j = 1 : length(BSm)-1
        J = find(t>lam(j,2) & t < lam(j+1,2));
        if isempty(J)
            
        end
        [dummy,I(j)] =  min( abs(y(J,j) - y(J,j+1))  ); % seems to be a bit of a problem line
        I(j) = I(j) + J(1)-1;
        if toplot
            plot(t(I(j)),y(I(j),j),'ksq','MarkerSize',8,'MarkerFaceColor',[0 0 0])
        end
    end
    
    M = lam(:,2); % class mean values
    S = lam(:,1); % class standarddeviation values
    B = [-Inf t(I)' Inf]; % class backscatter boundaries
    meanBS = mean(BS_all);
    
    eval(['save ',dirMAP3,'Bmean_constraint_',num2str(side),'_',num2str(beam),'_',num2str(numclass),' M S B meanBS'])
    clear M S B meanBS

    %% assign backscatter to classes and calculate precentages of the classes
    eval(['load ',dirMAP3,'Bmean_constraint_',num2str(side),'_',num2str(beam),'_',num2str(numclass),' M S B meanBS'])

    I1 = find(BS_all > B(1) & BS_all < B(2));
    p(k,1) = length(I1)/length(BS_all);
    if ng>1
    I2 = find(BS_all > B(2) & BS_all < B(3));
    p(k,2) = length(I2)/length(BS_all);
    end
    if ng>2
    I3 = find(BS_all > B(3) & BS_all < B(4));
    p(k,3) = length(I3)/length(BS_all);
    end
    if ng>3
    I4 = find(BS_all > B(4) & BS_all < B(5));
    p(k,4) = length(I4)/length(BS_all);
    end
    if ng>4
    I5 = find(BS_all > B(5) & BS_all < B(6));
    p(k,5) = length(I5)/length(BS_all);
    end
    if ng>5
    I6 = find(BS_all > B(6) & BS_all < B(7));
    p(k,6) = length(I6)/length(BS_all);
    end
    if ng>6
    I7 = find(BS_all > B(7) & BS_all < B(8));
    p(k,7) = length(I7)/length(BS_all);
    end
        
    %% save chiquare values to negelect bad fitting in subsequent step
    chi(k) = chi2r;    
    clear y
end

% idx = find( chi > min(chi)*5);
% p(idx,:)=[];

if length(refbeams)>1
    Pmean=mean(p);
else
    Pmean=p;
end

save([dirMAP3 'refbeam_values_',num2str(refbeams),'_side_',num2str(side),'_class',num2str(numclass),'.mat'],'BSclassmean','BSmean','Pmean');
