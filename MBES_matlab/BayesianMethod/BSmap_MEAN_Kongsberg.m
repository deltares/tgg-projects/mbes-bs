clear all; close all; clc;

addpath ../matlabFun/generallib
addpath ../matlabFun/mbeslib


%function BSmap_MEAN(dirDATA, fileSpec, dirMAP, theta_st)
%return;
% This function creates MAPmean*.mat-files, collecting backscatter strength 
% per angle of incidence (CHANGE INTO BEAM ANGLE) 
% and side (port/starbord).
% It considers the chaniging beam angle/incident angle due to the seabed
% slope.
% Averaging over a couple of pings and beams is applied to account for footprint effects, 
% dependend on the beam angle and water depth. First soundings are selected by
% original beam angle and pings. Afterwards selected soundings are sorted considering
% modified incident angle (considering slope) to account for location and new incident angle.
%
% Input data structure: data structure obtained from Processing.m
% (_filter_slope.dat)
%
% 1 = ping; 2 = Easting(x); 3 = Northing(y); 4 = depth; 5 = backscatter;
% 6 = beam; 7 = beam inclduing slope; 8= transducer depth; 
% 9 = heading; 10 = numb. scatterpixels; 11-14 =date;  15 = absoprtion
% 16 = range to normal incidence; 17 = Range ; 18 =tx slope; 19 = ty slope;
% 20 = total slope; 21 = corrected BS; 22 = corrected beam; 23 = BS
% correction term; 24 = footprint singal new; 25 = footprint singnal old; 26
% = footprint beam
% Written by Timo C. Gaida (17.10.2017)
% dirDATA: string, directory in which filterd data is stored
% dirDATA2: string, directory in which produced files are temporarily% stored
% dirMAP: string, directory in which MAPmean*.mat files containing BS per angle and side are stored
% theta_st: vector of beam angles that need to be processed (0 deg at nadir, 90 deg at horizontal)
%           often beams between beams of 20 deg and 60 deg (i.e. 70-30deg
%           incidence) are used in the later classification process 
%           we use: theta_st = [60:-2:38 34 30 26 20] 
%              (or theta_grazing = [30:2:52 56 60 64 70])

%%%%%%%%%%%%%
%% Folders
dirDATA1 = 'O:\TIMO\Projects\Rijkswaterstraat\RWS_ArmelandApril2017\DATA\SIS\files\';
dirDATA2 = 'O:\TIMO\Projects\Rijkswaterstraat\RWS_ArmelandApril2017\Bayesian\work\';
dirMAP = 'O:\TIMO\Projects\Rijkswaterstraat\RWS_ArmelandApril2017\Bayesian\patches\';


%% Assigne columns to MBES parameters (Columns are defined in the Processing_Kongberg.m script)
int_p = 1;      %ping
int_lon = 2;    % Easting, longitude 
int_lat = 3;    % Northing, latitude
int_d = 4;      % depth
int_ob = 6;     % beam angle (orginal beam angle given by the sonar)
int_BS =21;      % modified BS
int_b = 22;       % incident angle (modified by using beam angle and slope >> see slopeCorrection.m)
int_ns = 10;     % number of scatter pixel

%% Number of stored features    
n_feature = 5;

%% Specify filename and flist
fileSpec = '*filter_slope.mat';
flist = dir([dirDATA1, fileSpec]);

%% Chose beam and ping intervalls manually to consider amount of necessary scattering pixel per patch.
%% Ping and beam intervalls are selected to compensate different number of scatterpixels for different beam angles depth
%% Matlab script beam_distant.m not ready to share yet
%% Values are good approximation for EM2040C in a water depth between 10 and 50m. 
theta_st = [ 64:-1:54 53:-2:45 43:-3:30 27:-4:3 ];
pings_num = [10 4 3 2 1];
depth_bound = [0 5 10 15 30];
%% Tolerances for bounds of beam angles. Averaging over angles will take place inbetween these bounds, 
%% to compensate different number of scatterpixels within differnt angles
tol2 = ones(size(theta_st));
tol2(theta_st>=54 & theta_st<=60)=0.7;
tol2(theta_st>=45 & theta_st<=53)=1.3;
tol2(theta_st>=30 & theta_st<=43)=1.9;
tol2(theta_st>=3 & theta_st<=27)=2.5;

%% Initialize
dummy = [];
data_counter = 0;

% loop over files
for file_counter = 1:2%length(flist);    
    
    %% Display file
    disp(['Making MAPmean of file # ' num2str(file_counter) ' of: ' ...
        num2str(length(flist))]);    
    %% load file
    load([dirDATA1,flist(file_counter).name]);
    exist DATA;
    if ans == 1
       data =DATA;
       clear DATA
    end
    % Convert depth to postive values
    if median(data(:,int_d)) < 0
        data(:,int_d) = data(:,int_d).*-1;
    end
    
    %% Start counter
    data_counter = length(data(:,1)) + data_counter;
    
    %% select by number of scatterpixels > 1
    data = data((data(:,int_ns)>=1),:); 
    
    % Devide into data into starboard and portside
    side2 = find(data(:,int_b)>0); % side1: negative beam angle, side 2: positive beam angle
    data2 = data(side2,:);  % side 2
    data(side2,:) = [];     % side 1
   
   %% Define mean depth per ping to select number of pings for averaging
    depth_mean = NaN(size(data,int_p),1);
    for pings_idx = min(data(:,int_p)):1:max(data(:,int_p))
        if isempty(data(:,int_p)==pings_idx)==0
            depth_mean(data(:,int_p)==pings_idx) = nanmean((data(data(:,int_p)==pings_idx,int_d)));
        end
    end
    depth_mean2 = NaN(size(data2,1),1);
    for pings_idx2 = min(data2(:,int_p)):1:max(data2(:,int_p))
        if isempty(data2(:,int_p)==pings_idx2)==0
            depth_mean2(data2(:,int_p)==pings_idx2) = nanmean((data2(data2(:,int_p)==pings_idx2,int_d)));
        end
    end
        
    %% Start loop over angles
    for beam_counter = 1:length(theta_st)
        beam = theta_st(beam_counter);
        tol = tol2(beam_counter);  
        %% side 1          
        %% Selection of data around beam inbetween angle of tolerance angle (tol)       
        idx = find(abs((abs(data(:,int_b)))-beam)<=tol);
        datacut = data(idx,:);
        depth_mean_cut = depth_mean(idx,:);               
        if isempty(datacut)==0
        store_counter=1;
        B=[];        
        pings = min(datacut(:,int_p)):1:max(datacut(:,int_p));    
        pings_counter = 1;          % counter of pings
        last_ping_not_reached = 1;  % used to stop while loop
        while last_ping_not_reached == 1
            f = find(datacut(:,int_p)==pings(pings_counter));
            if isempty(f)
                pings_counter = pings_counter + 1;
                if pings_counter>=length(pings)
                    last_ping_not_reached = 0;
                end                
                continue
            end
            %%Find number of pings for averaging based on depth
            d = abs(depth_bound - depth_mean_cut(f(1)));            
            [I depth_ind]= min(d); % index of lower bound of depth
            ping_start = pings(pings_counter);
            ping_end = pings(pings_counter) + pings_num(depth_ind) - 1;
            pings_cut = ping_start:1:ping_end;
            idx = ismember(datacut(:,int_p),pings_cut);
            data_slope = datacut(idx,:);       
            if length(unique(data_slope(:,int_p)))<pings_num(depth_ind)                
                % leave out this ping
                if pings_counter>=length(pings)
                    last_ping_not_reached = 0;
                end
                dummy = [dummy ; datacut(find(datacut(:,int_p)==pings(pings_counter)),:)];
                pings_counter = pings_counter + 1;
            else
            A = [];
            %% Consider chaninging beam incident angle due to slope (added by Timo C. Gaida)            
            stop_idx = 1;            
            while stop_idx == 1                
                min_beam = min(abs(data_slope(:,int_ob)));
                idxx = find(abs(data_slope(:,int_ob)) < 2*tol+min_beam); 
                if length(data_slope(idxx,1)) < pings_num(depth_ind)
                    dummy = [dummy ; data_slope(idxx,:)];
                    data_slope(idxx,:)=[];
                    if length(data_slope(:,1)) < pings_num(depth_ind)
                        dummy = [dummy ; data_slope];
                        break
                    else
                        continue
                    end
                end
                % Filter backscatter data from beam angles using robust Z-scores (Rousseeuw and
                % Hubert 2011) before doing average over pings and beams
                data_slope_filter = data_slope(idxx,:);
                if length(idxx)>2
                idx_filter = find(abs((data_slope_filter(:,int_BS)-median(data_slope_filter(:,int_BS)))/(1.483*median(abs(data_slope_filter(:,int_BS)-median(data_slope_filter(:,int_BS)))))) > 2.5);
                data_slope_filter(idx_filter,:)=[];
                end                   
                A = [A ; nanmean(data_slope_filter(:,int_lon)); nanmean(data_slope_filter(:,int_lat)); nanmean(data_slope_filter(:,int_d)); nanmedian(data_slope_filter(:,int_BS)) ;sum(data_slope_filter(:,int_ns))];
                data_slope(idxx,:) = [];                
                if length(data_slope(:,1)) < pings_num(depth_ind)
                    stop_idx = 0;            
                end
            end            
            if isempty(A)== 1;
                pings_counter = pings_counter + 1; % pings_num(depth_ind);
                if pings_counter>=length(pings)
                    last_ping_not_reached = 0;
                end 
                continue
            end            
            %% Store data in B matrix            
            B(store_counter:length(A)+store_counter-1)=A;
            store_counter = store_counter+length(A);
            pings_counter = pings_counter + pings_num(depth_ind);
            end            
            if pings_counter>=length(pings)
                last_ping_not_reached = 0;
                if pings_counter>=length(pings)
                    last_ping_not_reached = 0;
                end 
            end          
        end
  
        %% interim save data        
        fid = fopen([dirDATA2,'sside1_beam',num2str(theta_st(beam_counter)),'mean.dat'],'a');
        B =B';
        fwrite(fid, B,'double');
        fclose(fid);
        clear B        
        end
  
        %% side 2          
        %% Selection of data around beam inbetween angle of tolerance angle (tol)
        idx2 = find(abs((data2(:,int_b))-beam)<=tol);   
        datacut2 = data2(idx2,:);
        depth_mean_cut2 = depth_mean2(idx2,:);        
         if isempty(datacut2)==0
             store_counter=1;
             B2=[];
             pings = min(datacut2(:,int_p)):1:max(datacut2(:,int_p));
             %NaN(length(pings),n_feature);   % data matrix       
             pings_counter = 1;          % counter of pings
             last_ping_not_reached = 1;  % used to stop while loop
         while last_ping_not_reached == 1
            f = find(datacut2(:,int_p)==pings(pings_counter));
            if isempty(f)
                pings_counter = pings_counter + 1;
                if pings_counter>=length(pings)
                    last_ping_not_reached = 0;
                end 
                continue
            end
            %%Find number of pings for averaging based on depth
            d = abs(depth_bound - depth_mean_cut2(f(1)));            
            [I depth_ind]= min(d); % index of lower bound of depth            
            ping_start = pings(pings_counter);
            ping_end = pings(pings_counter) + pings_num(depth_ind) - 1;
            pings_cut = ping_start:1:ping_end;            
            idx = ismember(datacut2(:,int_p),pings_cut);
            data_slope = datacut2(idx,:);         
            if length(unique(data_slope(:,int_p)))<pings_num(depth_ind)
                % leave out this ping
                if pings_counter>=length(pings)
                    last_ping_not_reached = 0;
                end
                dummy = [dummy ; datacut2(find(datacut2(:,int_p)==pings(pings_counter)),:)];
                pings_counter = pings_counter + 1;
            else
            A = [];
            %% Consider chaninging beam incident angle due to slope (added by Timo C. Gaida)            
            idxx2 = 0;
            stop_idx = 1;
            A = [];
            while stop_idx == 1              
                min_beam = min(abs(data_slope(:,int_ob)));
                idxx = find(abs(data_slope(:,int_ob)) < 2*tol+min_beam); 
                if length(data_slope(idxx,1)) < pings_num(depth_ind)
                    dummy = [dummy ; data_slope(idxx,:)];
                    data_slope(idxx,:)=[];
                    if length(data_slope(:,1)) < pings_num(depth_ind)
                        dummy = [dummy ; data_slope];
                        break
                    else
                        continue
                    end
                end
               % Filter backscatter data from patch using robust Z-scores (Rousseeuw and
                % Hubert 2011) before doing average over pings and beams
                data_slope_filter = data_slope(idxx,:);
                if length(idxx)>2
                idx_filter = find(abs((data_slope_filter(:,int_BS)-median(data_slope_filter(:,int_BS)))/(1.483*median(abs(data_slope_filter(:,int_BS)-median(data_slope_filter(:,int_BS)))))) > 2.5);
                data_slope_filter(idx_filter,:)=[];
                end                
                A = [A ; nanmean(data_slope_filter(:,int_lon)); nanmean(data_slope_filter(:,int_lat)); nanmean(data_slope_filter(:,int_d)); nanmedian(data_slope_filter(:,int_BS)) ;sum(data_slope_filter(:,int_ns))];
                data_slope(idxx,:) = [];                
                if length(data_slope(:,1)) < pings_num(depth_ind)
                    stop_idx = 0;            
                end
            end            
            if isempty(A)== 1;
                pings_counter = pings_counter + 1; % pings_num(depth_ind);
                if pings_counter>=length(pings)
                    last_ping_not_reached = 0;
                end 
                continue
            end
            % Store data in B matrix
            B2(store_counter:length(A)+store_counter-1)=A;
            store_counter = store_counter+length(A);
            pings_counter = pings_counter + pings_num(depth_ind); 
            end
                      
            if pings_counter>=length(pings)
                last_ping_not_reached = 0;
                if pings_counter>=length(pings)
                    last_ping_not_reached = 0;
                end 
            end    
         end   
        %% interim save data        
        fid = fopen([dirDATA2,'sside2_beam',num2str(theta_st(beam_counter)),'mean.dat'],'a');
        fwrite(fid, B2,'double');
        fclose(fid);
        clear B2
        end       
    end
end

%% save all data into files organized by beam angle and starbord and portack side
for beam_counter = 1:length(theta_st)
    theta_st(beam_counter)
    
    %% side 1
    lon_all = [];
    lat_all = [];
    Z_all = [];
    BS_all = [];
    No_all = [];     
    fid = fopen([dirDATA2,'sside1_beam',num2str(theta_st(beam_counter)),'mean.dat']);
    B = fread(fid,'double');
    fclose(fid);
    B = reshape(B,[n_feature,length(B)/n_feature]);
    lon_all = [B(1,:)'; lon_all]; 
    lat_all = [B(2,:)'; lat_all]; 
    Z_all = [B(3,:)'; Z_all]; 
    BS_all = [B(4,:)'; BS_all]; 
    No_all = [B(5,:)'; No_all];
    eval(['delete ',dirDATA2,'sside1_beam',num2str(theta_st(beam_counter)),'mean.dat']);
    clear B
    eval(['save ',dirMAP,'MAPmean_1_',num2str(theta_st(beam_counter)),'.mat',' lon_all lat_all Z_all BS_all No_all']);

    %% side 2
    lon_all = [];
    lat_all = [];
    Z_all = [];
    BS_all = [];
    No_all = [];    
    fid = fopen([dirDATA2,'sside2_beam',num2str(theta_st(beam_counter)),'mean.dat']);
    B2 = fread(fid,'double');
    fclose(fid);
    B2 = reshape(B2,[n_feature,length(B2)/n_feature]);   
    lon_all = [B2(1,:)'; lon_all]; 
    lat_all = [B2(2,:)'; lat_all]; 
    Z_all = [B2(3,:)'; Z_all]; 
    BS_all = [B2(4,:)'; BS_all]; 
    No_all = [B2(5,:)'; No_all];    
    eval(['delete ',dirDATA2,'sside2_beam',num2str(theta_st(beam_counter)),'mean.dat']);
    clear B2
    eval(['save ',dirMAP,'MAPmean_2_',num2str(theta_st(beam_counter)),'.mat',' lon_all lat_all Z_all BS_all No_all']);
end

data_perc = (length(dummy(:,1))/data_counter)*100;
disp([num2str(data_perc) '% of the data points are removed']);
