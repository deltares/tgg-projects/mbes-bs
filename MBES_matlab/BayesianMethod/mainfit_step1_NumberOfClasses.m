% function [numclass, stdmin, stdmax]=mainfit_step1_NumberOfClasses(dirMAP, ...
%     beam, side, latlon, depth, varargin)
close all; clear all; clc
toplot = true;
addpath ../matlabFun/generallib
addpath ../matlabFun/mbeslib

% This function estimates the number of classes in the backscatter strength 
% histogram of a multi-beam echosounder under a single beam angle and at a 
% single side.
%
% These data (averaged over a couple of pings and beams to account for 
% footprint effects) are read from MAPmean*.mat-files. 
% To obtain MAPmean*.mat-files, run BSmap_MEAN first.
%
% Step by step the number of classes is increased and the goodness of fit
% is determined by a chi^2 test, upon which an advice is given how many 
% classes should be input to the classification algorithm. The final
% choice is made by the user. 
%
% It is advisable to start this function with the outer beams (large beam
% angels or small depression angles), which have a better discrimination 
% ability.
%
% INPUT parameters are:
% dirMAP: string, directory in which MAP files containing BS per angle and 
%         side are stored
% OUTPUT parameters are:
% numclass: number of classes (Gaussians) advisable to use 
%           ATTENTION: a different choice might be appropriate depended on 
%                      the interpretetion of the results
% stdmin: minimum standarddeviation used to create the fitting Gaussians
% stdmax: maximum standarddeviation used to create the fitting Gaussians

%% Folders
dirMAP = 'G:\Ameland\2018\angle\';

%% Choose beam angles (incident angles in case slope correction is applied) for which fitting procedure is applied
%% Choose between a single beam or a range of beams to make the approach more robust (Timo C. Gaida) 
beam_int =  [65:-1:61 60:-2:52 50:-3:38];
% beam_int = [54];
% reference side: 1=port(default), 2=starbord
side = [1];

%% Choose the same beam angle range as choosen in BS_mean. 
%% Chose beam and pings intervalls manually. Check matlab script beam_distant
%% to consider amount of necessary scattering pixel
theta_st = [ 64:-1:54 53:-2:45 43:-3:30 27:-4:3 ];
pings_num = [10 4 3 2 1];
depth_bound = [0 5 10 15 30];
%% Choose the same beam angle range as choosen in BS_mean. 
%% Tolerances for bounds of beam angles. Averaging over angles will take place inbetween these bounds, 
%% to compensate different number of scatterpixels within differnt angles
tol2 = ones(size(theta_st));
tol2(theta_st>=61 & theta_st<=74)=0.6;
tol2(theta_st>=52 & theta_st<=60)=1.2;
tol2(theta_st>=35 & theta_st<=50)=1.8;
tol2(theta_st>=7 & theta_st<=32)=2.5;

%% Filter boundaries
scatterp = 5;    %number of scatter pixel
BSMIN = -50;    % lower BS bound
BSMAX = -5;     % upper BS bound
depth = [3 45]; % depth boundaries

%% MBES parameters
PL = (146*10^-6)*0.37;     % pulse length (see Processing_Kongsberg.m)
soundsp = 1500;            % Sound speed
Om = 1.3*pi/180;           % across track beam width (see Processing_Kongsberg.m)

%% testing the goodness of fit between the histogram and ng Gaussians
ng_start = 2; % minimum number of classes
ng_end = 8; % maximum number of classes 

%% decision on the upper and lower bound of the standard deviation %
%disp('The decision on the lower and upper bound of the standard deviation')
%disp('should be based on the actual standard deviation (green dots)')
%disp('displayed in the center plot in figure 1.')
stdmin = 0.5; 
stdmax = 2.5; 

%% Histogramm definition (Depends on backscatter value range)
hist_int = 0.5; % bin interval
hist_filter = 0.01; % filters out all bins in the histogram with number of BS values lower than hist_filter (0.01 = 1%)         

%% Visualization for detailed view on Gaussian fitting for a specific class and vessel side (see Figure 12)
vis_side = 1;  % starboard or portack side
vis_class = 4; % Class number

%% Initilize
kkk = 0;
ff =0;

%% Run fitting over beam angles and sides
for ii=1:length(beam_int)
    for jj=1:length(side)
        kkk = kkk+1;
        %% tol: tolerance value for referebce beam. tol +/- beam defines range of considerd
        [dummy idx] = min(abs(theta_st - beam_int(ii)));
        tol = tol2(idx);
        beam = theta_st(idx);
        %% LOAD DATA
       
        eval(['load ',dirMAP,'MAPmean_',num2str(side(jj)),'_',num2str(beam),'.mat']);
        BS_all=data(:,5);
        Z_all=data(:,4);
        lat_all=data(:,2);
        lon_all=data(:,3);
        No_all=data(:,6);
        %% Check if central limit theorem is valid
        I = find(No_all < scatterp);
        BS_all(I)=[]; No_all(I) = []; Z_all(I) = []; lat_all(I) = []; lon_all(I) =  [];
        clear I
        %% accounting for depth boundaries
        I = find(Z_all < depth(1) |  Z_all > depth(2));
        BS_all(I)=[]; No_all(I) = []; Z_all(I) = []; lat_all(I) = []; lon_all(I) =  [];
        clear I
        %% accounting for backscatter boundaries
        I = find(BS_all>BSMIN | BS_all<BSMAX);
        BS_all = BS_all(I);
        No_all = No_all(I);
        Z_all = Z_all(I);
        lat_all = lat_all(I);
        lon_all = lon_all(I);
        clear I
 
        %% determine the beam footprint %
        %% minimal an maximal beams of consideration (beams are defined by their 
        %% central beam angle)
        beam_min = beam - tol;
        beam_max = beam + tol;
        angle_min = beam_min*(pi/180);
        angle_max = beam_max*(pi/180);

        %% caclulate the actual coverage from all beams included
        %% acrosstrack size of beam footprint
        %% in case angle_min and angle_max are beam angel (not angle of incidence)
        %%Z_all = 80*ones(length(Z_all),1);
        dX_beam = Z_all*abs(tan(angle_min)-tan(angle_max)); 
        %% the following actually also accounts for alongtrack changes in size
        %% (however the component R*Om omitted since it cancels out with the same
        %% R*Om in the alongttrack size of the scatterpixels)
        dX_beam(Z_all<depth_bound(2)) = dX_beam(Z_all<depth_bound(2)) * pings_num(1);
        dX_beam(Z_all<depth_bound(3)&Z_all>=depth_bound(2)) = dX_beam(Z_all<depth_bound(3)&Z_all>=depth_bound(2)) * pings_num(2);
        dX_beam(Z_all<depth_bound(4)&Z_all>=depth_bound(3)) = dX_beam(Z_all<depth_bound(4)&Z_all>=depth_bound(3)) * pings_num(3);
        dX_beam(Z_all<depth_bound(5)&Z_all>=depth_bound(4)) = dX_beam(Z_all<depth_bound(5)&Z_all>=depth_bound(4)) * pings_num(4);
        dX_beam(Z_all>=depth_bound(5)) = dX_beam(Z_all>=depth_bound(5)) * pings_num(5);
        %% use average scatter pixel size over inner and outer beam
        dX_scatterpixel_min = (soundsp*PL)./(2*sin(angle_min));
        dX_scatterpixel_max = (soundsp*PL)./(2*sin(angle_max));
        dX_scatterpixel = mean([dX_scatterpixel_min dX_scatterpixel_max]);
        Ns = dX_beam./dX_scatterpixel;
        I = find(Ns < 1);
        Ns(I) = 1;
        % corresponding expected standard deviations
        T_std_1 = 10*pi*log10(exp(1))/sqrt(6);
        T_std_nsm = T_std_1./sqrt(No_all);
        T_std_Ns = T_std_1./sqrt(Ns);

        if toplot
            figure(58)
            clf
            hold on
            subplot(311)
            plot(Z_all,No_all,'.g');
            hold on
            plot(Z_all,Ns,'.r');
            h = get(gcf,'Children');
            set(h,'FontSize',14)
            xlabel('depth (m)');
            ylabel('no. scatterpixels/footprint');
            subplot(312)
            plot(BS_all,T_std_nsm,'.g');
            h = get(gcf,'Children');
            set(h,'FontSize',14);
            xlabel('BS (dB)');
            ylabel('theoretical std (dB)');
            subplot(313)
            plot(Z_all,T_std_nsm,'.g');
            hold on
            plot(Z_all,T_std_Ns,'.r');
            h = get(gcf,'Children');
            set(h,'FontSize',14);
            xlabel('depth (m) at beam footprint');
            ylabel('theoretical std (dB)');
            hold off
        end
 
        %% Find bins with low amount of BS values in backscatter histogram ( > 0.01 = 1% of)         
        x = BSMIN:hist_int:BSMAX;
        n = hist(BS_all,x);
        I = find(n > hist_filter*max(n));
        Data = [x(I)' n(I)'];
        t = Data(:,1);
        BSmin = min(x(I)); 
        BSmax = max(x(I));

        %% Visulize BS data per beam
        figure(56)
        hist(BS_all,x)
        xlim([BSmin BSmax])
        xlabel('backscatter strength [dB]')
        ylabel('number of measurements')
        %title(['beam angle ' num2str(beam) '\circ; Total number of measurements: ' num2str(length(BS_all))])
        
        %%Initilaize
        kk = 0;
        numclass=0;
        dummy = 0.001;
        test = 0; % value to check whether chi^2 test was successful
        
        %% loop over number of classes
        for ng = ng_start:ng_end 
            %% resolution BS data from MBES is 0.5 dB
            %% start solution non-linear parameters
            delta_BS = (BSmax-BSmin)/ng;
            BSm = BSmin + 0.5*delta_BS:delta_BS: BSmax - 0.5*delta_BS;
            BSm = BSm';    
            lam0 = [1*ones(length(BSm),1) BSm];
            global c;
            %% search intervals
            %% originally +-5, however this might cause the Gaussians to switch
            %% place what hampers finding the boundaries
            %% therefore, for the Cleaverbank dataset +-2
            ds = delta_BS/2;
            lb = [stdmin*ones(length(BSm),1) BSm-ds];
            ub = [stdmax*ones(length(BSm),1) BSm+ds];
    
            %% Solve Gaussian fitting
            options = optimset('lsqnonlin');
            options = optimset(options,'Algorithm','levenberg-marquardt');
            options = optimset(options,'MaxFunEvals',300000);
            options = optimset(options,'MaxIter',30000000);
            options = optimset(options,'TolX',1e-4);
            [lam,resnorm,residual,exitflag,output]= lsqnonlin('fitfunctionAliReza',...
            zeros(size(lam0)),lb,ub,options,Data);
            %% assessment of fit (visual)
            y(:,1) = c(1)*exp(-((t - lam(1,2)).^2)/(2*lam(1,1)^2));
            yfit   = c(1)*exp(-((t - lam(1,2)).^2)/(2*lam(1,1)^2));
            for j = 2 : length(BSm)
                y(:,j) = c(j)*exp(-((t - lam(j,2)).^2)/(2*lam(j,1)^2));
                yfit = yfit + y(:,j);
            end
    
            if toplot
                figure(ii)
                subplot(2,4,ng)
                hold on
                if ng == 2
                    errorbar(Data(:,1),Data(:,2),sqrt(Data(:,2)),'b')
                else
                    plot(Data(:,1),Data(:,2),'b')
                end
                h = get(gca,'Children');
                set(h,'LineWidth',1)
                h = get(gcf,'Children');
                set(h,'LineWidth',1)
                set(h,'FontSize',14)
                title([num2str(ng) ' Gaussians'], 'FontSize', 11)
                xlabel('y_j [dB]')
                ylabel('n_j')
                xlim([-50 -10])
                grid
                plot(t,yfit,'r','LineWidth',2)
                for j = 1 : length(BSm)
                    hold on
                    plot(t,y(:,j),'k','LineWidth',0.5)
                end
                hold off
                grid
            end
    
            figure(12)     
            if ng == vis_class && side(jj) == vis_side
                errorbar(Data(:,1),Data(:,2),sqrt(Data(:,2)),'b')
               % plot(Data(:,1),Data(:,2),'b','LineWidth',3)
                hold on
                h = get(gca,'Children');
                set(h,'LineWidth',1)
                h = get(gcf,'Children');
                set(h,'LineWidth',2)
                set(h,'FontSize',26)
                xlabel('Backscatter[dB]')
                ylabel('Number of measurements')
                xlim([-40 -15])           
                plot(t,yfit,'r--','LineWidth',3)
                for j = 1 : length(BSm)
                   hold on
                    plot(t,y(:,j),'k','LineWidth',2)
                end
            end
            
            if toplot
                lam;
            end
            %% reduced chi-square
            nu = (length(Data(:,2))-3*length(BSm));
            stdnu(ng-ng_start+1) = sqrt(2/nu);
            chi2r(ng-ng_start+1) = sum( ( (yfit - Data(:,2)).^2)./Data(:,2)  )/nu;
            storageChi2r(kkk,ng-ng_start+1) =  sum( ( (yfit - Data(:,2)).^2)./Data(:,2)  )/nu;
            if toplot
                figure(ii)
                subplot(2,4,1)
                hold on
                semilogy([ng_start:1:ng],chi2r,'x-b','LineWidth',2)
                h = get(gcf,'Children'); set(h,'FontSize',14); ...
                xlabel('number of Gaussians'); ylabel('\chi^{2}_{\nu}')
                title(['beam ' num2str(beam) ', side ' num2str(side(jj))],...
                'FontSize', 12, 'FontName', 'Courier')
%                 ylim([0 10])
            end
            alpha = 0.05;
            critical = 1;
            if toplot
                figure(ii)        
                plot([ng_start ng],critical,'--r')
                hold on
                %ylim([0 30])      
            end  
    
            if length(chi2r) > 1
                if  1-(chi2r(ng-1)/chi2r(ng-2)) <=0.1  && test==0  
                    numclass=ng-1;
                    dummy = chi2r(ng-2);
                    test=1;
                elseif chi2r(end)<critical && test==0
                    numclass=ng;
                    test=1;
                elseif 1-chi2r(end)/dummy > (0.1 + (ng-numclass)*0.03) && test==1 
                    numclass=ng;
                    dummy = chi2r(end);
                    test = 1;             
                end
            end
    end
    ff = ff+1;
    %plotStyle = {'b-','b:','r-' ,'r:','k-','k:','y-' ,'y:','c-','c:','m-' ,'m:','g-' ,'g:','b--','b-.','r--' ,'r-.','k--','k-.','y--' ,'y-.','c--','c-.','m--' ,'m-.','g--' ,'g-.','b-','b:','r-' ,'r:','k-','k:','y-' ,'y:','c-','c:','m-' ,'m:','g-' ,'g:','b--','b-.','r--' ,'r-.','k--','k-.','y--' ,'y-.','c--','c-.','m--' ,'m-.','g--' ,'g-.'};
     plotStyle = {'k','k','k','k','k','k','k','k','k','k','k','k','k','k','k','k','k','k','k','k','k','k','k','k','k','k','k','k','k','k','k','k','k','k','k','k','k','k','k','k','k','k','k','k','k','k','k','k','k','k','k','k','k','k','k','k','k','k','k','k','k','k','k','k','k','k','k','k','k','k','k','k','k','k','k','k','k','k','k','k','k','k'};
     figure(59)
     semilogy([ng_start:1:ng],chi2r,plotStyle{ff},'LineWidth',2)
     hold on
     grid on
     set(gca,'minorGridAlpha',1)
     set(gca,'GridAlpha',1)
     set(gca,'GridLineStyle', ':')
     set(gca,'minorGridLineStyle', ':')
     h = get(gcf,'Children'); set(h,'FontSize',48); ...
     xlabel('Number of Gaussians'); ylabel('\chi^{2}_{\nu}')
    
    if test==0
        disp('The critical value was not reached.')
        disp('However, convergency might be such that the number of classes')
        disp('can be selected from Fig. 3.')
    else
        disp(['The optimal number of classes was found as ',num2str(numclass),'.'])
        disp('However, according to Fig. 3. you might want alter this choice.')
        figure(ii)
        plot(numclass,chi2r(numclass-1),'ro','Markersize',12)
        hold off
    end

    if numclass > 0
        kk=kk+1;
        total_numclass(kk) = numclass;
        beam_range(kk) = beam;
        if side(jj) ==1
            total_numclass2(kk) = numclass+0.1;
        else
            total_numclass2(kk) = numclass-0.1;
        end
    end
clear t 
clear y 
clear chi2r
  end

end

figure(60)
hist(total_numclass,[2 3 4 5 6 7])
xlabel('number of Gaussians [-]')
ylabel('sample counts [-]')
%title('beam angles between 74 and 35 degree')

figure(61)
plot(beam_range, total_numclass2,'k.','Markersize',12)
ylim([2 8])
xlabel('beam angle [degree]')
ylabel('number of Gaussians [-]')


%% Calculate avergae Chi2 values for all considered beam angles including standard deviation.
%% If lower end of standard deviation reaches thresshold value (stdnu) this number is taken as numbre of
%% seaflor type which can bes distiguished in survey area (See Gaida et al. (2018))
Chi_mean = mean(storageChi2r);
Chi_std = std(storageChi2r);
stdnu = mean(stdnu);
nGaus =  ng_start:ng_end;
figure(65)
errorbar(nGaus,Chi_mean,Chi_std,'linewidth',2) 
hold on
figure(65)
ylim([0 10])
xlim([2 8])
xlabel('Number of Gaussians')
 ylabel('\chi^{2}_{\nu}')
grid on
set(gca,'fontsize',14)
hhh = ones(length(nGaus),1);

%errorbar(nGaus,hhh,stdnu,'--','Linewidth',1) 
%plot(nGaus,hhh,'k')
plot(nGaus,hhh+stdnu,'--k','Linewidth',1)
plot(nGaus,hhh,'--k','Linewidth',1)
plot(nGaus,hhh-stdnu,'--k','Linewidth',1)

