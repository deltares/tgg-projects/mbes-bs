%function mainfit_step2_ClassifyCheck(dirMAP, beam, side, numclass, stdmin, stdmax, latlon, depth, varargin)
close all
clear all
clc
toplot = true;
addpath ../matlabFun/generallib
addpath ../matlabFun/mbeslib

% This function assignes the backscatter strength of a multi-beam echosounder 
% under a single beam angle and at a single side to a fixed number of classes, 
% as is estimated by mainfit_step1_NumberOfClasses.m.
% ATTENTION: THIS PROGRAM NEEDS TO BE RUN FOR ALL REFERENCE ANGLES USED IN
% STEP3.
%
% It is advisable to start this function with the outer beams (large beam
% angels or small depression angles), which have a better discrimination 
% ability.
%
% INPUT parameters are:
% dirMAP: string, directory in which MAP files containing BS per angle and 
%         side are stored, e.g. 
%         dirMAP = 'D:\kherms\Documents\MBES\DATAKlaverbank2014\MAPfiles\';
% beam: the beam angle in degrees, interger value 
%       (90 degrees at the horizontal, 0 degrees at nadir direction)
%       as in the filename of the BSmap_MEAN files
% side: 1=port(default), 2=starbord
% numclass: number of classes used in the current classification 
%           (as determined from mainfit_step2_NumberOfClasses.m)
% stdmin: minimum standarddeviation used to create the fitting Gaussians
%         (as determined from mainfit_step2_NumberOfClasses.m)
% stdmax: minimum standarddeviation used to create the fitting Gaussians
%         (as determined from mainfit_step2_NumberOfClasses.m)
% depth: vector containing the POSITIVE(!) depth boundaries in the form
%        [depthmin depthmax]
%        (used to remove outliers)


%% Folders
dirMAP = 'G:\Ameland\2018\angle\';
dirMAP2 = 'G:\Ameland\2018\angle\step2\';

%% Set parameters
beam = 62;      % Reference beam angle
side = [1];
numclass = 5;   % Optimal number of classes idenitfied in Step 1

%% decision on the upper and lower bound of the standard deviation %
%disp('The decision on the lower and upper bound of the standard deviation')
%disp('should be based on the actual standard deviation (green dots)')
%disp('displayed in the center plot in figure 1.')
stdmin = 0.5; 
stdmax = 2.5; 

%% Filter boundaries
scatterp = 5;    %number of scatter pixel
BSMIN = -50;    % lower BS bound
BSMAX = -5;     % upper BS bound
depth = [3 45]; % depth boundaries

%% Histogramm definition (Depends on backscatter value range)
hist_int = 0.5; % bin interval
hist_filter = 0.01; % filters out all bins in the histogram with number of BS values lower than hist_filter (0.01 = 1%)         


%% LOAD DATA 
eval(['load ',dirMAP,'MAPmean_',num2str(side),'_',num2str(beam),'.mat']);
        BS_all=data(:,5);
        Z_all=data(:,4);
        lat_all=data(:,2);
        lon_all=data(:,3);
        No_all=data(:,6);
%% Check if central limit theorem is valid
I = find(No_all < scatterp);
BS_all(I)=[]; No_all(I) = []; Z_all(I) = []; lat_all(I) = []; lon_all(I) =  [];
clear I
%% accounting for depth boundaries
% I = find(Z_all < depth(1) |  Z_all > depth(2));
% BS_all(I)=[]; No_all(I) = []; Z_all(I) = []; lat_all(I) = []; lon_all(I) =  [];
% clear I
%% accounting for backscatter boundaries
I = find(BS_all>BSMIN | BS_all<BSMAX);
BS_all = BS_all(I);
No_all = No_all(I);
Z_all = Z_all(I);
lat_all = lat_all(I);
lon_all = lon_all(I);
clear I

%% Find bins with low amount of BS values in backscatter histogram ( > 0.01 = 1% of)         
x = BSMIN:hist_int:BSMAX;
n = hist(BS_all,x);
I = find(n > hist_filter*max(n));
Data = [x(I)' n(I)'];
t = Data(:,1);
BSmin = min(x(I)); 
BSmax = max(x(I));

%% testing the goodness of fit between the histogram and ng Gaussians  
ng = numclass;
    if toplot
        figure(2)
        hold on
        %bar(x,n)
        errorbar(Data(:,1),Data(:,2),sqrt(Data(:,2)),'k')
        h = get(gca,'Children');
        set(h,'LineWidth',1)
        h = get(gcf,'Children');
        set(h,'LineWidth',1)
        set(h,'FontSize',14)
        xlabel('y_j [dB]')
        ylabel('n_j')
        grid
    end
    %% start solution non-linear parameters
    delta_BS = (BSmax-BSmin)/ng;
    BSm = BSmin + 0.5*delta_BS:delta_BS: BSmax - 0.5*delta_BS; % resolution BS data from MBES is 0.5 dB
    BSm = BSm';
    
    % initial solution
    lam0 = [1*ones(length(BSm),1) BSm];
    % linear parameters
    global c;
    % search intervals
    % originally +-5, however this might cause the Gaussians to switch
    % place what hampers finding the boundaries
    % therefore, for the Cleaverbank dataset +-2
    ds = delta_BS/2;    
    lb = [stdmin*ones(length(BSm),1) BSm-ds];
    ub = [stdmax*ones(length(BSm),1) BSm+ds];
    
    %% solve
    options = optimset('lsqnonlin');
    options = optimset(options,'Algorithm','levenberg-marquardt');
    options = optimset(options,'MaxFunEvals',300000);
    options = optimset(options,'MaxIter',30000000);
    options = optimset(options,'TolX',1e-4);
    [lam,resnorm,residual,exitflag,output]= lsqnonlin('fitfunctionAliReza',zeros(size(lam0)),lb,ub,options,Data);
    y(:,1) = c(1)*exp(-((t - lam(1,2)).^2)/(2*lam(1,1)^2));
    yfit   = c(1)*exp(-((t - lam(1,2)).^2)/(2*lam(1,1)^2));
    for j = 2 : length(BSm)
        y(:,j) = c(j)*exp(-((t - lam(j,2)).^2)/(2*lam(j,1)^2));
        yfit = yfit + y(:,j);
    end
    
    if toplot
        figure(2)
        hold on
        plot(t,yfit,'-r','LineWidth',2)
        for j = 1 : length(BSm)
            hold on
            plot(t,y(:,j),'k','LineWidth',0.5)
        end
        hold off
        grid  
        lam
    end
    %% reduced chi-square
    nu = (length(Data(:,2))-3*length(BSm));    
    chi2r = sum( ((yfit - Data(:,2)).^2)./Data(:,2)  )/nu
    %% plot classes
    clear y; 
    dt = 0.01; t = [min(t) : dt : max(t)]';
    if toplot
        figure(3);
        hold on;
    end
    for j = 1 : length(BSm)
         y(:,j) = (1/(sqrt(2*pi)*lam(j,1) ) )* exp(-((t - lam(j,2)).^2)/(2*lam(j,1)^2));
%           y(:,j) = c(j)* exp(-((t - lam(j,2)).^2)/(2*lam(j,1)^2));
        if toplot
            plot(t,y(:,j),'-b')
        end
    end
    if toplot
        grid
        h = get(gca,'Children');
        set(h,'LineWidth',1.5)
        h = get(gcf,'Children');
        set(h,'LineWidth',1)
        set(h,'FontSize',14)
        xlabel('Backscatter strength [dB]')
        ylabel('Probability density function')
        grid
    end
    %% Determine class boundaries
    clear I
    if toplot
        figure(3);
        hold on
    end
    
    for j = 1 : length(BSm)-1
        J = find(t>lam(j,2) & t < lam(j+1,2));
        [dummy,I(j)] =  min( abs(y(J,j) - y(J,j+1))  );
        I(j) = I(j) + J(1)-1;
        if toplot
            plot(t(I(j)),y(I(j),j),'ksq','MarkerSize',8,'MarkerFaceColor',[0 0 0])
        end
    end
    
    M = lam(:,2); % class mean values
    S = lam(:,1); % class standarddeviation values
    B = [-Inf t(I)' Inf]; % class backscatter boundaries
    meanBS = mean(BS_all);

    eval(['save ',dirMAP2,'Bmean_',num2str(side),'_',num2str(beam),'_',num2str(numclass),' M S B meanBS'])

    %% assign backscatter to classes
    I1 = find(BS_all > B(1) & BS_all < B(2));
    if ng>1
    I2 = find(BS_all > B(2) & BS_all < B(3));
    end
    if ng>2
    I3 = find(BS_all > B(3) & BS_all < B(4));
    end
    if ng>3
    I4 = find(BS_all > B(4) & BS_all < B(5));
    end
    if ng>4
    I5 = find(BS_all > B(5) & BS_all < B(6));
    end
    if ng>5
    I6 = find(BS_all > B(6) & BS_all < B(7));
    end
    if ng>6
    I7 = find(BS_all > B(7) & BS_all < B(8));
    end
    
  