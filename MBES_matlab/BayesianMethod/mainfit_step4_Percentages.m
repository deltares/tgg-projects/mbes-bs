%function mainfit_step4_Percentages(dirMAP, beams, side, numclass, stdmin,...
 %   stdmax, BSclassmean, BSmean, Pmean, latlon, depth, varargin)
 close all;
 clear all;
 clc;
% This function assignes the backscatter strength of a multi-beam echosounder
% for all beams
% to a fixed number of classes, as is estimated by mainfit_step1_NumberOfClasses.m.
%
% This function is similar to mainfit_step2_ClassifyCheck.m. However, in
% order to make the classification results comparable for the different beams,
% backscatter strength values at these beams are shifted. Further, constaints
% are added to the class mean values (position on the backscatter axis) to
% obtain similar classes.
% This requires to run mainfit_step2_ClassifyCheck.m for the
% angles under consitderation, first.
%
% INPUT parameters are:
% dirMAP: string, directory in which MAP files containing BS per angle and
%         side are stored, e.g.
%         dirMAP = 'D:\kherms\Documents\MBES\DATAKlaverbank2014\MAPfiles\';
% beams !!! (VECTOR) !!!: the beam angle in degrees, vector of interger values containing
%       those beams which (after steps 1-3) still need to be processed
%       (90 degrees at the horizontal, 0 degrees at nadir direction)
%       as in the filename of the BSmap_MEAN files
% side: 1=port(default), 2=starbord
% numclass: number of classes used in the current classification
%           (as determined from mainfit_step2_NumberOfClasses.m)
% stdmin: minimum standarddeviation used to create the fitting Gaussians
%         (as determined from mainfit_step2_NumberOfClasses.m)
% stdmax: minimum standarddeviation used to create the fitting Gaussians
%         (as determined from mainfit_step2_NumberOfClasses.m)
% BSclassmean
% BSmean
% Pmean
% depth: vector containing the POSITIVE(!) depth boundaries in the form
%        [depthmin depthmax]
%        (used to remove outliers)

% OUTPUT parameters are:
% BSclassmean: mean values of the classes at reference beams
% BSmean: mean of all BS values (in all classes) at reference beams
% these will be used in step 4, where other the classification is done for 
% the remaining beams


%% Folders
dirMAP = 'G:\Ameland\2018\angle\';
dirMAP2 = 'G:\Ameland\2018\angle\step3\';
dirMAP3 = 'G:\Ameland\2018\angle\step4\';

%% Select beam angle range for the final classification (In general 65 to 10 degrees is recommned.
beams = [ 64:-1:54 53:-2:45 43:-3:30 27:-4:11];
beams = [ 64:-1:54 53:-2:45 43:-3:30];

%% Set parameters
%  refbeams = [54 56 58];      % Reference beam angle
  refbeams = [60 62 64];      % Reference beam angle
% refbeams = [40 43 47];      % Reference beam angle
refbeams=64;
side = [1 2];
numclass = 5;   % Optimal number of classes idenitfied in Step 1

%% decision on the upper and lower bound of the standard deviation %
%disp('The decision on the lower and upper bound of the standard deviation')
%disp('should be based on the actual standard deviation (green dots)')
%disp('displayed in the center plot in figure 1.')
stdmin = 0.5; 
stdmax = 2.5; 

%% Filter boundaries
scatter = 5;    %number of scatter pixel
BSMIN = -50;    % lower BS bound
BSMAX = -5;     % upper BS bound
depth = [3 45]; % depth boundaries

%% Histogramm definition (Depends on backscatter value range)
hist_int = 0.5; % bin interval
hist_filter = 0.01; % filters out all bins in the histogram with number of BS values lower than hist_filter (0.01 = 1%) 



% Initialize
dummy1=[];
dummy2=[];
dummy3=[];
dummy4=[];
dummy5=[];
% LOAD DATA %
%%%%%%%%%%%%%


for k=1:length(beams)
   for ii=1:length(side)
        beam = beams(k);
        eval(['load ',dirMAP,'MAPmean_',num2str(side(ii)),'_',num2str(beam)])    
        load([dirMAP2 'refbeam_values_',num2str(refbeams),'_side_',num2str(side(ii)),'_class',num2str(numclass),'.mat']);
        BS_all=data(:,5);
        Z_all=data(:,4);
        lat_all=data(:,2);
        lon_all=data(:,3);
        No_all=data(:,6);
        %% Check if central limit theorem is valid
        I = find(No_all < scatter);
        BS_all(I)=[]; No_all(I) = []; Z_all(I) = []; lat_all(I) = []; lon_all(I) =  [];
        clear I
        %% accounting for depth boundaries
        I = find(Z_all < depth(1) |  Z_all > depth(2));
        BS_all(I)=[]; No_all(I) = []; Z_all(I) = []; lat_all(I) = []; lon_all(I) =  [];
        clear I
        %% accounting for backscatter boundaries
        I = find(BS_all>BSMIN | BS_all<BSMAX);
        BS_all = BS_all(I);
        No_all = No_all(I);
        Z_all = Z_all(I);
        lat_all = lat_all(I);
        lon_all = lon_all(I);
        clear I
 
        %% Filter backscatter data based data which was used for fitting 
        x = BSMIN:hist_int:BSMAX;
        n = hist(BS_all,x);
        I = find(n >hist_filter*max(n));  
        try
            bs_low = x(min(I)-1);
        catch
            bs_low = x(min(I));
        end
        
        
        try
            bs_up = x(max(I)+1);
        catch
            bs_up=x(max(I));
        end
        
        
        I = find(BS_all>bs_up | BS_all<bs_low);
        BS_all(I)=[]; No_all(I) = []; Z_all(I) = []; lat_all(I) = []; lon_all(I) =  [];
        clear I
        
        %% sort by increasing BS values
        [BS_sort,I0] = sort(BS_all);
        lat_all = lat_all(I0);
        lon_all = lon_all(I0);
        BS_all = BS_all(I0);
        Z_all = Z_all(I0);
    
        %% select classes by percentage values
        ind_ub=0; % upper index
        sizeBS = size(BS_all,1);
        lat_dat=[];
        lon_dat=[];
        BS_dat=[];
        Z_dat=[];
        class_dat=[];
        Bb = zeros(1,numclass+1);
        Bb(1) = -Inf;
        Bb(numclass+1) = +Inf;
        for l=1:numclass
            ind_lb = ind_ub+1;        
            ind_ub = ind_ub + floor(Pmean(l)*sizeBS);
            clear I
            I=ind_lb:1:ind_ub;
            lat_dat = [lat_dat; lat_all(I)];
            lon_dat = [lon_dat; lon_all(I)];
            BS_dat = [BS_dat; BS_all(I)];
            Z_dat = [Z_dat; Z_all(I)];
            class_dat = [class_dat; l*ones(size(lat_all(I)))];
        
            %% Define boundaries for each beam angle
            if l>1 && l < numclass+1
                Bb(l) = min(BS_all(I));
            end
        end
    
        dummy1 = [dummy1; lon_dat];
        dummy2 = [dummy2; lat_dat];
        dummy3 = [dummy3; Z_dat];
        dummy4 = [dummy4; BS_dat];
        dummy5 = [dummy5; class_dat];
    
       % save([dirMAP3 'BSbound_classes_',num2str(numclass),'beam_',num2str(beam), '_side_',num2str(side(ii))],'Bb','-ascii'); 
   end   
end

A = [dummy1 dummy2 dummy5];
% save([dirMAP3 'classes_',num2str(numclass),'beams_ref2range_side12.txt'],'A','-ascii');
save([dirMAP3 'classes_',num2str(numclass),'beams_ref2range_side12__.mat'],'A','-v7.3');