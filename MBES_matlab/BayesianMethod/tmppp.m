    for beam_counter = 1:length(theta_st)
        beam = theta_st(beam_counter);
        tol = tol2(beam_counter);  
        %% side 1          
        %% Selection of data around beam inbetween angle of tolerance angle (tol)       
        idx = find(abs((abs(data(:,int_b)))-beam)<=tol);
        datacut = data(idx,:);
        depth_mean_cut = depth_mean(idx,:);               
        if isempty(datacut)==0
        store_counter=1;
        B=[];        
        pings = min(datacut(:,int_p)):1:max(datacut(:,int_p));    
        pings_counter = 1;          % counter of pings
        last_ping_not_reached = 1;  % used to stop while loop
        while last_ping_not_reached == 1
            f = find(datacut(:,int_p)==pings(pings_counter));
            if isempty(f)
                pings_counter = pings_counter + 1;
                if pings_counter>=length(pings)
                    last_ping_not_reached = 0;
                end                
                continue
            end
            %%Find number of pings for averaging based on depth
            d = abs(depth_bound - depth_mean_cut(f(1)));            
            [I,depth_ind]= min(d); % index of lower bound of depth
            ping_start = pings(pings_counter);
            ping_end = pings(pings_counter) + pings_num(depth_ind) - 1;
            pings_cut = ping_start:1:ping_end;
            idx = ismember(datacut(:,int_p),pings_cut);
            data_slope = datacut(idx,:);       
            if length(unique(data_slope(:,int_p)))<pings_num(depth_ind)                
                % leave out this ping
                if pings_counter>=length(pings)
                    last_ping_not_reached = 0;
                end
                dummy = [dummy ; datacut(find(datacut(:,int_p)==pings(pings_counter)),:)];
                pings_counter = pings_counter + 1;
            else
            A = [];
            %% Consider chaninging beam incident angle due to slope (added by Timo C. Gaida)            
            stop_idx = 1;            
            while stop_idx == 1                
                min_beam = min(abs(data_slope(:,int_ob)));
                idxx = find(abs(data_slope(:,int_ob)) < 2*tol+min_beam); 
                if length(data_slope(idxx,1)) < pings_num(depth_ind)
                    dummy = [dummy ; data_slope(idxx,:)];
                    data_slope(idxx,:)=[];
                    if length(data_slope(:,1)) < pings_num(depth_ind)
                        dummy = [dummy ; data_slope];
                        break
                    else
                        continue
                    end
                end
                % Filter backscatter data from beam angles using robust Z-scores (Rousseeuw and
                % Hubert 2011) before doing average over pings and beams
                data_slope_filter = data_slope(idxx,:);
                if length(idxx)>2
                idx_filter = find(abs((data_slope_filter(:,int_BS)-median(data_slope_filter(:,int_BS)))/(1.483*median(abs(data_slope_filter(:,int_BS)-median(data_slope_filter(:,int_BS)))))) > 2.5);
                data_slope_filter(idx_filter,:)=[];
                end                   
                A = [A ; nanmean(data_slope_filter(:,int_lon)); nanmean(data_slope_filter(:,int_lat)); nanmean(data_slope_filter(:,int_d)); nanmedian(data_slope_filter(:,int_BS)) ;sum(data_slope_filter(:,int_ns))];
                data_slope(idxx,:) = [];                
                if length(data_slope(:,1)) < pings_num(depth_ind)
                    stop_idx = 0;            
                end
            end            
            if isempty(A)== 1;
                pings_counter = pings_counter + 1; % pings_num(depth_ind);
                if pings_counter>=length(pings)
                    last_ping_not_reached = 0;
                end 
                continue
            end            
            %% Store data in B matrix            
            B(store_counter:length(A)+store_counter-1)=A;
            store_counter = store_counter+length(A);
            pings_counter = pings_counter + pings_num(depth_ind);
            end            
            if pings_counter>=length(pings)
                last_ping_not_reached = 0;
                if pings_counter>=length(pings)
                    last_ping_not_reached = 0;
                end 
            end          
        end
  
        %% interim save data        
        fid = fopen([dirDATA2,'sside1_beam',num2str(theta_st(beam_counter)),'mean.dat'],'a');
        B =B';
        fwrite(fid, B,'double');
        fclose(fid);
        clear B        
        end

    end