# -*- coding: utf-8 -*-
"""
Created on Fri Jan 25 15:34:17 2019

@author: karaouli
"""

from pyall import ALLReader
import numpy as np
import pandas as pd
import time
from datetime import datetime
from datetime import timedelta
import pyproj
import matplotlib.pyplot as plt
from scipy.interpolate import interp1d


wgs84_p = pyproj.Proj(init='epsg:4326')
utm_xy_31 = pyproj.Proj(init='epsg:32631') 





# * The function mb_coor_scale.c returns scaling factors
# * to turn longitude and latitude differences into distances in meters.
# * This function is based on code by James Charters (Scripps Institution
# * of Oceanography).
# * Author:	D. W. Caress
# * Date:	January 21, 1993
C1 =111412.84
C2 =-93.5
C3 =0.118
C4 =111132.92
C5 =-559.82
C6 =1.175
C7 =0.0023

start_time = time.time() # time the process
#r = ALLReader(r'D:\karaouli\Desktop\Projects\backscattered\DataAmeland2017\all_files\0001_20170905_184833_ARCA.all')
r = ALLReader('D:\\karaouli\\Desktop\\Projects\\backscattered\\DataAmeland2017\\all_files\\3\\0045_20170907_032433_ARCA.all')

navigation = np.asarray(r.loadNavigation())

# make here interpolation for lat,lon\
radlat =  np.deg2rad(navigation[:,1])
mtodeglat = 1. / np.abs(C4 + C5 * np.cos(2 * radlat) + C6 * np.cos(4 * radlat) + C7 * np.cos(6 * radlat));
mtodeglon = 1. / np.abs(C1 * np.cos(radlat) + C2 * np.cos(3 * radlat) + C3 * np.cos(5 * radlat));
#dx=(navigation[1:,1]-navigation[:-1,1])/mtodeglon[1:]
#dy=(navigation[1:,2]-navigation[:-1,2])/mtodeglon[1:]
#dt=navigation[1:,0]-navigation[:-1,0]
#speed=3.6*np.sqrt(np.power(dx,2)+np.power(dy,2))/dt

flat=interp1d(navigation[:,0],navigation[:,1],kind='linear',fill_value='extrapolate')
flon=interp1d(navigation[:,0],navigation[:,2],kind='linear',fill_value='extrapolate')

            

# preallocation of memory, not used right now. It seems concat from pandas is equal fast
pingCount_N=0
pingCount_X=0
pingCount_Y=0
while r.moreData():
    typeOfDatagram, datagram = r.readDatagram()
    # we read the P(Position)
    if typeOfDatagram == 'N':
        pingCount_N=pingCount_N+1
    if typeOfDatagram == 'X':
        pingCount_X=pingCount_X+1
    if typeOfDatagram == 'Y':
        pingCount_Y=pingCount_Y+1        
     
# Check here that datagrams have the same size. If not eoore
pingCount=np.unique(np.c_[pingCount_N,pingCount_X,pingCount_Y])
if pingCount.shape[0]==1:
    print('All Datagrams found')
    memory=(pingCount[0])*256
else:
    print('X=%d, Y=%d, N=%d',(pingCount_X,pingCount_Y,pingCount_N))
memoryx=(pingCount_X)*256
memoryy=(pingCount_Y)*256
memoryn=(pingCount_N)*256
    

# matrices to keep for index
id_x=np.zeros((pingCount_X,2))    
id_y=np.zeros((pingCount_Y,2))    
id_n=np.zeros((pingCount_N,2))    
# reset counter
pingCount_N=0
pingCount_X=0
pingCount_Y=0



# For now, I will store everything in 3 different matrices. It should never happen, but the idea is that we have a triple
# of N,X,Y datagrams, that belong on the same measure. If this is not the case, all hell break loose and we have to find
# what is where
#data_x=pd.DataFrame()
#data_y=pd.DataFrame()
#data_n=pd.DataFrame()

data_x=np.zeros((memoryx,10))
data_y=np.zeros((memoryy,4))
data_n=np.zeros((memoryn,8))

r.rewind()



m256=np.ones((256,))

# Remember, in the begging there is no lat, lon for 1-2 pings
lat=0
lon=0
utmx=0
utmy=0
height=0
radlat =   np.deg2rad(lat)
mtodeglat = 1. / np.abs(C4 + C5 * np.cos(2 * radlat) + C6 * np.cos(4 * radlat) + C7 * np.cos(6 * radlat));
mtodeglon = 1. / np.abs(C1 * np.cos(radlat) + C2 * np.cos(3 * radlat) + C3 * np.cos(5 * radlat));
height=0

pingCount2=1
CountX=0
CountN=0
CountY=0


fix_lon=0
fix_lat=0
t_fix=0
#keep always two pings
old_lon=np.copy(fix_lon)
old_lat=np.copy(fix_lon)
old_t=np.copy(t_fix)




while r.moreData():
    typeOfDatagram, datagram = r.readDatagram()
    rawbytes = r.readDatagramBytes(datagram.offset, datagram.numberOfBytes)
    
    # we read the P(Position)
    if typeOfDatagram == 'P':

        # Before datagram read, keep old values
        datagram.read()
        lat=datagram.Latitude
        lon=datagram.Longitude
        utmx,utmy = pyproj.transform(wgs84_p,utm_xy_31,lon,lat)
        radlat =  np.deg2rad(lat)
        mtodeglat = 1. / np.abs(C4 + C5 * np.cos(2 * radlat) + C6 * np.cos(4 * radlat) + C7 * np.cos(6 * radlat));
        mtodeglon = 1. / np.abs(C1 * np.cos(radlat) + C2 * np.cos(3 * radlat) + C3 * np.cos(5 * radlat));
             
        
        

    if typeOfDatagram == 'h':
        datagram.read()
        height=datagram.Height
     
    
    # we read the Raw range and angle
    if typeOfDatagram == 'N':
        datagram.read()
        CsT_4eh= np.asarray(datagram.SoundSpeedAtTransducer)/10
        DI_4eh= np.asarray(datagram.DetectionInfo)
        TT_4eh = np.asarray(datagram.TwoWayTravelTime)
        try:
            Counter_4eh=np.asarray(datagram.Counter)
        except:
            Counter_4eh=np.asarray(datagram.PingCounter)    
        
        AC_4eh = np.asarray(datagram.MeanAbsorption)/100    # make it a vactor?
        BA_4eh = np.asarray(datagram.BeamPointingAngle)   # check again the 100
        SL_4eh = np.asarray(datagram.SignalLength) #make it a vector?
        CF_4eh=np.asarray(datagram.CentreFrequency)
        SN_4eh=np.asarray(datagram.SerialNumber)        
        
        R=CsT_4eh*TT_4eh/2
#        tmp_n=pd.DataFrame({'Ping':Counter_4eh*m256,'SN':SN_4eh*m256,'Frequency':CF_4eh*m256,'Sonar_speed':CsT_4eh*m256,'Pulse_length':SL_4eh*m256,'Nomimal_point_angle':BA_4eh,'Absrorption':AC_4eh*m256,'Range':R})
#        data_n=pd.concat([data_n,tmp_n],ignore_index=True)
        data_n[CountN*256:(CountN+1)*256,:]=np.c_[Counter_4eh*m256,SN_4eh*m256,CF_4eh*m256,CsT_4eh*m256,SL_4eh*m256,BA_4eh,AC_4eh*m256,R]
        
        
        id_n[CountN,:]=np.c_[Counter_4eh,SN_4eh]
        CountN=CountN+1
        
        
        
        
    
    # we read the XYZ 88
    if typeOfDatagram == 'X':
        datagram.read()    
        count=datagram.Counter*m256
        depth=np.asarray(datagram.Depth)
        datagram.BeamIncidenceAngleAdjustment
        x1=np.asarray(datagram.AcrossTrackDistance)
        y1=np.asarray(datagram.AlongTrackDistance)
        heading=datagram.Heading
        bs=np.asarray(datagram.Reflectivity)
        sn=datagram.SerialNumber
        transducer_depth=np.asarray(datagram.TransducerDepth)
        depth=-(np.asarray(datagram.TransducerDepth)+np.asarray(datagram.Depth))
        
        
        beam=np.rad2deg(np.arctan2(x1,-depth-transducer_depth))
        
        date_object = (datetime.strptime(str(datagram.RecordDate), '%Y%m%d') + timedelta(0,datagram.Time) - 
                             datetime(1970, 1, 1)  ).total_seconds()
                            
        
        
#            dlon=np.copy(lon)
#            dlat=np.copy(lat)
        if (date_object>=0.9*np.min(navigation[:,0]))  & (date_object<=1.1*np.max(navigation[:,0])  ):
            dlon=flon(date_object)
            dlat=flat(date_object)
        else:
            dlon=0
            dlat=0
        
        
        headingy=np.cos(np.deg2rad(heading))
        headingx=np.sin(np.deg2rad(heading))
        
        dlon=dlon+ headingy*mtodeglon*x1 + headingx*mtodeglon*y1
        dlat=dlat - headingx*mtodeglat*x1 + headingy*mtodeglat*y1
        
         
        id_x[CountX,:]=np.c_[count[0],sn]
        
        #average slope
#        sx = 0.0;
#		sy = 0.0;
#		sxx = 0.0;
#		sxy = 0.0;
#        
#        sx=np.sum(x1)
#        sy += bath[k];
#		sxx += bathacrosstrack[k] * bathacrosstrack[k];
#		sxy += bathacrosstrack[k] * bath[k];
#        if ns>0:
#            delta = ns * sxx - sx * sx;
#			b = (ns * sxy - sx * sy) / delta;
#			avgslope = np.rad2deg(atan(b))
        
        
        
#        tmp_x=pd.DataFrame({'Ping':count*m256,'SN':sn*m256,'UTMX':dlon,'UTMY':dlat,'Height':height*m256,'Depth':depth,'BS':bs,'Beam':beam,'Transducer_depth':transducer_depth*m256,'Heading':heading*m256})
#        data_x=pd.concat([data_x,tmp_x],ignore_index=True)
        
 
      
#        if dlon[0]>0:
#            plt.scatter(dlon,dlat)
#            plt.title(datagram.Counter)
#            
#            plt.pause(1)
        data_x[CountX*256:(CountX+1)*256,:]=np.c_[count*m256,sn*m256,dlon,dlat,height*m256,depth,bs,beam,transducer_depth*m256,heading*m256]
        CountX=CountX+1
        
    
    # we read the Seabed image 89
    if typeOfDatagram == 'Y':
        datagram.read()    
        corr_59h=np.asarray(datagram.RangeToNormalIncidence)
        FS_59h=np.asarray(datagram.SampleFrequency)
        ping_59h=np.asarray(datagram.Counter)
        SN_59h=np.asarray(datagram.SerialNumber)
        
        
        TTI=corr_59h/FS_59h #traveltime to nomral indicdece
       
        
#        tmp_y=pd.DataFrame({'Ping':ping_59h*m256,'SN':SN_59h*m256,'UTMX':x1,'Frequency222':FS_59h*m256,'Traveltime_to_normal_incidence':TTI*m256})
         
#        data_y=pd.concat([data_y,tmp_y],ignore_index=True)
        id_y[CountY,:]=np.c_[ping_59h,SN_59h]

        data_y[CountY*256:(CountY+1)*256,:]=np.c_[ping_59h*m256,SN_59h*m256,FS_59h*m256,TTI*m256]
        CountY=CountY+1
        
        
        
        
        
        print('Reading file progress %.3f %%'%(100*pingCount2/np.max(pingCount)))
        pingCount2=pingCount2+1  # Careful, we only care about pingcounts in those datagrams
    
    



#here calaultete Range to normal incidence

    
r.rewind()
r.close()    



# Suncronize N,X,Y to start and end from the same ping and SN.  

id_nxy=np.unique(np.r_[id_n,id_x,id_y],axis=0)
id_nxy_rows=set(map(tuple,id_nxy))

ix_rows=set(map(tuple,data_x[:,:2]))
iy_rows=set(map(tuple,data_y[:,:2]))
in_rows=set(map(tuple,data_n[:,:2]))


ix=id_nxy_rows.difference(ix_rows)
iy=id_nxy_rows.difference(iy_rows)
inn=id_nxy_rows.difference(in_rows)

ix=np.asarray(list(ix))
iy=np.asarray(list(iy))
inn=np.asarray(list(inn))


if iy.shape[0]==0:
    iy=np.array([[],[]]).T
if ix.shape[0]==0:
    ix=np.array([[],[]]).T    
if inn.shape[0]==0:
    inn=np.array([[],[]]).T    
# pings to drop
in_all=np.unique(np.r_[ix,iy,inn],axis=0)

for i in range(0,in_all.shape[0]):
    ixx=np.where( (data_x[:,0]==in_all[i,0]) & (data_x[:,1]==in_all[i,1])  )[0]
    iyy=np.where( (data_y[:,0]==in_all[i,0]) & (data_y[:,1]==in_all[i,1])  )[0]
    inn=np.where( (data_n[:,0]==in_all[i,0]) & (data_n[:,1]==in_all[i,1])  )[0]
    
    
    if i==0:
        ixx_all=ixx
        iyy_all=iyy
        inn_all=inn
    else:
        ixx_all=np.r_[ixx_all,ixx]
        iyy_all=np.r_[iyy_all,iyy]
        inn_all=np.r_[inn_all,inn]
    
    
data_x=np.delete(data_x,(ixx_all),axis=0)
data_y=np.delete(data_y,(iyy_all),axis=0)
data_n=np.delete(data_n,(inn_all),axis=0)







test_1=np.array_equal(data_x[:,:2],data_n[:,:2])
test_2=np.array_equal(data_x[:,:2],data_y[:,:2])


#tmp_n=pd.DataFrame({'Ping':Counter_4eh*m256,'SN':SN_4eh*m256,'Frequency':CF_4eh*m256,'Sonar_speed':CsT_4eh*m256,'Pulse_length':SL_4eh*m256,'Nomimal_point_angle':BA_4eh,'Absrorption':AC_4eh*m256,'Range':R})
#tmp_y=pd.DataFrame({'Ping':ping_59h*m256,'SN':SN_59h*m256,'UTMX':x1,'Frequency222':FS_59h*m256,'Traveltime_to_normal_incidence':TTI*m256})
#tmp_x=pd.DataFrame({'Ping':count*m256,'SN':sn*m256,'UTMX':dlon,'UTMY':dlat,'Height':height*m256,'Depth':depth,'BS':bs,'Beam':beam,'Transducer_depth':transducer_depth*m256,'Heading':heading*m256})







if (test_1==True) & (test_2==True):
    print('Success')
    data=pd.DataFrame({'Ping':data_x[:,0],'SN':data_x[:,1],'UTMX':data_x[:,2],'UTMY':data_x[:,3],'Height':data_x[:,4],
                       'Depth':data_x[:,5],'BS':data_x[:,6],'Frequency':data_n[:,2],'Beam':data_x[:,7],'Transducer_depth':data_x[:,8],'Heading':data_x[:,9],
                       'Sonar_Speed':data_n[:,3],'Pulse_length':data_n[:,4],'Nomimal_point_angle':data_n[:,5],'Absrorption':data_n[:,6],'Range_to_normal_incidence':(data_n[:,3]*data_y[:,3])/2, 'Range':data_n[:,7]
                       
                       })
    data_x=np.zeros((0,))
    data_y=np.zeros((0,))
    data_n=np.zeros((0,))

else:
    print('Error')



print("Read Duration: %.3f seconds, pingCount %d" % (time.time() - start_time, pingCount)) # print the processing time. It is handy to keep an eye on processing performance.
