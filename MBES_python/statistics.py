# -*- coding: utf-8 -*-
"""
Created on Mon Sep  2 10:15:06 2019

@author: karaouli
"""

import numpy as np
from scipy.io import loadmat, savemat
import glob
from printProgressBar import printProgressBar
import matplotlib.pyplot as plt
#from scipy.optimize import curve_fit
from sklearn.mixture import GaussianMixture
from scipy.stats import norm



def gauss(x,mu,sigma,A):
    return A*np.exp(-(x-mu)**2/2/sigma**2)
#
#
#def modal(x,mu1,sigma1,A1,mu2,sigma2,A2):
#    return gauss(x,mu1,sigma1,A1)+gauss(x,mu2,sigma2,A2)
#
#
#def bimodal(x,mu1,sigma1,A1,mu2,sigma2,A2):
#    return gauss(x,mu1,sigma1,A1)+gauss(x,mu2,sigma2,A2)
#
#def trimodal(x,mu1,sigma1,A1,mu2,sigma2,A2,mu3,sigma3,A3):
#    return gauss(x,mu1,sigma1,A1)+gauss(x,mu2,sigma2,A2)++gauss(x,mu3,sigma3,A3)
#
#def tetrabimodal(x,mu1,sigma1,A1,mu2,sigma2,A2,mu3,sigma3,A3,mu4,sigma4,A4):
#    return gauss(x,mu1,sigma1,A1)+gauss(x,mu2,sigma2,A2)+gauss(x,mu3,sigma3,A3)+gauss(x,mu4,sigma4,A4)
#
#def pentamodal(x,mu1,sigma1,A1,mu2,sigma2,A2):
#    return gauss(x,mu1,sigma1,A1)+gauss(x,mu2,sigma2,A2)

#def bimodal(x,mu1,sigma1,A1,mu2,sigma2,A2):
#    return gauss(x,mu1,sigma1,A1)+gauss(x,mu2,sigma2,A2)
#
#def bimodal(x,mu1,sigma1,A1,mu2,sigma2,A2):
#    return gauss(x,mu1,sigma1,A1)+gauss(x,mu2,sigma2,A2)








theta_st=np.r_[np.arange(64,54-1,-1),np.arange(53,45-1,-2),np.arange(43,30-1,-3),np.arange(27,3-1,-4)]

BSMIN=-50
BSMAX=-5
hist_int=0.5
x=np.arange(BSMIN,BSMAX+hist_int,hist_int)
#
#datapath_out='F:\\v3\\'
#
#yt=glob.glob(r'E:\v3\mean\*.mat')
#
#
#for i in range(0,len(yt),2):
##    print(yt[i])
#    printProgressBar(i, len(yt), prefix = 'Merging:', suffix = 'Complete', length = 50)
#
#    tmp_1=loadmat(yt[i])['data']
#    tmp_2=loadmat(yt[i+1])['data']
#    
#    if i==0:
#        side_1=tmp_1.data
#        side_2=tmp_2
#    else:
#        side_1=np.r_[side_1,tmp_1]
#        side_2=np.r_[side_2,tmp_2]
##    plt.scatter(tmp_1[:,1],tmp_1[:,2])
##    plt.scatter(tmp_2[:,1],tmp_2[:,2])
#


side_1=np.load('side_1.npy')
side_2=np.load('side_2.npy')



#i=10
#i1=np.where(side_1[:,0]==i)[0]
##i2=np.where(side_2[:,0]==i)[0]
#X=side_1[i1,4]
## fit models with 1-10 components
#N = np.arange(3, 5)
#models = [None for i in range(len(N))]
#
#for i in range(len(N)):
#    models[i] = GaussianMixture(n_components=N[i], covariance_type="full", tol=0.001).fit(np.expand_dims(X,1))
#
## compute the AIC and the BIC
#AIC = [m.aic(np.expand_dims(X,1)) for m in models]
#BIC = [m.bic(np.expand_dims(X,1)) for m in models]
#
##------------------------------------------------------------
## Plot the results
##  We'll use three panels:
##   1) data + best-fit mixture
##   2) AIC and BIC vs number of components
##   3) probability that a point came from each component
#
#fig = plt.figure(figsize=(5, 1.7))
#fig.subplots_adjust(left=0.12, right=0.97,
#                    bottom=0.21, top=0.9, wspace=0.5)
#
#
## plot 1: data + best-fit mixture
#ax = fig.add_subplot(131)
##M_best = models[np.argmin(AIC)]
#M_best = models[1]
#
#
#x=np.arange(BSMIN,BSMAX+hist_int,hist_int)
#logprob = M_best.score_samples(x.reshape(-1, 1))
#
#responsibilities = M_best.predict_proba(x.reshape(-1, 1))
#pdf = np.exp(logprob)
#
#
#pdf_individual = responsibilities * pdf[:, np.newaxis]
#
#ax.hist(X, x, density=True, histtype='stepfilled', alpha=0.4)
#ax.plot(x, pdf, '-k')
#ax.plot(x, pdf_individual, '--k')
#ax.text(0.04, 0.96, "Best-fit Mixture",
#        ha='left', va='top', transform=ax.transAxes)
#ax.set_xlabel('$x$')
#ax.set_ylabel('$p(x)$')
#
#
## plot 2: AIC and BIC
#ax = fig.add_subplot(132)
#ax.plot(N, AIC, '-k', label='AIC')
#ax.plot(N, BIC, '--k', label='BIC')
#ax.set_xlabel('n. components')
#ax.set_ylabel('information criterion')
#ax.legend(loc=2)
#
#
## plot 3: posterior probabilities for each component
#ax = fig.add_subplot(133)
#
#p = responsibilities
##p = p[:, (1, 0, 2)]  # rearrange order so the plot looks better
#p = p.cumsum(1).T
#
#ax.fill_between(x, 0, p[0], color='gray', alpha=0.3)
#ax.fill_between(x, p[0], p[1], color='gray', alpha=0.4)
#ax.fill_between(x, p[1], p[2], color='gray', alpha=0.5)
#ax.fill_between(x, p[2], 1, color='gray', alpha=0.7)
#ax.set_xlim(-50, -5)
#ax.set_ylim(0, 1)
#ax.set_xlabel('$x$')
#ax.set_ylabel(r'$p({\rm class}|x)$')
#
##ax.text(-5, 0.3, 'class 1', rotation='vertical')
##ax.text(0, 0.5, 'class 2', rotation='vertical')
##ax.text(3, 0.3, 'class 3', rotation='vertical')
#
#plt.show()
#
#
#









X=np.c_[side_1[:,0],side_1[:,4]]

#X=np.c_[merged[:,1],merged[:,2]]




N = np.arange(4, 7)
models = [None for ii in range(len(N))]
for ii in range(len(N)):
    models[ii] = GaussianMixture(n_components=N[ii], covariance_type="full", tol=0.001).fit(X)

# compute the AIC and the BIC
AIC = [m.aic(X) for m in models]
BIC = [m.bic(X) for m in models]





print(N[np.argmin(AIC)])
M_best = models[1]
#M_best = models[k]
a234

XX, YY = np.meshgrid(np.unique(side_1[:,0]), np.arange(BSMIN,BSMAX+hist_int,hist_int))
XX2 = np.array([XX.ravel(), YY.ravel()]).T


#x=np.arange(BSMIN,BSMAX+hist_int,hist_int)
logprob = M_best.score_samples(XX2)



responsibilities = M_best.predict_proba(XX2)
pdf = np.exp(logprob)

pdf_individual = responsibilities * pdf[:, np.newaxis]

logprob=np.reshape(logprob,YY.shape)


plt.subplot(1,2,1)
plt.contourf(XX,YY,logprob,256,cmap='nipy_spectral')
plt.xlabel('Angle')
plt.ylabel('BS')
plt.title(r'Probability density of all points')
#plt.colorbar()
#plt.xlim([0.5,5.2])
#plt.ylim([-0.5,3.5])

#plt.subplot(1,3,2)
#plt.contourf(XX,YY,logprob,256,cmap='nipy_spectral',vmin=-10,vmax=-1.2)
##plt.colorbar()
#plt.scatter(X[:,0],X[:,1],s=0.2)
#plt.xlabel('Km/s')
#plt.ylabel('Resistivity (Ohm.m)')
#plt.title(r'Probability density $p({\rm class}|x)$ + Points')

#plt.xlim([0.5,5.2])
#plt.ylim([-0.5,3.5])
#val_pred=M_best.predict(XX2)
#val_pred=cm.rainbow(np.reshape(val_pred,YY.shape))



plt.subplot(1,2,2)
plt.contourf(XX,YY,logprob,256,cmap='nipy_spectral',vmin=-10,vmax=-1.2)
plt.colorbar()


c=['g','r','b','k','m','w']
for i in range(0,pdf_individual.shape[1]):
#    plt.subplot(2,3,i+1)
    plt.contour(XX,YY,np.reshape(pdf_individual[:,i],YY.shape),5,colors=c[i])


for i in range(0,pdf_individual.shape[1]):
    plt.plot(-1,-1,c=c[i],label='Class %d'%(i+1))
plt.legend()

#plt.xlabel('Km/s')
#plt.ylabel('Resistivity (Ohm.m)')
plt.title(r'Probability density per $p({\rm class}|x)$')

#plt.xlim([0.5,5.2])
#plt.ylim([-0.5,3.5])











plt.subplot(2,3,1)
plt.contourf(XX,YY,logprob,256,cmap='nipy_spectral',vmin=-10,vmax=-1.2)
plt.colorbar()
plt.xlabel('Km/s')
plt.ylabel('Ohm.m')
plt.title(r'Probability density per $p({\rm class}|x)$')



plt.xlim([0.5,5.2])
plt.ylim([-0.5,3.5])
locs, labels = plt.yticks()            # Get locations and labels
labels=['0.3','1.0','3.2','10','32','100','316','1000','3162']
plt.yticks(locs,labels)  # Set locations and labels

c=['g','r','b','k','m','w']
for i in range(0,pdf_individual.shape[1]):
    plt.subplot(2,3,i+2)
    plt.contourf(XX,YY,np.reshape(pdf_individual[:,i],YY.shape),256,cmap='nipy_spectral')
    plt.xlabel('Km/s')
    plt.ylabel('Ohm.m')
    plt.title(r'Probability density of $p({\rm class%d}|x)$'%(i+1))
#    plt.colorbar()
    plt.xlim([0.5,5.2])
    plt.ylim([-0.5,3.5])
    plt.yticks(locs,labels)  # Set locations and labels


a234










class_1=np.zeros((side_1.shape[0],3))
class_2=np.zeros((side_2.shape[0],3))

for i in range(0,theta_st.shape[0]):
    i1=np.where(side_1[:,0]==i)[0]
    #i2=np.where(side_2[:,0]==i)[0]
    X=side_1[i1,4]
    # fit models with 1-10 components
    N = np.arange(3, 4)
    models = [None for ii in range(len(N))]
    
    for ii in range(len(N)):
        models[ii] = GaussianMixture(n_components=N[ii], covariance_type="full", tol=0.001).fit(np.expand_dims(X,1))
    
    # compute the AIC and the BIC
    AIC = [m.aic(np.expand_dims(X,1)) for m in models]
    BIC = [m.bic(np.expand_dims(X,1)) for m in models]
    
    #------------------------------------------------------------
    # Plot the results
    #  We'll use three panels:
    #   1) data + best-fit mixture
    #   2) AIC and BIC vs number of components
    #   3) probability that a point came from each component
    
    for k in range(0,1):
        fig = plt.figure(figsize=(14, 7))
        fig.subplots_adjust(left=0.12, right=0.97,
                            bottom=0.21, top=0.9, wspace=0.5)
        
        
        # plot 1: data + best-fit mixture
        ax = fig.add_subplot(131)
        #M_best = models[np.argmin(AIC)]
        M_best = models[k]
        
        


        
        
        
        x=np.arange(BSMIN,BSMAX+hist_int,hist_int)
        logprob = M_best.score_samples(x.reshape(-1, 1))
        
        responsibilities = M_best.predict_proba(x.reshape(-1, 1))
        pdf = np.exp(logprob)
        
        
        pdf_individual = responsibilities * pdf[:, np.newaxis]
        
        ax.hist(X, x, density=True, histtype='stepfilled', alpha=0.4)
        ax.plot(x, pdf, '-k')
        ax.plot(x, pdf_individual, '--k')
        ax.text(0.04, 0.96, "Best-fit Mixture",
                ha='left', va='top', transform=ax.transAxes)
        ax.set_xlabel('$x$')
        ax.set_ylabel('$p(x)$')
        ax.set_title('side 1,classes:%d,angle:%d.png'%(N[k],theta_st[i]))
        
        # plot 2: AIC and BIC
        ax = fig.add_subplot(132)
        ax.plot(N, AIC, '-k', label='AIC')
        ax.plot(N, BIC, '--k', label='BIC')
        ax.set_xlabel('n. components')
        ax.set_ylabel('information criterion')
        ax.legend(loc=2)
        
        
        # plot 3: posterior probabilities for each component
        ax = fig.add_subplot(133)
        
        p = responsibilities
        #p = p[:, (1, 0, 2)]  # rearrange order so the plot looks better
        p = p.cumsum(1).T
        sort_indices=M_best.means_.argsort(axis=0)
        order=sort_indices[:,0]
        p=p[order,:]
        ax.fill_between(x, 0, p[0], color='gray', alpha=0.3)
        ax.fill_between(x, p[0], p[1], color='gray', alpha=0.4)
#        ax.fill_between(x, p[1], p[2], color='gray', alpha=0.5)
        ax.fill_between(x, p[1], 1, color='gray', alpha=0.7)
        ax.set_xlim(-40, -15)
        ax.set_ylim(0, 1)
        ax.set_xlabel('$x$')
        ax.set_ylabel(r'$p({\rm class}|x)$')
        
        #ax.text(-5, 0.3, 'class 1', rotation='vertical')
        #ax.text(0, 0.5, 'class 2', rotation='vertical')
        #ax.text(3, 0.3, 'class 3', rotation='vertical')
        
        plt.show()
        plt.figure()
#        plt.scatter(X,M_best.predict(np.expand_dims(X,1)))
        
        
        sort_indices=M_best.means_.argsort(axis=0)
        order=sort_indices[:,0]
#        M_best.means_=M_best.means_[order,:]
#        M_best.covariances_= M_best.covariances_[order,0]
#        W=np.split(M_best.weights_,N[k])
#        W=np.asarray(W)
#        W=np.ravel(W[order,:])
#        M_best.weights_=W
        
        val=M_best.predict(np.expand_dims(X,1))
        for nn in range(0,order.shape[0]):
            ixx=np.where(order==nn)[0]
            val[val==nn]=ixx+10
        val=val-10
        class_1[i1,k]=val
        
        
        
        plt.savefig('side_1_classes_%d,angle_%d.png'%(N[k],theta_st[i]))
        plt.close()






    i1=np.where(side_2[:,0]==i)[0]
    #i2=np.where(side_2[:,0]==i)[0]
    X=side_2[i1,4]
    # fit models with 1-10 components
    N = np.arange(3, 4)
    models = [None for ii in range(len(N))]
    
    for ii in range(len(N)):
        models[ii] = GaussianMixture(n_components=N[ii], covariance_type="full", tol=0.001).fit(np.expand_dims(X,1))
    
    # compute the AIC and the BIC
    AIC = [m.aic(np.expand_dims(X,1)) for m in models]
    BIC = [m.bic(np.expand_dims(X,1)) for m in models]
    
    #------------------------------------------------------------
    # Plot the results
    #  We'll use three panels:
    #   1) data + best-fit mixture
    #   2) AIC and BIC vs number of components
    #   3) probability that a point came from each component
    
    for k in range(0,1):
        fig = plt.figure(figsize=(14, 7))
        fig.subplots_adjust(left=0.12, right=0.97,
                            bottom=0.21, top=0.9, wspace=0.5)
        
        
        # plot 1: data + best-fit mixture
        ax = fig.add_subplot(131)
        #M_best = models[np.argmin(AIC)]
        M_best = models[k]
        
        
        x=np.arange(BSMIN,BSMAX+hist_int,hist_int)
        logprob = M_best.score_samples(x.reshape(-1, 1))
        
        responsibilities = M_best.predict_proba(x.reshape(-1, 1))
        pdf = np.exp(logprob)
        
        
        pdf_individual = responsibilities * pdf[:, np.newaxis]
        
        ax.hist(X, x, density=True, histtype='stepfilled', alpha=0.4)
        ax.plot(x, pdf, '-k')
        ax.plot(x, pdf_individual, '--k')
        ax.text(0.04, 0.96, "Best-fit Mixture",
                ha='left', va='top', transform=ax.transAxes)
        ax.set_xlabel('$x$')
        ax.set_ylabel('$p(x)$')
        ax.set_title('side 2,classes:%d,angle:%d.png'%(N[k],theta_st[i]))

        
        # plot 2: AIC and BIC
        ax = fig.add_subplot(132)
        ax.plot(N, AIC, '-k', label='AIC')
        ax.plot(N, BIC, '--k', label='BIC')
        ax.set_xlabel('n. components')
        ax.set_ylabel('information criterion')
        ax.legend(loc=2)
        
        
        # plot 3: posterior probabilities for each component
        ax = fig.add_subplot(133)
        
        p = responsibilities
        #p = p[:, (1, 0, 2)]  # rearrange order so the plot looks better
        p = p.cumsum(1).T
        
        sort_indices=M_best.means_.argsort(axis=0)
        order=sort_indices[:,0]
        p=p[order,:]
    
        ax.fill_between(x, 0, p[0], color='gray', alpha=0.3)
        ax.fill_between(x, p[0], p[1], color='gray', alpha=0.4)
#        ax.fill_between(x, p[1], p[2], color='gray', alpha=0.5)
        ax.fill_between(x, p[1], 1, color='gray', alpha=0.7)
        ax.set_xlim(-40, -15)
        ax.set_ylim(0, 1)
        ax.set_xlabel('$x$')
        ax.set_ylabel(r'$p({\rm class}|x)$')
        
        #ax.text(-5, 0.3, 'class 1', rotation='vertical')
        #ax.text(0, 0.5, 'class 2', rotation='vertical')
        #ax.text(3, 0.3, 'class 3', rotation='vertical')
        
        plt.show()

        sort_indices=M_best.means_.argsort(axis=0)
        order=sort_indices[:,0]
#        M_best.means_=M_best.means_[order,:]
#        M_best.covariances_= M_best.covariances_[order,0]
#        W=np.split(M_best.weights_,N[k])
#        W=np.asarray(W)
#        W=np.ravel(W[order,:])
#        M_best.weights_=W
        
        val=M_best.predict(np.expand_dims(X,1))
        for nn in range(0,order.shape[0]):
            ixx=np.where(order==nn)[0]
            val[val==nn]=ixx+10
        val=val-10
        class_1[i1,k]=val
        
        plt.savefig('side_2_classes_%d_angle_%d.png'%(N[k],theta_st[i]))
        plt.close()











out=np.r_[np.c_[side_1,class_1],np.c_[side_2,class_2]]



import pandas as pd

def mode(df, key_cols, value_col, count_col):
    '''                                                                                                                                                                                                                                                                                                                                                              
    Pandas does not provide a `mode` aggregation function                                                                                                                                                                                                                                                                                                            
    for its `GroupBy` objects. This function is meant to fill                                                                                                                                                                                                                                                                                                        
    that gap, though the semantics are not exactly the same.                                                                                                                                                                                                                                                                                                         

    The input is a DataFrame with the columns `key_cols`                                                                                                                                                                                                                                                                                                             
    that you would like to group on, and the column                                                                                                                                                                                                                                                                                                                  
    `value_col` for which you would like to obtain the mode.                                                                                                                                                                                                                                                                                                         

    The output is a DataFrame with a record per group that has at least one mode                                                                                                                                                                                                                                                                                     
    (null values are not counted). The `key_cols` are included as columns, `value_col`                                                                                                                                                                                                                                                                               
    contains a mode (ties are broken arbitrarily and deterministically) for each                                                                                                                                                                                                                                                                                     
    group, and `count_col` indicates how many times each mode appeared in its group.                                                                                                                                                                                                                                                                                 
    '''
    return df.groupby(key_cols + [value_col]).size() \
             .to_frame(count_col).reset_index() \
             .sort_values(count_col, ascending=False) \
             .drop_duplicates(subset=key_cols)
             
             
             
             
xs=np.load('xs999.npy')
ys=np.load('ys999.npy')
xmin=np.nanmin(xs)
xmax=np.nanmax(xs)
ymin=np.nanmin(ys)
ymax=np.nanmax(ys)

















dx=1

for k in range(6,9):
              
    # basically, I only need the xmin and xmax that I generates the asc files
    arrays=np.c_[out[:,1],out[:,2],out[:,k]]
    xs=np.arange(xmin,xmax+dx,dx)
    ys=np.arange(ymin,ymax+dx,dx)
    
    
    
    bs=np.nan*np.zeros((xs.shape[0]*ys.shape[0],1))
    
    
    # there are sevearl choices to handle points excaclty on youndary.. Here I just move them by a tiny number
    tiny_number= np.finfo(float).eps
    arrays[arrays[:,0]==np.max(arrays[:,0]),0]=arrays[arrays[:,0]==np.max(arrays[:,0]),0]-tiny_number
    arrays[arrays[:,1]==np.max(arrays[:,1]),1]=arrays[arrays[:,1]==np.max(arrays[:,1]),1]-tiny_number
    
    
    i1=np.int32(np.floor_divide(arrays[:,0]-xmin,dx))
    i2=np.int32(np.floor_divide(arrays[:,1]-ymin,dx))
    # keep only what's in the boundary. Points excaclty on boundary, are removed
    ix=np.where((i1>=0) & (i1<xs.shape[0]) & (i2>=0) & (i2<ys.shape[0])   )[0]
    
    lin_index=(i1[ix])*(ys.shape[0])  +(i2[ix]) 
    
    
    
    
    df=pd.DataFrame({'values':arrays[ix,2],'ii':lin_index})
    #bi=df.groupby('ii').mean()
    #bii=df.groupby('ii').median()
    #    bii=df.groupby('ii').agg(lambda x:x.value_counts().index[0])
    #    bii2=df.groupby('ii')['values'].agg(lambda x: pd.Series.mode(x)[0])
    bii2=mode(df, ['ii'], 'values', 'count')
    
    #    bs[bi.index.values]=np.reshape(bi['values'].values,(bi.shape[0],1))
    
    
    bs[bii2['ii'].values]=np.reshape(bii2['values'].values,(bii2.shape[0],1))
    
    #elev7[bii.index.values]=bii.values
    bs=np.reshape(bs,((xs.shape[0],ys.shape[0])))
    
    bs=np.flipud(bs.T)
    #        plt.imshow(bs)
    
    
    
    
    
    bs[np.isnan(bs)]=-999.99
    
    
    #    filename='bs_%d.asc'%nn
    filename='new_%d.asc'%(k)
    file=open('D:\\karaouli\\Desktop\\Projects\\backscattered\\'+filename,'w')
    file.write('ncols %d\n'%bs.shape[1])
    file.write('nrows %d\n'%bs.shape[0])
    file.write('xllcorner %.3f\n'%np.min(xs))
    file.write('yllcorner %.3f\n'%np.min(ys))
    file.write('cellsize %.3f\n'%dx)
    file.write('NODATA_value -999.99\n')
    for i in range(0,bs.shape[0]):
        for j in range(0,bs.shape[1]):
            file.write('%.2f '%bs[i,j])
        file.write('\n')
    
    file.close()











fig, ax = plt.subplots(5, 6, figsize=(14, 9), sharex=True, sharey=True)
ii=0
nn=0

for i in range(0,theta_st.shape[0]):
    
    i1=np.where(side_1[:,0]==i)[0]
#    plt.subplot(5,6,i+1)
    ax[ii,nn].hist(side_1[i1,4],x,normed=True)
    ax[ii,nn].set_xlim([-55,-5])
    ax[ii,nn].set_title('%.1f'%theta_st[i])
    nn=nn+1
    if nn==6:
        ii=ii+1
        nn=0
    
    
    
    
i=10


i1=np.where(side_1[:,0]==i)[0]
#i2=np.where(side_2[:,0]==i)[0]
work_data=side_1[i1,:]






#no_bins,bins,_=plt.hist(work_data[:,4],x,normed=True)
no_bins,bins=np.histogram(work_data[:,4],x)
i1=np.where(no_bins>0.01*np.max(no_bins))[0]

i2=np.where((work_data[:,4]>=x[i1[0]]       )  & (work_data[:,4]<=x[i1[-1]])    )[0]
work_data=work_data[i2,:]
gmm = GaussianMixture(n_components=2, covariance_type="full", tol=0.001)
gmm_results = gmm.fit(X=np.expand_dims(work_data[:,4],1))
gmm_x=x[i1]
gmm_y=np.exp(gmm_results.score_samples(gmm_x.reshape(-1, 1)))
means=gmm.means_
stds=gmm.covariances_
weights=gmm.weights_








for i in range(0,2):
    y=norm.pdf(gmm_x,means[i],stds[i,0,0])*weights[i]
    y2=gauss(gmm_x,means[i],stds[i,0,0],1/(np.sqrt(2*np.pi)*np.power(stds[i,0,0],2)    )   )
    y3=norm.pdf(gmm_x,means[i],stds[i,0,0])*(  1/(np.sqrt(2*np.pi)*np.power(stds[i,0,0],2)    )   )
    plt.plot(gmm_x,y,'r')
#    plt.plot(gmm_x,y2,'k')
#    plt.plot(gmm_x,y3,'m')








plt.plot(gmm_x,gmm_y,label='gmm')
plt.plot(gmm_x,gmm_y,label='gmm')

#for i in range(0,theta_st.size[0]):
#    i1=np.where(side_1[:,0]==i)[0]
#    i2=np.where(side_2[:,0]==i)[0]
#    plt.subplot(2,1,1)
#    plt.hist(side_1[i1,4],x)
#    gmm = GaussianMixture(n_components=3, covariance_type="full", tol=0.0001)
#    gmm = gmm.fit(X=side_1[i1,4])
#    
#    gmm_x=x
#    gmm_y=np.exp(gmm.score_samples(gmm_x.reshape(-1, 1)))
    
    
    

    