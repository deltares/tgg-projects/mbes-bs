# -*- coding: utf-8 -*-
"""
Created on Wed Feb 13 16:27:39 2019

@author: karaouli
"""

import numpy as np
import pandas as pd

def running_mean(x, N):
    cumsum = np.cumsum(np.insert(x, 0, 0)) 
    return (cumsum[N:] - cumsum[:-N]) / float(N)


def noiseFilter(data,avg_pings,b_BS):
    
    
   
    ping=np.unique(data[:,0]).astype('int32')
    
    # find mean for every windows pins
    list1=np.arange(ping[0],ping[-1],avg_pings) 
    list1=np.unique(np.r_[list1,ping[-1]+1]).astype('int32')
    
    mean2=np.zeros((list1.shape[0]-1,1))
    # find average of few pings
    for k in range(0,list1.shape[0]-1):
        ix=np.where( (data[:,0]>=list1[k]) &     (data[:,0]<=list1[k]+avg_pings-1) )[0]
        mean2[k]=np.nanmean(data[ix,7])
        
    # find mean for every ping
    mean1=np.zeros((ping.shape[0],2))
    for k in range(0,ping.shape[0]):
        mean1[k,0]=data2['BS'].loc[data2['Ping_ID']==ping[k]].mean()
        # find where this ping belongs to
        ia=np.where((list1>=ping[k] ) )[0]
        ia=ia[0]-1
        if mean2[ia]-mean1[k,0]>b_BS:
            mean1[k,1]=1

    # ping to remove
    ii=ping[np.where(mean1[:,1]==1)[0]] 
    data=data.drop( data[data["Ping"].isin(ii)].index    )
    
    return data