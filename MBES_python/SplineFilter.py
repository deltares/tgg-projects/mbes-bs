# -*- coding: utf-8 -*-
"""
Created on Wed Feb 13 16:15:55 2019

@author: karaouli
"""

#Applying spline filter for removing bad bathymetry soundings
  
# 1D-spline
from scipy import interpolate
from scipy.signal import savgol_filter
import pandas as pd
import numpy as np
import matplotlib.pyplot as plt
from printProgressBar import printProgressBar




def SplineFilter(data,wF,flag2):
    data_in=data.shape[0]
#    print ('Number of data in Slope_Correction:%d'%data_in)
    
    pings=np.unique(data[:,0])
    sn=np.unique(data[:,2])
    
    
    if flag2==2:
        
        for i in range(0,pings.shape[0]):
            
#            print('Spline progress %.3f %%'%(100*i/pings.shape[0]))
            printProgressBar(i, pings.shape[0], prefix = 'Spline Filter Progress:', suffix = 'Complete', length = 50)

            #left
#            z_left=data['Depth'].loc[(data['Ping_ID']==pings[i]) & (data['SN']==sn[0])]
#            z_right=data['Depth'].loc[(data['Ping_ID']==pings[i]) & (data['SN']==sn[1])]
            
            z_left_id=np.where((data[:,0]==pings[i]) & (data[:,2]==sn[0]))[0]
            z_left=data[z_left_id,6]
            z_right_id=np.where((data[:,0]==pings[i]) & (data[:,2]==sn[1]))[0]
            z_right=data[z_right_id,6]
            if z_left.shape[0]>0:
                z_left_new=savgol_filter(z_left,5,2)
                diff_left=np.abs(z_left_new-z_left)
                std_dif_left=np.std(diff_left)
                idx_left=np.where(diff_left>1.2*wF*std_dif_left)[0]
#                plt.clf()
#                dis1=np.arange(0,z_left.shape[0])
#                plt.plot(dis1,z_left,'x')
#                plt.plot(dis1,z_left_new)
#                plt.plot(dis1[idx_left],z_left[idx_left],'o')
                idx_left=z_left_id[idx_left]
            else:
                idx_left=np.array(())
                
            if z_right.shape[0]>0:                
                z_right_new=savgol_filter(z_right,5,2)
                diff_right=np.abs(z_right_new-z_right)
                std_dif_right=np.std(diff_right)
                idx_right=np.where(diff_right>1.2*wF*std_dif_right)[0]    
#                plt.clf()
#                dis2=np.arange(0,z_right.shape[0])+256
#                plt.plot(dis2,z_right,'x')
#                plt.plot(dis2,z_right_new)
#                plt.plot(dis2[idx_right],z_right[idx_right],'o')
                idx_right=z_right_id[idx_right]
                
            else:
                idx_right=np.array(())            
            
            
            id_both=np.r_[idx_left,idx_right]
            
            

            

            
            
            
            if i==0:
                id_out=id_both
            else:
                id_out=np.r_[id_out,id_both]
        
        
        
        
        data=np.delete(data,id_out.astype('int32'),axis=0)
    
    
            
    #2D interpolation        
    elif flag2==1:
        a=1
        # make a moving grid
        
    
#        xmin = np.min(data['UTMX'])
#        xmax = np.max(data['UTMX'])
#        ymin = np.min(data['UTMY'])
#        ymax = np.max(data['UTMY'])
#        
#        x_idx=np.arange(xmin+delta2/2,xmax-delta2/2,delta2)
#        y_idx=np.arange(ymin+delta2/2,ymax-delta2/2,delta2)
#        #these are the centers
#        xx,yy=np.meshgrid(x_idx,y_idx)
#        xx=xx.ravel()
#        yy=yy.ravel()
#        
#        
#        
#        
#        
#        for k in range(0,xx.shape[0]):
#            #select points in the vincinity of the center
#            data_tmp=data.loc[(data["UTMX"]>=xx[k]-delta2/1.5) & (data["UTMX"]<=xx[k]+delta2/1.5) & (data["UTMY"]>=yy[k]-delta2/1.5 ) & (data["UTMY"]<=yy[k]+delta2/1.5)]
#            if data_tmp.shape[0]>5:
#                x2=np.arange(np.min(data_tmp['UTMX']),np.max(data_tmp['UTMX']),0.5)
#                y2=np.arange(np.min(data_tmp['UTMY']),np.max(data_tmp['UTMY']),0.5)
#                
#                x2,y2=np.meshgrid(x2,y2)
#                
#                
#                
#                if data_tmp.shape[0]>3:
#                    rbfi=Rbf(data_tmp['UTMX'],data_tmp['UTMY'],data_tmp['Depth'],epsilon=2,smooth=0.03)
#                    new_val=rbfi(data_tmp['UTMX'],data_tmp['UTMY'])
#                    
#    #                new_val2=rbfi(x2.ravel(),y2.ravel())
#    #                plt.subplot(2,2,1)
#                    plt.scatter(data_tmp['UTMX'].values,data_tmp['UTMY'].values,5,c=data_tmp['Depth'].values,cmap='nipy_spectral',vmin=-48,vmax=-24)
#                    plt.axis('equal')
        #            plt.colorbar()
    #                plt.subplot(2,2,2)
    #                plt.scatter(data_tmp['UTMX'].values,data_tmp['UTMY'].values,5,c=new_val,cmap='nipy_spectral',vmin=-48,vmax=-24)            
    #    #            plt.colorbar()
    #                plt.axis('equal')
    #                
    #                plt.subplot(2,2,3)
    #                plt.scatter(x2.ravel(),y2.ravel(),5,c=new_val2,cmap='nipy_spectral',vmin=-48,vmax=-24)            
    #    #            plt.colorbar()
    #                plt.axis('equal')
    #                
    #                plt.subplot(2,2,4)
    #                plt.scatter(data_tmp['UTMX'].values,data_tmp['UTMY'].values,5,c=data_tmp['Depth'].values-new_val,cmap='nipy_spectral',vmin=-1.2,vmax=1.2)            
    #    #            plt.colorbar()    
    #                plt.axis('equal')
        
        
        
    #    plt.scatter(data['UTMX'].values,data['UTMY'].values,10,c=data['Depth'].values,cmap='nipy_spectral')
    #    plt.axis('equal')    
        
    return data   ,id_out 