# -*- coding: utf-8 -*-
"""
Created on Tue Nov 13 10:16:53 2018

@author: karaouli
"""

import pandas as pd
import matplotlib.pyplot as plt
import sys
sys.path.insert(0, r'D:\karaouli\Desktop\Projects\python_tools')
import glob
import numpy as np
from make_a2d import make_a2d
from rgbcmyk import multidim_intersect
from pseudo_depth import pseudo_depth
from geofac2 import geofac2
from pyall import ALLReader





r=ALLReader(r'D:\karaouli\Desktop\Projects\backscattered\data\All\0222_20181112_193013_ARCA.all')



#a1=r.loadInstallationRecords()
#a2=r.loadNavigation()
#a3=r.readDatagram()
k=0
z=0
while r.moreData():
    # read a datagram.  If we support it, return the datagram type and aclass for that datagram
    # The user then needs to call the read() method for the class to undertake a fileread and binary decode.  This keeps the read super quick.
    TypeOfDatagram, datagram = r.readDatagram()
    print(TypeOfDatagram)
    if k==0:
        lala=[TypeOfDatagram]
    else:
        lala.append(TypeOfDatagram)
    k=k+1
    if TypeOfDatagram == 'P':
        datagram.read()
#        print ("Lat: %.5f Lon: %.5f" % (datagram.Latitude, datagram.Longitude))
        if z==0:
            a=np.c_[datagram.Latitude, datagram.Longitude]
            z=1
        else:
            a=np.r_[a,np.c_[datagram.Latitude, datagram.Longitude]]    
        

    if TypeOfDatagram == 'X':
        datagram.read()


r.rewind()
print("Complete reading ALL file :-)")
    
r.close() 