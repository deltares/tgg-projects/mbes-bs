# -*- coding: utf-8 -*-
"""
Created on Fri Mar  1 12:40:02 2019

@author: karaouli
"""

# -*- coding: utf-8 -*-
"""
Created on Tue Feb 26 16:47:13 2019

@author: karaouli
"""

# -*- coding: utf-8 -*-
"""
Created on Tue Jan 29 15:32:12 2019

@author: karaouli
"""

# -*- coding: utf-8 -*-
"""
Created on Tue Nov 13 18:43:16 2018

@author: mario
"""
import sys

# sys.path.insert(0, r'D:\karaouli\Desktop\Projects\python_tools')

import numpy as np
import pandas as pd
import matplotlib.pyplot as plt
import scipy

import glob
import pyproj

from scipy import interpolate
import statsmodels.api as sm
from scipy.interpolate import Rbf
from mpl_toolkits.mplot3d import Axes3D


from scipy.io import loadmat, savemat

# import tables

import time
from datetime import datetime
from datetime import timedelta
from all_file_reader import all_file_reader
from slopeCorr_fast import slopeCorr_fast
from noiseFilter_fast import noiseFilter_fast
from SplineFilter_fast import SplineFilter_fast
from local_arc import local_arc
from BSmap_mean import BSmap_mean
from remove_bad_gps import remove_bad_gps

import h5py
import os

import warnings

warnings.simplefilter(action="ignore", category=FutureWarning)

# utm_zone = (int)(((reference_lon + 183.0) / 6.0) + 0.5);

# myProj = pyproj.Proj("+proj=utm +zone=31, +north +ellps=WGS84 +datum=WGS84 +units=m +no_defs")
myProj = pyproj.Proj("+proj=utm +zone=24 +south +datum=WGS84 +units=m +no_defs")
rd_p = pyproj.Proj(init="epsg:28992")
rd_p2 = pyproj.Proj(init="epsg:7415")
utm_xy_31 = pyproj.Proj(init="epsg:32631")
utm_xy_32 = pyproj.Proj(init="epsg:32632")
wgs84_p = pyproj.Proj(init="epsg:4326")
etrs89_p = pyproj.Proj(init="epsg:4258")


# Sonar Parameters for slope correction
PL = 146 * 10 ^ -6
# nominal pulse length [microseconds]
T = 0.37
# correction term for pulse shading in [%] to recieve effective pulse length
Ox = 1.3 * np.pi / 180
# along track beam width in [degree]
Oy = 1.3 * np.pi / 180
# along track beam width in [degree]
frequency = 300
# nominal center frequency (selectec in SIS)
# Application of filter (on=1; off=0)

#  Bad ping filter (designed for bad pings due to water bubbles causing false backscatter)
flag1 = 1
avg_pings = 10
# pings used for averaging BS value to find outlier by differing from that avg value
b_BS = 1.8
# defines BS difference (single ping to averaged pings) for rejecting pings

# Spline Filter (for adjusting parameters see spline.m)
flag2 = 0
# 1 = 2D Spline filter (fast); 2 = 1D Spline filter; 0 = no spline filter
wF = 2.0
# 1D: weightening factor (the smaller the stronger the spline filter)
span = 0.03
# 1D: percentage of data points used for fitting
BAT_filter = 0.004
# 2D: the smaller the srtonger the filter (between 2.5 and 3.5)
delta2 = 3
# patch size for fitting a 2D plane (the smaller the patch size the faster the algorithm); depends on sounding density and water depth (between 5 and 25 m)
ping_sp = 200  # number of pings used for spline calculation

## Slope correction (Correcting for beam footprint)
flag3 = 1
pings_num = 50
# number of pings used for variable grid calculation (considered variable ship heading)
delta = 1
# Grid Cell size (spatial resolution); consider MBES characteristics and water depth

## Real Time TVG correction
flag4 = 0

## Absorption correction
flag5 = 0
# 1= consider several SVP profiles; 2 = define constant absorption coeff; 0 = no correction
absorp = 72.9
# absorption coefficent: required in case flag5 is 2
filename2 = "absorption_all.txt"
# filename: required in case flag5 is 1
freq_ab = np.array([250, 300, 350])


## Backscatter filter
flagBS = 1
# Z-score filter = 1 / Threshold filter = 0
BS_filter = 3.5
# filter strength increases with decreasing value: Standard = 2.5 / reasonable = 3.5
BS_low = -50
# lower threshold
BS_up = -5
# upper threshold

## Simple rejection of false coordinates im georahical coordinates (WGS84)
lonmin = 2.0
lonmax = 6.0
latmin = 51.0
latmax = 55.0

## Simple rejection of false water depth (negative water depth in [m])
depthmin = -12
depthmax = -35
DS_filter = 5  # filter strength increases with decreasing value: Standard = 2.5 / reasonable = 3.5

## Simple rejection of false beam angles (uncorrected beam in [degree])
beammax = 75

## Simple rejection of false beam angles (corrected beam in [degree])
beammax2 = 80

## Simple rejection of false slope in [degree]
slope_up = 50

## Simple rejection of wrong backscatter correction term in [dB]
c_up = 10


# settings for ploting BS after correction
sliding_window = 100  # Pings used for local averaging
ref_ang = 45  # Reference angle used for compensation
tol = 15  # beam intervall used for reference angle
theta_st_plot = np.arange(0, 71, 1)  # Beam Amgle used depends on swath coverage
tol2_plot = 0.5  # Defines bin size for local AVG
# flag1 = 0              # 1 = normalize BS per sonar head seperately; 0 = normalize BS to a averaged value from both sonmar heads
# flag2 = 0              # Requirement flag1 = 1; Interpolate normalized BS between sonar heads

beam_up_plot = 70  # Define beam angles consdiered for visualistion
beam_low_plot = 0  # Define beam angles consdiered for visualistion
BS_up_plot = -2
BS_low_plot = -45
BS_filter_plot = 3.5


# fix shift
shift = 1 * 26.220


# Values are good approximation for EM2040C in a water depth between 10 and 50m.
theta_st = np.r_[
    np.arange(64, 54 - 1, -1),
    np.arange(53, 45 - 1, -2),
    np.arange(43, 30 - 1, -3),
    np.arange(27, 3 - 1, -4),
]
pings_num_2 = np.r_[10, 4, 3, 2, 1]
depth_bound = np.r_[0, 5, 10, 15, 30]
# Tolerances for bounds of beam angles. Averaging over angles will take place inbetween these bounds,
# to compensate different number of scatterpixels within differnt angles
tol2 = np.ones((theta_st.shape[0]))
tol2[(theta_st >= 54) & (theta_st <= 60)] = 0.7
tol2[(theta_st >= 45) & (theta_st <= 53)] = 1.3
tol2[(theta_st >= 30) & (theta_st <= 43)] = 1.9
tol2[(theta_st >= 3) & (theta_st <= 27)] = 2.5

print("here")
filename = glob.glob(
    r"C:/Users/leentvaa/OneDrive - Stichting Deltares/Documents/testmbes/*.all"
)
print(filename)
datapath_out = "C:/Users/leentvaa/OneDrive - Stichting Deltares/Documents/testmbes/"
# 103 124 154 156 158 160
ww = []
for i in range(0, len(filename)):
    start_time = time.time()  # time the process
    # Program starts here
    data, header = all_file_reader(filename[i])
    ix1 = np.where(data[:, 7] < -100)[0]  # here directly remove bad BS
    size_raw = data.shape[0] - ix1.shape[0]
    data[ix1, 3:] = np.nan

    # Remove false pings (Especially due to water bubbles). Find pins that differ from a windoes oif pins
    # I assume 256 for all pins
    if flag1 == 1:
        ix2 = noiseFilter_fast(data, avg_pings, b_BS)
    size_after_noise = data.shape[0] - ix1.shape[0] - ix2.shape[0]
    data[ix2, 3:] = np.nan

    # remove soungings with bad coordianates
    ix3 = np.where(
        (data[:, 3] < lonmin)
        | (data[:, 3] > lonmax)
        | (data[:, 4] < latmin)
        | (data[:, 4] > latmax)
    )[0]
    ix3b = np.where(data[:, 11] == 0)[0]  # and no heading
    ix3 = np.unique(np.r_[ix3, ix3b])
    data[ix3, 3:] = np.nan

    utmx, utmy, depth = pyproj.transform(
        wgs84_p, utm_xy_31, data[:, 3], data[:, 4], data[:, 6]
    )
    data = np.c_[data, utmx, utmy]
    header.extend(["UTMX", "UTMY"])

    # remove systematic bad GPS pings
    ix0 = remove_bad_gps(data, 40)
    data[ix0, 3:] = np.nan
    size_after_gps = data.shape[0] - ix1.shape[0] - ix2.shape[0] - ix0.shape[0]

    # fix offset
    sign_1 = np.nanmean(data[:, 11])
    data[:, 19] = data[:, 19] + shift * np.sin(np.deg2rad(data[:, 11]))
    data[:, 20] = data[:, 20] + shift * np.cos(np.deg2rad(data[:, 11]))

    # Remove soundings with false BS (Z-Score filter or threshold filter)
    if flagBS == 1:
        ix4 = np.where(
            np.abs(data[:, 7] - np.nanmedian(data[:, 7]))
            / (1.483 * np.nanmedian(np.abs(data[:, 7] - np.nanmedian(data[:, 7]))))
            > BS_filter
        )[0]
    else:
        ix4 = np.where((data[:, 7] < BS_low) & (data[:, 7] > BS_up))
    data[ix4, 3:] = np.nan

    size_after_BS = (
        data.shape[0]
        - ix1.shape[0]
        - ix2.shape[0]
        - ix3.shape[0]
        - ix4.shape[0]
        - ix0.shape[0]
    )

    # Remove beam angles
    ix5 = np.where(data[:, 9] >= beammax)[0]
    data[ix5, 3:] = np.nan
    size_after_beam = (
        data.shape[0]
        - ix1.shape[0]
        - ix2.shape[0]
        - ix3.shape[0]
        - ix4.shape[0]
        - ix5.shape[0]
        - ix0.shape[0]
    )

    # Remove soundings with false depth
    ix6 = np.where((data[:, 6] >= depthmin) | (data[:, 6] <= depthmax))[0]
    data[ix6, :] = np.nan
    # ix6b=np.where (np.abs(  data[:,6]-np.nanmedian(data[:,6]) )  /  (1.483*np.nanmedian( np.abs(  data[:,6]-np.nanmedian(data[:,6]) ))) > DS_filter )[0]
    # data[ix6b,:]=np.nan

    # remove false values
    size_after_depth = (
        data.shape[0]
        - ix1.shape[0]
        - ix2.shape[0]
        - ix3.shape[0]
        - ix4.shape[0]
        - ix5.shape[0]
        - ix6.shape[0]
        - ix0.shape[0]
    )

    # Smooth data
    ix7 = SplineFilter_fast(data, wF, 2)
    data[ix7, 3:] = np.nan
    size_after_smooth = (
        data.shape[0]
        - ix1.shape[0]
        - ix2.shape[0]
        - ix3.shape[0]
        - ix4.shape[0]
        - ix5.shape[0]
        - ix6.shape[0]
        - ix7.shape[0]
        - ix0.shape[0]
    )

    # Correct for slope
    data, header, ix8 = slopeCorr_fast(
        data,
        pings_num,
        delta,
        T,
        Ox,
        Oy,
        frequency,
        c_up,
        slope_up,
        beammax2,
        BS_filter,
        header,
    )
    data[ix8, 3:] = np.nan
    size_after_slope = (
        data.shape[0]
        - ix1.shape[0]
        - ix2.shape[0]
        - ix3.shape[0]
        - ix4.shape[0]
        - ix5.shape[0]
        - ix6.shape[0]
        - ix7.shape[0]
        - ix8.shape[0]
        - ix0.shape[0]
    )

    # calculate mean for clustering (port,portside)
    B1, B2 = BSmap_mean(data, theta_st, tol2, depth_bound, pings_num_2)

    # correct BS values for plotting only
    data, header = local_arc(
        data,
        sliding_window,
        ref_ang,
        tol,
        tol2_plot,
        beam_up_plot,
        beam_low_plot,
        BS_up_plot,
        BS_low_plot,
        BS_filter_plot,
        theta_st_plot,
        header,
    )

    filter_data = pd.DataFrame(
        {
            "Raw Size": [size_raw],
            "After noise": [size_after_noise],
            "After BS": [size_after_BS],
            "After Beam": [size_after_beam],
            "After Depth": [size_after_depth],
            "After Smooth": [size_after_smooth],
            "After Slope": [size_after_slope],
        }
    )

    h5f = h5py.File(datapath_out + os.path.basename(filename[i])[:-4] + ".h5", "w")
    h5f.create_dataset("dataset_1", data=data[:, [19, 20, 6, 7, 24, 27]])
    h5f.close()

    savemat(
        datapath_out + os.path.basename(filename[i])[:-4] + "v2.mat",
        mdict={"data": data},
    )
    filter_data.to_csv(
        datapath_out + os.path.basename(filename[i])[:-4] + "_log.csv", index=False
    )
    print(
        "File Duration: %.3f seconds" % (time.time() - start_time,)
    )  # print the processing time. It is handy to keep an eye on processing performance.
    print(datapath_out + "mean\\" + os.path.basename(filename[i])[:-4] + "_side1.mat")

    scipy.io.savemat(
        datapath_out + "mean\\" + os.path.basename(filename[i])[:-4] + "_side1.mat",
        mdict={"data": B1},
    )
    scipy.io.savemat(
        datapath_out + "mean\\" + os.path.basename(filename[i])[:-4] + "_side2.mat",
        mdict={"data": B2},
    )

    print(
        "File Duration: %.3f seconds" % (time.time() - start_time,)
    )  # print the processing time. It is handy to keep an eye on processing performance.

    with open("header.txt", "w") as f:
        for item in header:
            f.write("%s\n" % item)
    """except:
        print(i)
        print('doesntwork')
        ww.append(i)"""
