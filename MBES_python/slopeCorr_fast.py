# -*- coding: utf-8 -*-
"""
Created on Fri Mar  1 14:12:29 2019

@author: karaouli
"""

# -*- coding: utf-8 -*-
"""
Created on Wed Jan 30 09:59:05 2019

@author: karaouli
"""

import numpy as np
import pandas as pd
#from scipy.ndimage import generic_filter
#from scipy.interpolate import griddata
#import  matplotlib.pyplot as plt
#import scipy.interpolate as interp
#import time
from scipy import stats
from scipy.io import loadmat
import matplotlib.pyplot as plt
from printProgressBar import printProgressBar


#def calc_slope(in_filter, x_cellsize, y_cellsize):
#    
#    #slope calculation here - note need to reshape in array to be 3*3
#    if -9999 in in_filter:
#        return -9999 #Will return -9999 around edge with mode constant and cval -9999
#    else:
#        [a, b, c, d, e, f, g, h, i] = in_filter.flatten()
#        #From 3x3 box, row 1: a, b, c
#        #              row 2: d, e, f
#        #              row 3: g, h, i
#
#        dz_dx = ((c + 2*f + i) - (a + 2 * d + g)) / (8 * float(x_cellsize))
#        dz_dy = ((g + 2*h + i) - (a + 2 * b + c)) / (8 * float(y_cellsize))
#        slope = np.sqrt(dz_dx ** 2 + dz_dy**2)
#
#        return (180/np.pi)*np.arctan(dz_dx),(180/np.pi)*np.arctan(dz_dy),(180/np.pi)*np.arctan(slope) #we want slope in degrees rather than radians
#
#

def slopeCorr_fast(data,pings_num,delta,T,Ox,Oy,frequency,c_up,slope_up,beammax2,BS_filter,header):

    
    num_pings=data.shape[0]/256
    if np.equal(np.mod(num_pings, 1), 0):
        print('Pings before Slope are ok:')
    else:
        print('!!!!Pings before Slope are not ok:')

    
    num_pings_all=np.unique(np.r_[np.arange(0,data.shape[0],2*256*pings_num),data.shape[0]])
    
    # reset ping number to make things easier
        
    
    data=np.c_[data,np.nan*np.ones((data.shape[0],3))]
    header.extend(['Slope','Tx','Ty'])
    for i in range(0,num_pings_all.shape[0]-1):
#        print('Slope progress %.3f %%'%(100*i/pings_end.shape[0]))
        printProgressBar(i, num_pings_all.shape[0]-1, prefix = 'Slope Calculation Progress:', suffix = 'Complete', length = 50)
        if i>0:
            iia=np.int32(np.arange(num_pings_all[i]-(2*256*pings_num/5),num_pings_all[i+1]))  # consder overal regrin of pings_num/5
        else:
            iia=np.arange(num_pings_all[i],num_pings_all[i+1])  # not for first ping
        #%read data from selected pings

        E=data[iia,19]
        N=data[iia,20]
        Z=data[iia,6]
#        BS=data2[iia,4]
        
        #Rotation matrix. Using ship heading to transform from global into
        #local coordinates
        iib=np.where(np.isfinite(data[iia,11]))[0]        
        alpha=np.nanmean(np.unwrap(data[iia[iib],11] * np.pi/180))        
        Mat=np.array([[np.cos(alpha),-np.sin(alpha)],[np.sin(alpha),np.cos(alpha)]])
        
        ENp=np.dot(Mat,np.c_[E,N].T) 
        
        #Check heading for grid calculation

        if (np.nanmean(data[iia,11])>270) | (np.nanmean(data[iia,11])<90):
            sig=1
        else:
            sig =-1
            
        #coordinates along-track (Xp) and across-track (Yp)
        Xp=sig*ENp[1,:]
        Yp=sig*ENp[0,:]
            
        #Calculate grid parameters    
        xmin=np.nanmin(Xp)
        xmax=np.nanmax(Xp)
        ymin=np.nanmin(Yp)
        ymax=np.nanmax(Yp)
        if (np.isfinite(xmin))& (np.isfinite(ymin) )&(np.isfinite(ymin) )&(np.isfinite(ymax)):
            x_idx=np.arange(xmin+delta/2,xmax-delta/2,delta)
            y_idx=np.arange(ymin+delta/2,ymax-delta/2,delta)
            
            
            gridc=np.nan*np.ones((x_idx.shape[0],y_idx.shape[0]))
            
    #        gridx,gridz=np.meshgrid(x_idx,y_idx)
            
            kk=0
    #        counter=-999*np.ones((x_idx.shape[0]*y_idx.shape[0],2550))      
            
            for ii in range(0,x_idx.shape[0]):
                for jj in range(0,y_idx.shape[0]):
                    
                    dummy1=np.where( (Xp>=x_idx[ii]-delta/2) & (Xp<x_idx[ii]+delta/2)  &  
                                     (Yp>=y_idx[jj]-delta/2) & (Yp<y_idx[jj]+delta/2)    
                                    )[0]
    #                counter[kk,0:dummy1.shape[0]]=dummy1
                    if kk==0:
                        counter=[list(dummy1)]
                    else:
                        counter.append([list(dummy1)])
                    kk=kk+1
                    Z_filter=np.copy(Z[dummy1])
                    idx_filter=np.where( np.abs(  (Z_filter-np.nanmedian(Z_filter) )/ (1.483*np.nanmedian(  np.abs(Z_filter-np.nanmedian(Z_filter)    )      )       )               ) >3.5     )[0]
                    Z_filter[idx_filter]=np.nan
                                    
                    gridc[ii,jj]=np.nanmean(Z_filter)
                    
            # remove outlines
            idx_filter=np.where( np.abs(  (gridc-np.nanmedian(gridc) )/ (1.483*np.nanmedian(  np.abs(gridc-np.nanmedian(gridc)    )      )       )               ) >3.5     )
            gridc[idx_filter[0],idx_filter[1]]=np.nan
            # interpolate to reduce Nan in 2 delta        
            for ii in range(1,x_idx.shape[0]-1):
                for jj in range(1,y_idx.shape[0]-1):
                    if np.isnan(gridc[ii,jj]):
                        gridc[ii,jj]=np.nanmean(np.c_[gridc[ii-1,jj],gridc[ii+1,jj],gridc[ii,jj-1],gridc[ii,jj+1]           ])
                        
                        
            
    #        gx,gz=np.gradient(gridc,delta)
    #        slope=(180/np.pi)*np.arctan(np.sqrt(np.power(gx,2)+np.power(gz,2)))
    #        gx=(180/np.pi)*np.arctan(gx)
    #        gz=(180/np.pi)*np.arctan(gz)
            
            gx=np.nan*np.ones((x_idx.shape[0],y_idx.shape[0]))
            gz=np.nan*np.ones((x_idx.shape[0],y_idx.shape[0]))
            slope=np.nan*np.ones((x_idx.shape[0],y_idx.shape[0]))
    
            for ii in range(1,x_idx.shape[0]-1):
                for jj in range(1,y_idx.shape[0]-1):
                    dy = -1*(((gridc[ii-1,jj-1] + 2*gridc[ii,jj-1] + gridc[ii+1,jj-1]
                    -gridc[ii-1,jj+1] - 2*gridc[ii,jj+1] - gridc[ii+1,jj+1]))
                    /(8*delta))
                    dx = -1*(((gridc[ii+1,jj-1] + 2*gridc[ii+1,jj] + gridc[ii+1,jj+1]
                    -gridc[ii-1,jj-1] - 2*gridc[ii-1,jj] - gridc[ii-1,jj+1]))
                    /(8*delta))
                        
                    gx[ii,jj]=(180/np.pi)*np.arctan(dx)  
                    gz[ii,jj]=(180/np.pi)*np.arctan(dy)
                    slope[ii,jj]=(180/np.pi)*np.arctan(np.sqrt(np.power(dx,2)+np.power(dy,2)))
            
            
            
            
            
            
    #        plt.clf()
    #        plt.subplot(1,2,1)
    #        plt.imshow(np.flipud(gridc),vmin=-23,vmax=-21.5,cmap='rainbow')
    #        plt.subplot(1,2,2)
    #        plt.imshow(np.flipud(slope),vmin=0,vmax=6,cmap='rainbow')
    #        plt.pause(1)
         
            kk=0
            for ii in range(0,x_idx.shape[0]):
                for jj in range(0,y_idx.shape[0]):
    #                idx2=np.where(counter[kk,:]>=0)[0]
    #                idx3=counter[kk,idx2].astype('int32')
                    idx3=counter[kk]
    #                idx4=data2[iia[idx3],6].astype('int32')
                    idx4=iia[idx3].astype('int32')
    
                    
                    data[idx4,21]=gx[ii,jj]
                    data[idx4,22]=gz[ii,jj]
                    data[idx4,23]=slope[ii,jj]
                    kk=kk+1
                
          
#    data['Slope']=pd.Series(data2[:,9])
#    data['Tx']=pd.Series(data2[:,7])
#    data['Ty']=pd.Series(data2[:,8])
    
    
    # Slope correction: Correction for footprint (and grazining angle)
    # Using tx and ty (slope in local (ship coordinates) x and y direction)
    teta=data[:,9]
    tx=data[:,22]
    ty=data[:,23]
    BS=np.copy(data[:,7])
    beam_po=np.abs(data[:,14])
    c=data[:,12]
    RI=data[:,16]
    R=np.copy(data[:,17])
    PL=np.copy(T*data[:,13])
    f=data[:,8]
    
    #Find starboard and port tack side
    sign=np.sign(data[:,9])
    
    
    ax_all=np.tan(tx*(np.pi/180))
    ay_all=np.tan(ty*(np.pi/180))
    
    # True incident angle
    grz=90-teta
    Argcos = (np.sin(grz*(np.pi/180)) + sig*sign*np.cos(grz*(np.pi/180))*ay_all) / np.sqrt(np.power(ax_all,2)+np.power(ay_all,2) + 1)
    beam_ang=np.arccos(Argcos)*(180/np.pi)*sign
    
    
    #Calculate footprints with respect to true grazing/incident angle
    #make sure that no complex number exist due to RI > R
    idx=np.where(RI>=R)[0]
    R[idx]=RI[idx]+RI[idx]*0.001
    
    #Account for frequency shift between different sonar heads
    dx_array = (c/(frequency*1000))/(Ox)
    dy_array = (c/(frequency*1000))/(Oy)
    Ox = (c/f)/dx_array
    Oy = (c/f)/dy_array  
    
     #Consider wideing of acrosstrack beamwidth with increasing beam
    Oy_beam = Oy*1/(np.cos(np.deg2rad(beam_po)))   
    A_beam= (Ox*Oy_beam*np.power(R,2))
    
    
    #Consider wideing of acrosstrack beamwidth with increasing beam
    A_signal_old = (c*PL*R*Ox)/(2*np.sqrt(1-((np.power(RI,2))/(np.power(R,2)))))
    A_signal_new = (c*PL*R*Ox)/(2*np.sin(np.abs((teta - sig*sign*ty))*np.pi/180)*np.cos(tx*np.pi/180))
    
    
    
    # CORRECTION OF BACKSCATTER VALUES
    C1 = 10*np.log10(A_signal_old) - 10*np.log10(A_signal_new)
    BS1 = np.copy(BS) + C1       
    C2 =  10*np.log10(A_signal_old) - 10*np.log10((np.power(R,2) * Ox * Oy_beam)/(np.cos(tx*(np.pi/180))*np.cos(ty*(np.pi/180))))
    BS2 = np.copy(BS) + C2      
    C3 = 10*np.log10(np.cos(tx*np.pi/180) * np.cos(ty*np.pi/180))
    BS3 = np.copy(BS) +C3      
    C4 = 10*np.log10(A_beam) - 10*np.log10(A_signal_new)
    BS4 = np.copy(BS) + C4
    
    
    idxx1 = np.where( (A_signal_old < A_beam) & (A_signal_new < A_beam))[0]
    idxx2 = np.where((A_signal_old < A_beam) & (A_signal_new >= A_beam))[0]
    idxx3 = np.where((A_signal_old >= A_beam) &( A_signal_new >= A_beam))[0]
    idxx4 = np.where((A_signal_old >= A_beam) &( A_signal_new < A_beam))[0]
    
    
    
    BS[idxx1] = BS1[idxx1]
    BS[idxx2] = BS2[idxx2]
    BS[idxx3] = BS3[idxx3]
    BS[idxx4] = BS4[idxx4]
    
    C = np.zeros((BS.shape[0],1))
    C[idxx1,0] = C1[idxx1]
    C[idxx2,0] = C2[idxx2]
    C[idxx3,0] = C3[idxx3]
    C[idxx4,0] = C4[idxx4]
    
    
    
    
    
    # store the data
#    data['BS_new']=pd.Series(BS)
#    data['Beam_angle']=pd.Series(beam_ang)
#    data['CC']=pd.Series(C[:,0])
    
    data=np.c_[data,BS,beam_ang,C]
    header.extend(['BS_new','Beam Angle','CC'])
    
    
#    plt.plot(data['BS_new']-data['BS'],'x')
    

    
    # Remove soundings with false BS correction term
    ix1=np.where(data[:,26]>=c_up)[0]
#    data[ix1,:]=np.nan
    
    
    # Remove soundings with false slope
    ix2=np.where(data[:,21]>=slope_up)[0]
#    data[ix2,:]=np.nan
        
    # Remove soundings with false BS (Z-Score filter or threshold filter) 
    ix3=np.where (np.abs(  data[:24]-np.nanmedian(data[:,24]) )  /  (1.483*np.nanmedian( np.abs(  data[:,24]-np.nanmedian(data[:,24]) ))) > BS_filter )[0]
#    data[ix3,:]=np.nan

    #Remove beam angles
    ix4=np.where(data[:,9]>=beammax2)[0]
#    data[ix4,:]=np.nan
    
    
    
    # remove final nan
    ix5=np.where(np.isnan(data[:,21])==True)[0]
#    data[ix5,:]=np.nan
    
    ix_out=np.r_[ix1,ix2,ix3,ix4,ix5]
    
    return data,header,ix_out


