# -*- coding: utf-8 -*-
"""
Created on Fri Mar 20 13:40:58 2020

@author: karaouli
"""
import numpy as np

def remove_bad_gps(data,no_gps):
    
    num_pings=data.shape[0]/256
    first_time=0
    ix=np.array([])
    if np.equal(np.mod(num_pings, 1), 0):
        print('Pings before GPS are ok:')
    else:
        print('!!!!Pings before GPS are not ok:')
    
    
    for i in range(0,np.int32(num_pings)):
        # if there are two many nan in the gps, something is wrong with the ping
        p1=np.count_nonzero(~np.isnan(data[i*256:(i+1)*256,19]))
        p2=np.count_nonzero(~np.isinf(data[i*256:(i+1)*256,19]))

        if (p1<=no_gps) | (p2<=no_gps):
            if first_time==0:
                ix=np.arange(i*256,(i+1)*256)
                first_time=1
            else:
                ix=np.r_[ix,np.arange(i*256,(i+1)*256)]
        
                        
            
    return ix        