# -*- coding: utf-8 -*-
"""
Created on Tue Feb 26 18:19:43 2019

@author: karaouli
"""

import numpy as np




def noiseFilter_fast(data,avg_pings,b_BS):
    # get average per ping
    
    
    mean1=np.zeros(np.int32(len(data[:,7])/256))
    for i in range(0,len(mean1)):
        mean1[i]=np.nanmean(data[i*256:(i+1)*256,7])   
    
    mean2=np.zeros(np.int32(len(data[:,7])/(256*avg_pings)+1))
    for i in range(0,len(mean2)-1):
        mean2[i]=np.nanmean(data[i*256*avg_pings:(i+1)*256*avg_pings,7])   
    # make lat batch
    mean2[i+1]=np.nanmean(data[(i+1)*256*avg_pings:,7])
    
    # get average every 10 pings
#    ids = np.arange(len(data[:,7]))//(256*avg_pings)
#    mean2=np.bincount(ids,data[:,7])/np.bincount(ids)
#    mean2=np.r_[mean2,mean2[-1]]    
    
    list1=np.arange(0,mean1.shape[0],avg_pings)
#    list1=np.unique(np.r_[list1,mean1.shape[0]]).astype('int32')
    
    ix=np.zeros((mean1.shape[0]))
    for k in range(0,mean1.shape[0]):
        # find where this ping belongs to
        ia=np.where((list1>=k ) )[0]
        if len(ia)>0:
            ia=ia[0]        
        else:
            ia=mean2.shape[0]-1
        if mean2[ia]-mean1[k]>b_BS:
            ix[k]=1
    
    ix_del=np.where(ix==1)[0]
    
    ixx=np.in1d(data[:,0],ix_del)
    ixx=np.where(ixx==True)[0]

#    data=np.delete(data,ixx,axis=0)
    
    return ixx
    