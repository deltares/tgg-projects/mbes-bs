# -*- coding: utf-8 -*-
"""
Created on Mon Jun 17 13:26:11 2019

@author: karaouli
"""

import numpy as np
from printProgressBar import printProgressBar



def local_arc(data,sliding_window,ref_ang,tol,tol2,beam_up,beam_low,BS_up,BS_low,BS_filter,theta_st,header):
#0-110 north part

    idx=np.where((data[:,24]<BS_low) | (data[:,24]>BS_up))[0]
    data[idx,3:]=np.nan
    
    
#    idx=np.where(np.abs(np.abs(data[:,25])-ref_ang) <=tol)[0]
#    data_ref=data[idx,:]
    #%% local AVG
    #% Loop over sliding window. Which is defined by certain numer of pings to
    sn=np.unique(data[:,2]) 
    #fre=np.unique(data[:,8]) 
    data=np.c_[data,np.nan*np.ones((data.shape[0],1))]
    header.extend(['BS_plot'])
    num_pings=data.shape[0]/256
    for i in range(0,np.int32(num_pings),100):
        printProgressBar(i, np.int32(num_pings)/100, prefix = 'BS plot Progress:', suffix = 'Complete', length = 50)
    #    print(i)
        if (i+100)*256<data.shape[0]:
            data_ref=data[i*256:(i+100)*256,:]
            iix=np.arange(i*256,(i+100)*256)
        else:
            data_ref=data[i*256:,:]
            iix=np.arange(i*256,data.shape[0])
        
            
        idx1=np.where((data_ref[:,2]==sn[0]) & (np.abs(np.abs(data_ref[:,25])-ref_ang) <=tol))[0]
        idx2=np.where((data_ref[:,2]==sn[1]) & (np.abs(np.abs(data_ref[:,25])-ref_ang) <=tol))[0]
        mean_ref1=np.nanmean(data_ref[idx1,24])
        std_ref1=np.nanstd(data_ref[idx1,24])
        
        mean_ref2=np.nanmean(data_ref[idx2,24])
        std_ref2=np.nanstd(data_ref[idx2,24])    
        data_ref=np.c_[data_ref,np.nan*np.ones((data_ref.shape[0],1))]
        
        for k in range(0,theta_st.shape[0]):
            idx3=np.where((data_ref[:,2]==sn[0])  & (np.abs(np.abs(data_ref[:,25])-theta_st[k]) <=tol2)) [0]
            idx4=np.where((data_ref[:,2]==sn[1])  & (np.abs(np.abs(data_ref[:,25])-theta_st[k]) <=tol2)) [0]
    
            data[iix[idx3],27]=((data_ref[idx3,24]-np.nanmean(data_ref[idx3,24]))/np.nanstd(data_ref[idx3,24])) * std_ref1  + mean_ref1
            data[iix[idx4],27]=((data_ref[idx4,24]-np.nanmean(data_ref[idx4,24]))/np.nanstd(data_ref[idx4,24])) * std_ref2  + mean_ref2


       
    return data, header
    


