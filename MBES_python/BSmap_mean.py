# -*- coding: utf-8 -*-
"""
Created on Fri Mar  1 16:51:21 2019

@author: karaouli
"""
# %function BSmap_MEAN(dirDATA, fileSpec, dirMAP, theta_st)
# %return;
# % This function creates MAPmean*.mat-files, collecting backscatter strength
# % per angle of incidence (CHANGE INTO BEAM ANGLE)
# % and side (port/starbord).
# % It considers the chaniging beam angle/incident angle due to the seabed
# % slope.
# % Averaging over a couple of pings and beams is applied to account for footprint effects,
# % dependend on the beam angle and water depth. First soundings are selected by
# % original beam angle and pings. Afterwards selected soundings are sorted considering
# % modified incident angle (considering slope) to account for location and new incident angle.
# %
# % Input data structure: data structure obtained from Processing.m
# % (_filter_slope.dat)
# %
# % 1 = ping; 2 = Easting(x); 3 = Northing(y); 4 = depth; 5 = backscatter;
# % 6 = beam; 7 = beam inclduing slope; 8= transducer depth;
# % 9 = heading; 10 = numb. scatterpixels; 11-14 =date;  15 = absoprtion
# % 16 = range to normal incidence; 17 = Range ; 18 =tx slope; 19 = ty slope;
# % 20 = total slope; 21 = corrected BS; 22 = corrected beam; 23 = BS
# % correction term; 24 = footprint singal new; 25 = footprint singnal old; 26
# % = footprint beam
# % Written by Timo C. Gaida (17.10.2017)
# % dirDATA: string, directory in which filterd data is stored
# % dirDATA2: string, directory in which produced files are temporarily% stored
# % dirMAP: string, directory in which MAPmean*.mat files containing BS per angle and side are stored
# % theta_st: vector of beam angles that need to be processed (0 deg at nadir, 90 deg at horizontal)
# %           often beams between beams of 20 deg and 60 deg (i.e. 70-30deg
# %           incidence) are used in the later classification process
# %           we use: theta_st = [60:-2:38 34 30 26 20]
# %              (or theta_grazing = [30:2:52 56 60 64 70])
#

import numpy as np
import matplotlib.pyplot as plt
from printProgressBar import printProgressBar


def BSmap_mean(data, theta_st, tol2, depth_bound, pings_num_2):
    data[:, 6] = np.abs(data[:, 6])
    sn = np.unique(data[:, 2])
    i1 = np.where(np.isfinite(sn))
    sn = sn[i1]

    idx_port = np.where(data[:, 2] == sn[0])[0]
    idx_portside = np.where(data[:, 2] == sn[1])[0]

    data_port = data[idx_port, :]
    data_portside = data[idx_portside, :]
    #    depth_mean_port=depth_mean[idx_port]
    #    depth_mean_portside=depth_mean[idx_portside]
    first_time = 0
    # make a guess for B1, B2 size
    B1 = np.nan * np.zeros((np.int32(len(data_port[:, 6])), 6))
    B2 = np.nan * np.zeros((np.int32(len(data_portside[:, 6])), 6))

    for i in range(0, np.int32(len(data_port[:, 6]) / 256)):
        printProgressBar(
            i,
            np.int32(len(data_port[:, 6]) / 256),
            prefix="Port Progress:",
            suffix="Complete",
            length=50,
        )
        cur_data = data_port[i * 256 : (i + 1) * 256, :]
        depth_mean = np.nanmean(cur_data[:, 6])
        A = np.array([])
        if np.isfinite(depth_mean):  # only work with pings that have data
            d = np.abs(depth_bound - depth_mean)
            ind1 = np.argmin(d)
            num_pings_needed = pings_num_2[ind1]
            cur_data_extend = data_port[i * 256 : (i + num_pings_needed + 1) * 256, :]
            # make sure we have two pings, 2nd pings should have some data
            depth_mean2 = np.nanmean(
                data_port[(i + 1) * 256 : (i + num_pings_needed + 1) * 256, 6]
            )
            if np.isfinite(depth_mean2):
                # loop over angles

                for beam_counter in range(0, theta_st.shape[0]):
                    beam = theta_st[beam_counter]
                    tol = tol2[beam_counter]
                    # Selection of data around beam inbetween angle of tolerance angle (tol)
                    idx_port = np.where(
                        (np.abs(np.abs(cur_data_extend[:, 25]) - beam) <= tol)
                    )[0]
                    data_slope = cur_data_extend[idx_port, :]
                    stop_idx = 1

                    if idx_port.shape[0] >= 2:  # make sure we have sometinh
                        while stop_idx == 1:
                            min_beam = np.nanmin(np.abs(data_slope[:, 9]))
                            idxx = np.where(
                                np.abs(data_slope[:, 9]) < 2 * tol + min_beam
                            )[0]
                            data_slope_filter = data_slope[idxx, :]
                            #                        % Filter backscatter data from beam angles using robust Z-scores (Rousseeuw and
                            #                        % Hubert 2011) before doing average over pings and beams
                            if idxx.shape[0] >= 2:  # make sure we have 2 data to go on
                                idx_filter = np.where(
                                    np.abs(
                                        data_slope_filter[:, 7]
                                        - np.nanmedian(data_slope_filter[:, 7])
                                    )
                                    / (
                                        1.483
                                        * np.nanmedian(
                                            np.abs(
                                                data_slope_filter[:, 7]
                                                - np.nanmedian(data_slope_filter[:, 7])
                                            )
                                        )
                                    )
                                    < 2.5
                                )[0]
                                data_slope_filter = data_slope_filter[idx_filter, :]
                                tmp = np.array(
                                    [
                                        beam_counter,
                                        np.nanmean(data_slope_filter[:, 19]),
                                        np.nanmean(data_slope_filter[:, 20]),
                                        np.nanmean(data_slope_filter[:, 6]),
                                        np.nanmedian(data_slope_filter[:, 24]),
                                        np.sum(data_slope_filter[:, 18]),
                                    ]
                                )
                                #                                A.append(tmp)
                                A = np.r_[A, tmp]
                            data_slope = np.delete(data_slope, idxx, axis=0)
                            if data_slope.shape[0] < 2:
                                stop_idx = 0
            # dont let A grow to much in size, it is slow
        if A.shape[0] >= 6:
            A = np.reshape(A, (np.int32(A.shape[0] / 6), 6))
            first_time = first_time + 1
        if first_time == 1:
            if A.shape[0] >= 6:
                B1[0 : A.shape[0], :] = A
                cur_pos = A.shape[0]
            else:
                cur_pos = 0
        else:
            if A.shape[0] > 0:
                B1[cur_pos : cur_pos + A.shape[0], :] = A
                cur_pos = cur_pos + A.shape[0]

    first_time = 0
    for i in range(0, np.int32(len(data_portside[:, 6]) / 256)):
        printProgressBar(
            i,
            np.int32(len(data_portside[:, 6]) / 256),
            prefix="Star Port side Progress:",
            suffix="Complete",
            length=50,
        )
        cur_data = data_portside[i * 256 : (i + 1) * 256, :]
        depth_mean = np.nanmean(cur_data[:, 6])
        A = np.array([])
        if np.isfinite(depth_mean):  # only work with pings that have data
            d = np.abs(depth_bound - depth_mean)
            ind1 = np.argmin(d)
            num_pings_needed = pings_num_2[ind1]
            cur_data_extend = data_portside[
                i * 256 : (i + num_pings_needed + 1) * 256, :
            ]
            # make sure we have two pings, 2nd pings should have some data
            depth_mean2 = np.nanmean(
                data_portside[(i + 1) * 256 : (i + num_pings_needed + 1) * 256, 6]
            )
            if np.isfinite(depth_mean2):
                # loop over angles

                for beam_counter in range(0, theta_st.shape[0]):
                    beam = theta_st[beam_counter]
                    tol = tol2[beam_counter]
                    # Selection of data around beam inbetween angle of tolerance angle (tol)
                    idx_port = np.where(
                        (np.abs(np.abs(cur_data_extend[:, 25]) - beam) <= tol)
                    )[0]
                    data_slope = cur_data_extend[idx_port, :]
                    stop_idx = 1

                    if idx_port.shape[0] >= 2:  # make sure we have sometinh
                        while stop_idx == 1:
                            min_beam = np.nanmin(np.abs(data_slope[:, 9]))
                            idxx = np.where(
                                np.abs(data_slope[:, 9]) < 2 * tol + min_beam
                            )[0]
                            data_slope_filter = data_slope[idxx, :]
                            #                        % Filter backscatter data from beam angles using robust Z-scores (Rousseeuw and
                            #                        % Hubert 2011) before doing average over pings and beams
                            if idxx.shape[0] >= 2:  # make sure we have 2 data to go on
                                idx_filter = np.where(
                                    np.abs(
                                        data_slope_filter[:, 7]
                                        - np.nanmedian(data_slope_filter[:, 7])
                                    )
                                    / (
                                        1.483
                                        * np.nanmedian(
                                            np.abs(
                                                data_slope_filter[:, 7]
                                                - np.nanmedian(data_slope_filter[:, 7])
                                            )
                                        )
                                    )
                                    < 2.5
                                )[0]
                                data_slope_filter = data_slope_filter[idx_filter, :]
                                tmp = np.array(
                                    [
                                        beam_counter,
                                        np.nanmean(data_slope_filter[:, 19]),
                                        np.nanmean(data_slope_filter[:, 20]),
                                        np.nanmean(data_slope_filter[:, 6]),
                                        np.nanmedian(data_slope_filter[:, 24]),
                                        np.sum(data_slope_filter[:, 18]),
                                    ]
                                )
                                #                                A.append(tmp)
                                A = np.r_[A, tmp]
                            data_slope = np.delete(data_slope, idxx, axis=0)
                            if data_slope.shape[0] < 2:
                                stop_idx = 0
            # dont let A grow to much in size, it is slow
        if A.shape[0] >= 6:
            A = np.reshape(A, (np.int32(A.shape[0] / 6), 6))
            first_time = first_time + 1
        if first_time == 1:
            if A.shape[0] >= 6:
                B2[0 : A.shape[0], :] = A
                cur_pos = A.shape[0]
            else:
                cur_pos = 0
        else:
            if A.shape[0] > 0:
                B2[cur_pos : cur_pos + A.shape[0], :] = A
                cur_pos = cur_pos + A.shape[0]

    # drop nan
    ix1 = np.where(np.isfinite(B1[:, 1]))[0]
    B1 = B1[ix1, :]
    ix2 = np.where(np.isfinite(B2[:, 1]))[0]
    B2 = B2[ix2, :]
    return B1, B2
