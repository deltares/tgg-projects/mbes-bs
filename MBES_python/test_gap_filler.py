# -*- coding: utf-8 -*-
"""
Created on Thu Jun 27 12:45:18 2019

@author: karaouli
"""

import numpy as np
import sys
sys.path.insert(0, r'D:\karaouli\Desktop\Projects\python_tools')
sys.path.insert(0, r'D:\karaouli\Desktop\Projects\python_tools\image_reconstruction-master')



from scipy import misc 
import tensorflow as tf
import imageio
from DataLoader import DataLoader
from Network import Network
from Trainer import Trainer



# Set parameters
patch_dims = [32,32]
image_dims = [512,512]
steps = [4,4]
input_units = patch_dims[0]*patch_dims[1]
learning_rate = 0.001
batch_size = 128
epoches = 100
validation_step = 2
hiddent_units = 512

# Create dataloader
dataLoader = DataLoader(patch_dims,image_dims,batch_size,steps)

# Create network
net = Network(input_units,learning_rate,hiddent_units)

# Create trainer
trainer = Trainer(net,batch_size,epoches,validation_step)

# Train the network
trainer.train(dataLoader)

# Test the network and save the image
g_best = trainer.test(dataLoader)

dataLoader.combine_and_save_image_patches(g_best[0], 'best')

# Close the tensorflow session opened in trainer
trainer.close()
    
