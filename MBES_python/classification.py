# -*- coding: utf-8 -*-
"""
Created on Thu May  7 14:32:44 2020

@author: karaouli
"""

import numpy as np
import pandas as pd
import matplotlib.pyplot as plt
from scipy.io import loadmat, savemat

import glob
from sklearn.mixture import GaussianMixture

import scipy.stats as stats

from scipy.stats import norm
import pickle


def solve(m1, m2, std1, std2, s1, s2):
    a = 1 / (2 * std1**2) - 1 / (2 * std2**2)
    b = m2 / (std2**2) - m1 / (std1**2)
    # c = m1**2 /(2*std1**2) - m2**2 / (2*std2**2) - np.log(std2/std1)
    c = (
        m1**2 / (2 * std1**2)
        - m2**2 / (2 * std2**2)
        - np.log((std2 * s1) / (std1 * s2))
    )

    return np.roots([a, b, c])


def chisqg(ydata, ymod, sd=None):
    """
    Returns the chi-square error statistic as the sum of squared errors between
    Ydata(i) and Ymodel(i). If individual standard deviations (array sd) are supplied,
    then the chi-square error statistic is computed as the sum of squared errors
    divided by the standard deviations.     Inspired on the IDL procedure linfit.pro.
    See http://en.wikipedia.org/wiki/Goodness_of_fit for reference.

    x,y,sd assumed to be Numpy arrays. a,b scalars.
    Returns the float chisq with the chi-square statistic.

    Rodrigo Nemmen
    http://goo.gl/8S1Oo
    """
    # Chi-square statistic (Bevington, eq. 6.9)
    if sd == None:
        chisq = np.sum((ydata - ymod) ** 2)
    else:
        chisq = np.sum(((ydata - ymod) / sd) ** 2)

    return chisq


def redchisqg(ydata, ymod, deg=2, sd=None):
    """
    Returns the reduced chi-square error statistic for an arbitrary model,
    chisq/nu, where nu is the number of degrees of freedom. If individual
    standard deviations (array sd) are supplied, then the chi-square error
    statistic is computed as the sum of squared errors divided by the standard
    deviations. See http://en.wikipedia.org/wiki/Goodness_of_fit for reference.

    ydata,ymod,sd assumed to be Numpy arrays. deg integer.

    Usage:
    >>> chisq=redchisqg(ydata,ymod,n,sd)
    where
     ydata : data
     ymod : model evaluated at the same x points as ydata
     n : number of free parameters in the model
     sd : uncertainties in ydata

    Rodrigo Nemmen
    http://goo.gl/8S1Oo
    """
    # Chi-square statistic
    if sd == None:
        chisq = np.sum((ydata - ymod) ** 2)
    else:
        chisq = np.sum(((ydata - ymod) / sd) ** 2)

    # Number of degrees of freedom assuming 2 free parameters
    nu = ydata.size - 1 - deg

    return chisq / nu


datapath_out = (
    "C:/Users/leentvaa/OneDrive - Stichting Deltares/Documents/testmbes/angle/"
)

# %% Choose beam angles (incident angles in case slope correction is applied) for which fitting procedure is applied
# %% Choose between a single beam or a range of beams to make the approach more robust (Timo C. Gaida)
beam_int = np.r_[
    np.arange(65, 61 - 1, -1), np.arange(60, 52 - 2, -2), np.arange(50, 38 - 3, -3)
]

# % reference side: 1=port(default), 2=starbord
side = np.array([1, 2])

# Values are good approximation for EM2040C in a water depth between 10 and 50m.
theta_st = np.r_[
    np.arange(64, 54 - 1, -1),
    np.arange(53, 45 - 1, -2),
    np.arange(43, 30 - 1, -3),
    np.arange(27, 3 - 1, -4),
]
pings_num_2 = np.r_[10, 4, 3, 2, 1]
depth_bound = np.r_[0, 5, 10, 15, 30]
# Tolerances for bounds of beam angles. Averaging over angles will take place inbetween these bounds,
# to compensate different number of scatterpixels within differnt angles
tol2 = np.ones((theta_st.shape[0]))
tol2[(theta_st >= 61) & (theta_st <= 74)] = 0.6
tol2[(theta_st >= 52) & (theta_st <= 60)] = 1.2
tol2[(theta_st >= 35) & (theta_st <= 50)] = 1.8
tol2[(theta_st >= 7) & (theta_st <= 32)] = 2.5

# %% Filter boundaries
scatterp = 5  # %number of scatter pixel
BSMIN = -50  # % lower BS bound
BSMAX = -5  # % upper BS bound
depth = np.array([3, 45])  # % depth boundaries

# %% MBES parameters
PL = (146 * 1e-6) * 0.37  # % pulse length (see Processing_Kongsberg.m)
soundsp = 1500  # % Sound speed
Om = 1.3 * np.pi / 180  # % across track beam width (see Processing_Kongsberg.m)

# %% testing the goodness of fit between the histogram and ng Gaussians
ng_start = 2  # % minimum number of classes
ng_end = 8  # % maximum number of classes

# %% decision on the upper and lower bound of the standard deviation %
# %disp('The decision on the lower and upper bound of the standard deviation')
# %disp('should be based on the actual standard deviation (green dots)')
# %disp('displayed in the center plot in figure 1.')
stdmin = 0.5
stdmax = 2.5

# %% Histogramm definition (Depends on backscatter value range)
hist_int = 0.5  # % bin interval
hist_filter = 0.01  # % filters out all bins in the histogram with number of BS values lower than hist_filter (0.01 = 1%)

# %% Visualization for detailed view on Gaussian fitting for a specific class and vessel side (see Figure 12)
vis_side = 1  # % starboard or portack side
vis_class = 4  # % Class number

# %% Initilize
kkk = 0
ff = 0

# fig = plt.figure(figsize=(16, 9) )
tmp1 = 1

results = dict()

for ii in range(0, beam_int.shape[0]):
    out3 = dict()
    for jj in range(0, len(side)):
        idx = np.argmin(np.abs(theta_st - beam_int[ii]))

        tol = tol2[idx]
        beam = theta_st[idx]

        data = loadmat(
            datapath_out + "MAPmean_" + str(side[jj]) + "_" + str(beam) + ".mat"
        )["data"]

        # %% Check if central limit theorem is valid
        I = np.where(data[:, 5] < scatterp)[0]
        data[I, :] = np.nan

        # %% accounting for depth boundaries
        I = np.where((data[:, 3] < depth[0]) | (data[:, 3] > depth[1]))[0]
        data[I, :] = np.nan

        # %% accounting for backscatter boundaries
        I = np.where((data[:, 4] < BSMIN) | (data[:, 4] > BSMAX))[0]
        data[I, :] = np.nan

        # %% determine the beam footprint %
        # %% minimal an maximal beams of consideration (beams are defined by their
        # %% central beam angle)
        # beam_min = beam - tol
        # beam_max = beam + tol
        # angle_min = beam_min*(np.pi/180)
        # angle_max = beam_max*(np.pi/180)

        # # %% caclulate the actual coverage from all beams included
        # # %% acrosstrack size of beam footprint
        # # %% in case angle_min and angle_max are beam angel (not angle of incidence)
        # # %%Z_all = 80*ones(length(Z_all),1);
        # dX_beam = data[:,3]*np.abs(np.tan(angle_min)-np.tan(angle_max))
        # # %% the following actually also accounts for alongtrack changes in size
        # # %% (however the component R*Om omitted since it cancels out with the same
        # # %% R*Om in the alongttrack size of the scatterpixels)

        # for kk in range(0,depth_bound.shape[0]):
        #     if kk<depth_bound.shape[0]-1:
        #         ix=np.where((data[:,3]>depth_bound[kk]) & (  data[:,3]<depth_bound[kk+1] ))[0]
        #     else:
        #         ix=np.where((data[:,3]>depth_bound[kk]  ))[0]
        #     dX_beam[ix]=dX_beam[ix]* pings_num_2[kk]

        # # %% use average scatter pixel size over inner and outer beam
        # dX_scatterpixel_min = (soundsp*PL)/(2*np.sin(angle_min))
        # dX_scatterpixel_max = (soundsp*PL)/(2*np.sin(angle_max))
        # dX_scatterpixel = np.nanmean([dX_scatterpixel_min,dX_scatterpixel_max])
        # Ns = dX_beam/dX_scatterpixel
        # I = np.where(Ns < 1)[0]
        # Ns[I] = 1
        # # % corresponding expected standard deviations
        # T_std_1 = 10*np.pi*np.log10(np.exp(1))/np.sqrt(6)
        # T_std_nsm = T_std_1/np.sqrt(data[:,5])
        # T_std_Ns = T_std_1/np.sqrt(Ns)

        # remove 1% edges
        X = data[:, 4]
        x = np.arange(BSMIN - hist_int / 2, BSMAX + hist_int / 2 + hist_int, hist_int)
        n, binEdges = np.histogram(X, x)
        bincenters = 0.5 * (binEdges[1:] + binEdges[:-1])
        I = np.where(n > hist_filter * np.max(n))[0]

        # find new min,max
        BSmin = np.min(bincenters[I])
        BSmax = np.max(bincenters[I])
        # exclude those poiunts from classification
        iii = np.where((data[:, 4] >= BSmin) & (data[:, 4] <= BSMAX))[0]
        X = data[iii, 4]
        # plt.hist(X,x)

        # Now calculate new x_edges
        x_new = np.arange(
            BSmin - hist_int / 2, BSmax + hist_int / 2 + hist_int, hist_int
        )
        y, binEdges = np.histogram(X, x_new, density="True")
        bincenters = 0.5 * (binEdges[1:] + binEdges[:-1])

        # chane here for the numer of gaussians
        N = np.arange(2, 9, 1)

        y_score = np.zeros(N.shape)
        models = [None for ii in range(len(N))]
        for ng in range(len(N)):
            BSm = np.linspace(BSmin, BSmax, N[ng])
            # models[ng] = GaussianMixture(n_components=N[ng],covariance_type="spherical", tol=1e-4,max_iter=100,n_init=1).fit(np.expand_dims(X,1))
            models[ng] = GaussianMixture(
                n_components=N[ng],
                covariance_type="spherical",
                tol=1e-4,
                max_iter=100,
                n_init=1,
                means_init=np.expand_dims(BSm, 1),
            ).fit(np.expand_dims(X, 1))
        # compute the AIC and the BIC
        AIC = [m.aic(np.expand_dims(X, 1)) for m in models]
        BIC = [m.bic(np.expand_dims(X, 1)) for m in models]

        fig = plt.figure(figsize=(32, 18))
        ax = fig.add_subplot(2, 4, 1)

        lns1 = ax.plot(N, AIC / np.max(AIC), c="r", label="AIC")
        lns2 = ax.plot(N, BIC / np.max(AIC), c="g", label="BIC")
        ax2 = ax.twinx()
        out2 = dict()
        for ng in range(0, len(N)):
            ax = fig.add_subplot(2, 4, ng + 2)

            M_best = models[ng]
            weights = M_best.weights_
            means = M_best.means_
            covars = M_best.covariances_

            logprob = M_best.score_samples(bincenters.reshape(-1, 1))

            responsibilities = M_best.predict_proba(bincenters.reshape(-1, 1) + 0.25)
            pdf = np.exp(logprob)

            pdf_individual = responsibilities * pdf[:, np.newaxis]
            #     # ax = fig.add_subplot(2,4,i+2)
            #     # ax.hist(X+hist_int/2, x, density=True, histtype='stepfilled', alpha=0.4)
            ax.plot(bincenters, y, "-b")
            ax.plot(bincenters, pdf, "-r")
            ax.plot(bincenters, pdf_individual, "--k")
            nu = len(y) - 1 - 3 * 5
            y_score[ng] = np.sum(((pdf - y) ** 2) / y) / nu

            for k in range(0, len(weights)):
                #         # ax.plot(x_new,weights[k]*stats.norm.pdf(x_new,means[k],np.sqrt(covars[k])).ravel(), c='red')
                #     # ax.plot(x_new,(1/np.sqrt(2*np.pi*np.sqrt(covars[k])))*stats.norm.pdf(x_new,means[k],np.sqrt(covars[k])).ravel(), c='green')
                ax.plot(
                    bincenters,
                    (1 / np.sqrt(2 * np.pi * covars[k]))
                    * stats.norm.pdf(bincenters, means[k], np.sqrt(covars[k])).ravel(),
                    c="green",
                )

            ax.text(
                0.04,
                0.96,
                "Best-fit Mixture",
                ha="left",
                va="top",
                transform=ax.transAxes,
            )
            ax.set_xlabel("$dB$")
            ax.set_ylabel("$p(x)$")
            ax.set_title("%d Gauusian" % N[ng])

            # this is to order the gaussian in increasing order
            sort_indices = M_best.means_.argsort(axis=0)
            order = sort_indices[:, 0]

            weights = weights[order]
            means = means[order]
            covars = covars[order]

            # find boundaries of classes
            roots = np.zeros((N[ng] - 1, 2))
            roots_store = np.zeros((N[ng] - 1, 1))
            for sol in range(0, N[ng] - 1):
                # roots[sol,:]=solve(means[sol,0],means[sol+1,0],np.sqrt(covars[sol]),np.sqrt(covars[sol+1]),weights[sol],weights[sol+1])

                roots[sol, :] = solve(
                    means[sol, 0],
                    means[sol + 1, 0],
                    np.sqrt(covars[sol]),
                    np.sqrt(covars[sol + 1]),
                    (1 / np.sqrt(2 * np.pi * covars[sol])),
                    (1 / np.sqrt(2 * np.pi * covars[sol + 1])),
                )

                a1 = (1 / np.sqrt(2 * np.pi * covars[sol])) * norm.pdf(
                    roots[sol, :], means[sol, 0], np.sqrt(covars[sol])
                )
                a2 = (1 / np.sqrt(2 * np.pi * covars[sol + 1])) * norm.pdf(
                    roots[sol, :], means[sol + 1, 0], np.sqrt(covars[sol + 1])
                )

                i1 = np.argmax(a1)
                i2 = np.argmax(a2)
                if i1 != i2:
                    print("error")
                else:
                    ib = i1
                    ax.plot(roots[sol, ib], a1[ib], "bx", markersize=24)
                    # ax.plot(roots[sol,ib],a2[ib],'x')
                    roots_store[sol] = roots[sol, ib]

            out = {
                "MeanBS": np.nanmean(X),
                "Mean Per Class": means,
                "Covarience per class": covars,
                "Weight per class": weights,
                "Boundaries": roots_store,
            }

            out2["Class No:%d" % N[ng]] = out

        ax = fig.add_subplot(2, 4, 1)

        lns3 = ax2.plot(N, y_score / np.max(y_score), c="b", label="X2 test")

        lns = lns1 + lns2 + lns3
        labs = [l.get_label() for l in lns]
        ax.legend(lns, labs, loc=0)
        ax.set_title("Angle %d" % beam)

        plt.savefig("TES_2018_Angle%d, Side%d.png" % (beam, side[jj]))
        plt.close()
        out3["Side:%d" % side[jj]] = out2

    results["Angle:%d" % beam] = out3


with open(
    "C:/Users/leentvaa/OneDrive - Stichting Deltares/Documents/testmbes/nord_2017.p",
    "wb",
) as fp:
    # json.dump(data,fp,ensure_ascii=False)
    pickle.dump(results, fp, protocol=pickle.HIGHEST_PROTOCOL)
