# -*- coding: utf-8 -*-
"""
Created on Tue May 12 19:45:18 2020

@author: karaouli
"""
import os
import numpy as np
import pandas as pd
import matplotlib.pyplot as plt
from scipy.io import loadmat, savemat
from pathlib import Path
import glob
from sklearn.mixture import GaussianMixture

import scipy.stats as stats

from scipy.stats import norm
import pickle


def solve(m1, m2, std1, std2, s1, s2):
    a = 1 / (2 * std1**2) - 1 / (2 * std2**2)
    b = m2 / (std2**2) - m1 / (std1**2)
    # c = m1**2 /(2*std1**2) - m2**2 / (2*std2**2) - np.log(std2/std1)
    c = (
        m1**2 / (2 * std1**2)
        - m2**2 / (2 * std2**2)
        - np.log((std2 * s1) / (std1 * s2))
    )

    return np.roots([a, b, c])


datapath_out = "C:/Users/leentvaa/OneDrive - Stichting Deltares/Documents/testmbes/"

# %% Choose beam angles (incident angles in case slope correction is applied) for which fitting procedure is applied
# %% Choose between a single beam or a range of beams to make the approach more robust (Timo C. Gaida)
beam_int = np.r_[
    np.arange(65, 61 - 1, -1), np.arange(60, 52 - 2, -2), np.arange(50, 38 - 3, -3)
]

# % reference side: 1=port(default), 2=starbord
side = np.array([1, 2])

# Values are good approximation for EM2040C in a water depth between 10 and 50m.
theta_st = np.r_[
    np.arange(64, 54 - 1, -1),
    np.arange(53, 45 - 1, -2),
    np.arange(43, 30 - 1, -3),
    np.arange(27, 3 - 1, -4),
]
pings_num_2 = np.r_[10, 4, 3, 2, 1]
depth_bound = np.r_[0, 5, 10, 15, 30]
# Tolerances for bounds of beam angles. Averaging over angles will take place inbetween these bounds,
# to compensate different number of scatterpixels within differnt angles
tol2 = np.ones((theta_st.shape[0]))
tol2[(theta_st >= 61) & (theta_st <= 74)] = 0.6
tol2[(theta_st >= 52) & (theta_st <= 60)] = 1.2
tol2[(theta_st >= 35) & (theta_st <= 50)] = 1.8
tol2[(theta_st >= 7) & (theta_st <= 32)] = 2.5

# %% Filter boundaries
scatterp = 5  # %number of scatter pixel
BSMIN = -50  # % lower BS bound
BSMAX = -5  # % upper BS bound
depth = np.array([3, 55])  # % depth boundaries

# %% MBES parameters
PL = (146 * 1e-6) * 0.37  # % pulse length (see Processing_Kongsberg.m)
soundsp = 1500  # % Sound speed
Om = 1.3 * np.pi / 180  # % across track beam width (see Processing_Kongsberg.m)

# %% testing the goodness of fit between the histogram and ng Gaussians
ng_start = 2  # % minimum number of classes
ng_end = 8  # % maximum number of classes

# %% decision on the upper and lower bound of the standard deviation %
# %disp('The decision on the lower and upper bound of the standard deviation')
# %disp('should be based on the actual standard deviation (green dots)')
# %disp('displayed in the center plot in figure 1.')
stdmin = 0.5
stdmax = 2.5

# %% Histogramm definition (Depends on backscatter value range)
hist_int = 0.5  # % bin interval
hist_filter = 0.01  # % filters out all bins in the histogram with number of BS values lower than hist_filter (0.01 = 1%)

# %% Visualization for detailed view on Gaussian fitting for a specific class and vessel side (see Figure 12)
vis_side = 1  # % starboard or portack side
vis_class = 4  # % Class number

# %% Initilize
kkk = 0
ff = 0

# fig = plt.figure(figsize=(16, 9) )
tmp1 = 1


# Which angle to use
ref_angle = np.array([64])
# Which side to use
side = 1
ng = 3


with open(os.path.join(datapath_out, "nord_2017.p"), "rb") as fp:
    data = pickle.load(fp)
    print(data["Angle:64"]["Side:1"]["Class No:2"]["MeanBS"])
# with open(r'd:\\backscattered\\nord_2017.p', 'rb') as fp:
#    data = pickle.load(fp)

filename = glob.glob(r"E:\\*ARCA.all")
for i in range(0, len(ref_angle)):
    if i == 0:
        M_all = data["Angle:%d" % ref_angle[i]]["Side:%d" % side]["Class No:%d" % ng][
            "Mean Per Class"
        ]
        BSmean = data["Angle:%d" % ref_angle[i]]["Side:%d" % side]["Class No:%d" % ng][
            "MeanBS"
        ]
    else:
        M_all = np.c_[
            M_all,
            data["Angle:%d" % ref_angle[i]]["Side:%d" % side]["Class No:%d" % ng][
                "Mean Per Class"
            ],
        ]
        BSmean = np.c_[
            BSmean,
            data["Angle:%d" % ref_angle[i]]["Side:%d" % side]["Class No:%d" % ng][
                "MeanBS"
            ],
        ]
if len(ref_angle) > 1:
    BSclassmean = np.nanmean(
        M_all, axis=1
    )  # mean values of the classes at reference beams
    BSmean = np.nanmean(
        BSmean
    )  # %mean of all BS values (in all classes) at reference beams
else:
    BSclassmean = np.nanmean(
        M_all, axis=1
    )  # mean values of the classes at reference beams
    BSmean = np.nanmean(
        BSmean
    )  # %mean of all BS values (in all classes) at reference beams


p = np.zeros((ng, len(ref_angle)))
fig = plt.figure(figsize=(32, 18))
ax = fig.add_subplot(1, 1, 1)
for ii in range(0, ref_angle.shape[0]):
    beam = ref_angle[ii]
    data = loadmat(
        datapath_out + "angle/Mapmean_" + str(side) + "_" + str(beam) + ".mat"
    )["data"]
    # data = loadmat(
    #   datapath_out + "mean\\" + os.path.basename(filename[0])[:-4] + "_side1.mat"
    # )["data"]
    # %% Check if central limit theorem is valid
    I = np.where(data[:, 5] < scatterp)[0]
    data[I, :] = np.nan

    # %% accounting for depth boundaries
    I = np.where((data[:, 3] < depth[0]) | (data[:, 3] > depth[1]))[0]
    data[I, :] = np.nan

    # %% accounting for backscatter boundaries
    I = np.where((data[:, 4] < BSMIN) | (data[:, 4] > BSMAX))[0]
    data[I, :] = np.nan

    # Shift BS values
    if len(ref_angle) > 1:
        data[:, 4] = data[:, 4] + BSmean - np.nanmean(data[:, 4])
        BSMIN = BSMIN + BSmean - np.nanmean(data[:, 4])
        BSMAX = BSMAX + BSmean - np.nanmean(data[:, 4])

    # remove 1% edges
    X = data[:, 4]
    x = np.arange(BSMIN - hist_int / 2, BSMAX + hist_int / 2 + hist_int, hist_int)
    n, binEdges = np.histogram(X, x)
    bincenters = 0.5 * (binEdges[1:] + binEdges[:-1])
    I = np.where(n > hist_filter * np.max(n))[0]

    # find new min,max
    BSmin = np.min(bincenters[I])
    BSmax = np.max(bincenters[I])
    # exclude those poiunts from classification
    iii = np.where((data[:, 4] >= BSmin) & (data[:, 4] <= BSMAX))[0]
    X = data[iii, 4]

    # Now calculate new x_edges
    x_new = np.arange(BSmin - hist_int / 2, BSmax + hist_int / 2 + hist_int, hist_int)
    y, binEdges = np.histogram(X, x_new, density="True")
    bincenters = 0.5 * (binEdges[1:] + binEdges[:-1])

    BSm = BSclassmean
    models = GaussianMixture(
        n_components=ng,
        covariance_type="spherical",
        tol=1e-4,
        max_iter=100,
        n_init=1,
        means_init=np.expand_dims(BSm, 1),
    ).fit(np.expand_dims(X, 1))

    M_best = models
    weights = M_best.weights_
    means = M_best.means_
    covars = M_best.covariances_

    logprob = M_best.score_samples(bincenters.reshape(-1, 1))

    responsibilities = M_best.predict_proba(bincenters.reshape(-1, 1) + 0.25)
    pdf = np.exp(logprob)

    pdf_individual = responsibilities * pdf[:, np.newaxis]
    #     # ax = fig.add_subplot(2,4,i+2)
    #     # ax.hist(X+hist_int/2, x, density=True, histtype='stepfilled', alpha=0.4)
    ax.plot(bincenters, y, "-b")
    ax.plot(bincenters, pdf, "-r")
    ax.plot(bincenters, pdf_individual, "--k")

    for k in range(0, len(weights)):
        #         # ax.plot(x_new,weights[k]*stats.norm.pdf(x_new,means[k],np.sqrt(covars[k])).ravel(), c='red')
        #     # ax.plot(x_new,(1/np.sqrt(2*np.pi*np.sqrt(covars[k])))*stats.norm.pdf(x_new,means[k],np.sqrt(covars[k])).ravel(), c='green')
        ax.plot(
            bincenters,
            (1 / np.sqrt(2 * np.pi * covars[k]))
            * stats.norm.pdf(bincenters, means[k], np.sqrt(covars[k])).ravel(),
            c="green",
        )

    ax.text(0.04, 0.96, "Best-fit Mixture", ha="left", va="top", transform=ax.transAxes)
    ax.set_xlabel("$dB$")
    ax.set_ylabel("$p(x)$")
    ax.set_title("%d Gaussian" % ng)

    sort_indices = M_best.means_.argsort(axis=0)
    order = sort_indices[:, 0]

    weights = weights[order]
    means = means[order]
    covars = covars[order]

    # find boundaries of classes
    roots = np.zeros((ng - 1, 2))
    roots_store = np.zeros((ng - 1, 1))
    for sol in range(0, ng - 1):
        # roots[sol,:]=solve(means[sol,0],means[sol+1,0],np.sqrt(covars[sol]),np.sqrt(covars[sol+1]),weights[sol],weights[sol+1])

        roots[sol, :] = solve(
            means[sol, 0],
            means[sol + 1, 0],
            np.sqrt(covars[sol]),
            np.sqrt(covars[sol + 1]),
            (1 / np.sqrt(2 * np.pi * covars[sol])),
            (1 / np.sqrt(2 * np.pi * covars[sol + 1])),
        )

        a1 = (1 / np.sqrt(2 * np.pi * covars[sol])) * norm.pdf(
            roots[sol, :], means[sol, 0], np.sqrt(covars[sol])
        )
        a2 = (1 / np.sqrt(2 * np.pi * covars[sol + 1])) * norm.pdf(
            roots[sol, :], means[sol + 1, 0], np.sqrt(covars[sol + 1])
        )

        i1 = np.argmax(a1)
        i2 = np.argmax(a2)
        if i1 != i2:
            print("error")
        else:
            ib = i1
            ax.plot(roots[sol, ib], a1[ib], "bx", markersize=24)
            # ax.plot(roots[sol,ib],a2[ib],'x')
            plt.show()
            roots_store[sol] = roots[sol, ib]

    # %% assign backscatter to classes and calculate precentages of the classes
    B = np.r_[-np.inf, roots_store[:, 0], np.inf]

    num_el = np.count_nonzero(~np.isnan(X))

    for sol in range(0, ng):
        I1 = np.where((X > B[sol]) & (X < B[sol + 1]))[0]
        p[sol, ii] = len(I1) / num_el


if len(ref_angle) > 1:
    Pmean = np.nanmean(p, axis=1)
else:
    Pmean = p


np.save(
    "refbeam_values" + str(ref_angle) + "_side_%d" % side + "_class_%d" % ng + ".npy",
    Pmean,
)
