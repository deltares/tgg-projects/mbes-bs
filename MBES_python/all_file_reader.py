# -*- coding: utf-8 -*-
"""
Created on Tue Jan 29 15:21:29 2019
This file takes as an input a .all file and returns a matrix with all neccessary 
values for backsacktter classification
@author: karaouli
"""

from pyall import ALLReader
import time
from datetime import datetime
from datetime import timedelta
import numpy as np
import pyproj
import pandas as pd
import sys
import gc
from scipy.interpolate import interp1d
import matplotlib.pyplot as plt
from printProgressBar import printProgressBar

def all_file_reader(filename):
    # * The function mb_coor_scale.c returns scaling factors
    # * to turn longitude and latitude differences into distances in meters.
    # * This function is based on code by James Charters (Scripps Institution
    # * of Oceanography).
    # * Author:	D. W. Caress
    # * Date:	January 21, 1993
    C1 =111412.84
    C2 =-93.5
    C3 =0.118
    C4 =111132.92
    C5 =-559.82
    C6 =1.175
    C7 =0.0023
    myProj = pyproj.Proj("+proj=utm +zone=31 +north +ellps=WGS84 +datum=WGS84 +units=m +no_defs")
    rd_p = pyproj.Proj(init='epsg:28992') 
    rd_p2 = pyproj.Proj(init='epsg:7415') 
    utm_xy_31 = pyproj.Proj(init='epsg:32631') 
    utm_xy_32 = pyproj.Proj(init='epsg:32632') 
    wgs84_p = pyproj.Proj(init='epsg:4326')
    etrs89_p = pyproj.Proj(init='epsg:4258')
    start_time = time.time() # time the process
    r = ALLReader(filename)
    first_time=0
    
    
    navigation = np.asarray(r.loadNavigation())
    # make here interpolation for lat,lon\
    radlat =  np.deg2rad(navigation[:,1])
    mtodeglat = 1. / np.abs(C4 + C5 * np.cos(2 * radlat) + C6 * np.cos(4 * radlat) + C7 * np.cos(6 * radlat));
    mtodeglon = 1. / np.abs(C1 * np.cos(radlat) + C2 * np.cos(3 * radlat) + C3 * np.cos(5 * radlat));
    #dx=(navigation[1:,1]-navigation[:-1,1])/mtodeglon[1:]
    #dy=(navigation[1:,2]-navigation[:-1,2])/mtodeglon[1:]
    #dt=navigation[1:,0]-navigation[:-1,0]
    #speed=3.6*np.sqrt(np.power(dx,2)+np.power(dy,2))/dt
    
    flat=interp1d(navigation[:,0],navigation[:,1],kind='linear',fill_value='extrapolate')
    flon=interp1d(navigation[:,0],navigation[:,2],kind='linear',fill_value='extrapolate')
    
    
    
    # preallocation of memory
    pingCount_N=0
    pingCount_X=0
    pingCount_Y=0
    while r.moreData():
        typeOfDatagram, datagram = r.readDatagram()
        # we read the P(Position)
        if typeOfDatagram == 'N':
            pingCount_N=pingCount_N+1
        if typeOfDatagram == 'X':
            pingCount_X=pingCount_X+1
        if typeOfDatagram == 'Y':
            pingCount_Y=pingCount_Y+1        
         
    # Check here that datagrams have the same size. If not eoore
    pingCount=np.unique(np.c_[pingCount_N,pingCount_X,pingCount_Y])
    if pingCount.shape[0]==1:
        print('All Datagrams found')
        memory=(pingCount[0])*256
    else:
        print('X=%d, Y=%d, N=%d'%(pingCount_X,pingCount_Y,pingCount_N))
        memory=(np.max(pingCount))*256
    
    memoryx=(pingCount_X)*256
    memoryy=(pingCount_Y)*256
    memoryn=(pingCount_N)*256   
    
    # For now, I will store everything in 3 different matrices.
    data_x=np.zeros((memoryx,10))
    data_y=np.zeros((memoryy,5))
    data_n=np.zeros((memoryn,8))
    
    r.rewind()
    # vector to use when single value is exctracted   
    m256=np.ones((256,))
    
    
    # matrices to keep for index
    id_x=np.zeros((pingCount_X,2))    
    id_y=np.zeros((pingCount_Y,2))    
    id_n=np.zeros((pingCount_N,2)) 
    
    # Remember, in the begging there is no lat, lon for 1-2 pings
    lat=0
    lon=0
    utmx=0
    utmy=0
    heading=0
    height=0
    radlat =   np.deg2rad(lat)
    mtodeglat = 1. / np.abs(C4 + C5 * np.cos(2 * radlat) + C6 * np.cos(4 * radlat) + C7 * np.cos(6 * radlat));
    mtodeglon = 1. / np.abs(C1 * np.cos(radlat) + C2 * np.cos(3 * radlat) + C3 * np.cos(5 * radlat));
    first_time=0
    first_time_x=0
    height=0
    
    pingCount2=1
    CountX=0
    CountN=0
    CountY=0
    while r.moreData():
        typeOfDatagram, datagram = r.readDatagram()
        rawbytes = r.readDatagramBytes(datagram.offset, datagram.numberOfBytes)
        
        # we read the P(Position)
        if typeOfDatagram == 'P':
            datagram.read()
            lat=datagram.Latitude
            lon=datagram.Longitude
            utmx,utmy = pyproj.transform(wgs84_p,utm_xy_31,lon,lat)
            if first_time==0:
                radlat =  np.deg2rad(lat)
                heading=datagram.Heading
                mtodeglat = 1. / np.abs(C4 + C5 * np.cos(2 * radlat) + C6 * np.cos(4 * radlat) + C7 * np.cos(6 * radlat));
                mtodeglon = 1. / np.abs(C1 * np.cos(radlat) + C2 * np.cos(3 * radlat) + C3 * np.cos(5 * radlat));
                first_time=0
        # we read the height (h)
        if typeOfDatagram == 'h':
            datagram.read()
            height=datagram.Height
  
      # we read the Raw range and angle
        if typeOfDatagram == 'N':
            
            datagram.read()
            CsT_4eh= np.asarray(datagram.SoundSpeedAtTransducer)/10
            DI_4eh= np.asarray(datagram.DetectionInfo)
            TT_4eh = np.asarray(datagram.TwoWayTravelTime)
            
            try:
                Counter_4eh=np.asarray(datagram.Counter)
            except:
                Counter_4eh=np.asarray(datagram.PingCounter)  
                
            AC_4eh = np.asarray(datagram.MeanAbsorption)/100    # make it a vactor?
            BA_4eh = np.asarray(datagram.BeamPointingAngle)   # check again the 100
            SL_4eh = np.asarray(datagram.SignalLength) #make it a vector?
            CF_4eh=np.asarray(datagram.CentreFrequency)
            SN_4eh=np.asarray(datagram.SerialNumber)
            
            # Calulte range from Timo's formula
            R=CsT_4eh*TT_4eh/2
            data_n[CountN*256:(CountN+1)*256,:]=np.c_[Counter_4eh*m256,SN_4eh*m256,CF_4eh*m256,CsT_4eh*m256,SL_4eh*m256,BA_4eh,AC_4eh*m256,R]
            id_n[CountN,:]=np.c_[Counter_4eh,SN_4eh]
            CountN=CountN+1
        
        # we read the XYZ 88
        if typeOfDatagram == 'X':
            datagram.read()    
            count=datagram.Counter*m256
            depth=np.asarray(datagram.Depth)
            datagram.BeamIncidenceAngleAdjustment
            x1=np.asarray(datagram.AcrossTrackDistance)
            y1=np.asarray(datagram.AlongTrackDistance)
#            heading=datagram.Heading
            bs=np.asarray(datagram.Reflectivity)
            sn=datagram.SerialNumber
            transducer_depth=np.asarray(datagram.TransducerDepth)
            depth=-(np.asarray(datagram.TransducerDepth)+np.asarray(datagram.Depth))
            
            # Calulate Grazing angle for flat bottom (see mblist.c)
            beam=np.rad2deg(np.arctan2(x1,-depth-transducer_depth))
            # Calulate current lat lon for every N,X,Y datagram (see mblist.c)
            
            date_object = (datetime.strptime(str(datagram.RecordDate), '%Y%m%d') + timedelta(0,datagram.Time) - 
                                 datetime(1970, 1, 1)  ).total_seconds()
                                
            if first_time_x==0: # keep 1st referecne
                date_object_ref=date_object
                first_time_x=1
                
            if date_object-date_object_ref<0.0101:# the 10ms difference, keep old value
                date_object=date_object_ref

            if (date_object>=0.9*np.min(navigation[:,0]))  & (date_object<=1.1*np.max(navigation[:,0])  ):
                dlon=flon(date_object)
                dlat=flat(date_object)
            else:
                dlon=0
                dlat=0
            
            headingy=np.cos(np.deg2rad(heading))
            headingx=np.sin(np.deg2rad(heading))
            
            dlon=dlon+ headingy*mtodeglon*x1 + headingx*mtodeglon*y1
            dlat=dlat - headingx*mtodeglat*x1 + headingy*mtodeglat*y1

            data_x[CountX*256:(CountX+1)*256,:]=np.c_[count*m256,sn*m256,dlon,dlat,height*m256,depth,bs,beam,transducer_depth*m256,heading*m256]
            id_x[CountX,:]=np.c_[count[0],sn]
            
            # update date_object_ref
            date_object_ref=date_object
            
            
            CountX=CountX+1
            
        
        # we read the Seabed image 89
        if typeOfDatagram == 'Y':
            datagram.read()    
            corr_59h=np.asarray(datagram.RangeToNormalIncidence)
            FS_59h=np.asarray(datagram.SampleFrequency)
            ping_59h=np.asarray(datagram.Counter)
            SN_59h=np.asarray(datagram.SerialNumber)
            
            # Calculata it based on Timo's forula
            TTI=corr_59h/FS_59h #traveltime to normal indicdece
            
            # Get number of beams
            nss=np.zeros((256,))
            for k in range(0,256):
                nss[k]=datagram.beams[k].numberOfSamplesPerBeam
                
  
    
            data_y[CountY*256:(CountY+1)*256,:]=np.c_[ping_59h*m256,SN_59h*m256,FS_59h*m256,TTI*m256,nss]
            id_y[CountY,:]=np.c_[ping_59h,SN_59h]
            CountY=CountY+1
            

            printProgressBar(pingCount2, np.max(pingCount), prefix = 'Read File Progress:', suffix = 'Complete', length = 50)
            pingCount2=pingCount2+1  # Careful, we only care about pingcounts in those datagrams
        
        
    
    
    
    #File read finish, store to one matrix
    
        
    r.rewind()
    r.close()    
    
    
    
    # Suncronize N,X,Y to start and end from the same ping and SN 
    # find all common pings and SN
    id_nxy,index=np.unique(np.r_[id_n,id_x,id_y],axis=0,return_index=True)
    id_nxy=np.r_[id_n,id_x,id_y][np.sort(index)]
    id_nxy_rows=set(map(tuple,id_nxy))
    
    ix_rows=set(map(tuple,data_x[:,:2]))
    iy_rows=set(map(tuple,data_y[:,:2]))
    in_rows=set(map(tuple,data_n[:,:2]))
    
    # find from subset if some pings/sn are missing
    ix=id_nxy_rows.difference(ix_rows)
    iy=id_nxy_rows.difference(iy_rows)
    inn=id_nxy_rows.difference(in_rows)
    
    ix=np.asarray(list(ix))
    iy=np.asarray(list(iy))
    inn=np.asarray(list(inn))
    
    
    
    
    if iy.shape[0]==0:
        iy=np.array([[],[]]).T
    if ix.shape[0]==0:
        ix=np.array([[],[]]).T    
    if inn.shape[0]==0:
        inn=np.array([[],[]]).T    
    # pings to drop
    in_all=np.r_[ix,iy,inn]
    if in_all.shape[0]>0:
        in_all=np.unique(in_all,axis=0)
    
    
    for i in range(0,in_all.shape[0]):
        iall=np.where((id_nxy[:,0]==in_all[i,0]) & (id_nxy[:,1]==in_all[i,1]))[0]
        if i==0:
            i_out=iall
        else:
            i_out=np.r_[i_out,iall]
    if in_all.shape[0]>0:
        id_nxy=np.delete(id_nxy,(i_out),axis=0)

    
    ixx_all=np.array([[]])
    iyy_all=np.array([[]])
    inn_all=np.array([[]])
    for i in range(0,in_all.shape[0]):
        ixx=np.where( (data_x[:,0]==in_all[i,0]) & (data_x[:,1]==in_all[i,1])  )[0]
        iyy=np.where( (data_y[:,0]==in_all[i,0]) & (data_y[:,1]==in_all[i,1])  )[0]
        inn=np.where( (data_n[:,0]==in_all[i,0]) & (data_n[:,1]==in_all[i,1])  )[0]
        
        
        if i==0:
            ixx_all=ixx
            iyy_all=iyy
            inn_all=inn
        else:
            ixx_all=np.r_[ixx_all,ixx]
            iyy_all=np.r_[iyy_all,iyy]
            inn_all=np.r_[inn_all,inn]
        
    # remove the missing pings    
    #data_x=np.delete(data_x,(ixx_all),axis=0)
    #data_y=np.delete(data_y,(iyy_all),axis=0)
    #data_n=np.delete(data_n,(inn_all),axis=0)



    test_1=np.array_equal(data_x[:,:2],data_n[:,:2])
    test_2=np.array_equal(data_x[:,:2],data_y[:,:2])
    
    ping_id=np.zeros((256*id_nxy.shape[0]))
    k=0
    for i in range(0,id_nxy.shape[0]):
        if (i>0) & (id_nxy[i,0]!=id_nxy[i-1,0]):
            k=k+1
        ping_id[i*256:(i+1)*256]=k
        
        

    if (test_1==True) & (test_2==True):
        print('Success')
#        s_PingCount=np.unique(data_x[:,0])
#        data=pd.DataFrame({'Ping_ID':ping_id,'Ping':data_x[:,0],'SN':data_x[:,1],'Longitude':data_x[:,2],'Latitude':data_x[:,3],'Height':data_x[:,4],
#                           'Depth':data_x[:,5],'BS':data_x[:,6],'Frequency':data_n[:,2],'Beam':data_x[:,7],'Transducer_depth':data_x[:,8],'Heading':data_x[:,9],
#                           'Sonar_Speed':data_n[:,3],'Pulse_length':data_n[:,4],'Nomimal_point_angle':data_n[:,5],'Absrorption':data_n[:,6],'Range_to_normal_incidence':(data_n[:,3]*data_y[:,3])/2, 'Range':data_n[:,7],
#                           'Number of Raw Pixels':data_y[:,4]
#                           
#                           })

        
        
        header=['Ping_ID','Ping','SN','Longitude','Latitude','Height',
                           'Depth','BS','Frequency','Beam','Transducer_depth','Heading',
                           'Sonar_Speed','Pulse_length','Nomimal_point_angle','Absrorption','Range_to_normal_incidence','Range',
                           'Number of Raw Pixels']
        data=np.c_[ping_id,data_x[:,0],data_x[:,1],data_x[:,2],data_x[:,3],data_x[:,4],
                           data_x[:,5],data_x[:,6],data_n[:,2],data_x[:,7],data_x[:,8],data_x[:,9],
                           data_n[:,3],data_n[:,4],data_n[:,5],data_n[:,6],(data_n[:,3]*data_y[:,3])/2,data_n[:,7], data_y[:,4]
                           
                           ]
        # force gc, probably not working
        data_x=np.zeros((0,))
        data_y=np.zeros((0,))
        data_n=np.zeros((0,))
    
    else:
        print('Error')
    
    
    
    print("Read Duration: %.3f seconds, pingCount %d" % (time.time() - start_time, np.max(pingCount))) # print the processing time. It is handy to keep an eye on processing performance.
    gc.collect()
    return data,header

