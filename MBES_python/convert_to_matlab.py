# -*- coding: utf-8 -*-
"""
Created on Mon Aug 19 14:39:56 2019

@author: karaouli
"""

import numpy as np
from scipy.io import loadmat, savemat
import glob
from printProgressBar import printProgressBar
import matplotlib.pyplot as plt


theta_st = np.r_[
    np.arange(64, 54 - 1, -1),
    np.arange(53, 45 - 1, -2),
    np.arange(43, 30 - 1, -3),
    np.arange(27, 3 - 1, -4),
]

datapath_out = "C:/Users/leentvaa/OneDrive - Stichting Deltares/Documents/testmbes/"

yt = glob.glob(
    r"C:/Users/leentvaa/OneDrive - Stichting Deltares/Documents/testmbes/mean/*.mat"
)

print(yt)
for i in range(0, len(yt), 2):
    #    print(yt[i])
    printProgressBar(i, len(yt), prefix="Merging:", suffix="Complete", length=50)

    tmp_1 = loadmat(yt[i])["data"]
    tmp_2 = loadmat(yt[i + 1])["data"]

    if i == 0:
        side_1 = tmp_1
        side_2 = tmp_2
    else:
        side_1 = np.r_[side_1, tmp_1]
        side_2 = np.r_[side_2, tmp_2]
#    plt.scatter(tmp_1[:,1],tmp_1[:,2])
#    plt.scatter(tmp_2[:,1],tmp_2[:,2])

print("ákdsjfjfd", np.shape(side_1))
for i in range(0, theta_st.shape[0]):
    print("hoi", side_1)
    ix = np.where(side_1[:, 0] == i)[0]
    savemat(
        datapath_out + "angle\\" + "MAPmean_1_%d.mat" % theta_st[i],
        mdict={"data": side_1[ix, :]},
    )

    ix = np.where(side_2[:, 0] == i)[0]
    savemat(
        datapath_out + "angle\\" + "MAPmean_2_%d.mat" % theta_st[i],
        mdict={"data": side_2[ix, :]},
    )
