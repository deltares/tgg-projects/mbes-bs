# -*- coding: utf-8 -*-
"""
Created on Fri Mar  1 13:07:09 2019

@author: karaouli
"""

# -*- coding: utf-8 -*-
"""
Created on Wed Feb 13 16:15:55 2019

@author: karaouli
"""

#Applying spline filter for removing bad bathymetry soundings
  
# 1D-spline
from scipy import interpolate
from scipy.signal import savgol_filter
import pandas as pd
import numpy as np
import matplotlib.pyplot as plt
from printProgressBar import printProgressBar




def SplineFilter_fast(data,wF,flag2):
     
    num_pings=data.shape[0]/256
    if np.equal(np.mod(num_pings, 1), 0):
        print('Pings before Sline are ok:')
    else:
        print('!!!!Pings before Sline are not ok:')
    
   
    
    if flag2==2:
        
        for i in range(0,np.int32(num_pings)):
            
#            print('Spline progress %.3f %%'%(100*i/pings.shape[0]))
            printProgressBar(i, num_pings, prefix = 'Spline Filter Progress:', suffix = 'Complete', length = 50)

            z=data[i*256:(i+1)*256,6]

            

            
            # for i in range(83,np.int32(num_pings)):
            #     xx=data[i*256:(i+1)*256,19]
            #     yy=data[i*256:(i+1)*256,20]
            #     plt.scatter(xx,yy)
            #     plt.title('%d'%i)
            #     plt.pause(0.1)
            
            # do something with nan
            i1=np.where(np.isfinite(z))[0]
            z_new=np.nan*np.ones((256,))
            if (i1.shape[0]>0) &(i1.shape[0]>5):
                z_new[i1]=savgol_filter(z[i1],5,2)
            
            diff=np.abs(z-z_new)
            std_dif=np.nanstd(diff)
            idx=np.where(diff>1.2*wF*std_dif)[0]
            
            
            # fid x,y dis
            if (np.max(data[i*256:(i+1)*256,19])- np.min(data[i*256:(i+1)*256,19]))>(np.max(data[i*256:(i+1)*256,20])- np.min(data[i*256:(i+1)*256,20])):
                dis=data[i*256:(i+1)*256,19]
            else:
                dis=data[i*256:(i+1)*256,20]
            

            # find outliers
            center=np.nanmedian(dis)
            var=np.abs(dis-center)
            st=np.nanstd(var)
            # dis2=dis[1:]-dis[:-1]
            # idx2=np.where(np.abs(dis2)>2)[0]+1
            idx2=np.where(var>5*st)[0]
            
            
#            plt.plot(dis,z)
#            plt.plot(dis,z_new)
#            plt.plot(dis[idx],z[idx],'x')
            idx=np.unique(np.r_[idx,idx2])

            idx=idx+i*256
            # idx2=idx2+i*256
            
                       
            if i==0:                
                id_out=idx

            else:
                id_out=np.r_[id_out,idx]      


        
        
        
        
    
    
            
    #2D interpolation        
    elif flag2==1:
        # basically, since I keep all the pings, reshape matrix and into 2*256 X pings.
        # Maybe in the future
        a=1

        
    return  id_out 