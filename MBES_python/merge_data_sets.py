# -*- coding: utf-8 -*-
"""
Created on Wed Apr 22 14:13:33 2020

@author: karaouli
"""

import numpy as np
import matplotlib.pyplot as plt
import h5py
import glob


import sys
sys.path.insert(0, r'DD:\karaouli\Desktop\Projects\ff5\src')

from gridder import Geo_Gridder

def sum_nan_arrays_v2(a,b):
    ma = np.isnan(a)
    mb = np.isnan(b)
    m_keep_a = ~ma & mb
    m_keep_b = ma & ~mb
    out = a + b
    out[m_keep_a] = a[m_keep_a]
    out[m_keep_b] = b[m_keep_b]
    return out



# 15 18 48 51 67 bad for 2017
yt=glob.glob(r'.\2017\*elev*.npy')

elev=yt[0:-1:2]
count=yt[1:-1:2]
count.append(yt[-1])


for i in range(0,len(elev)):
    e1=np.load(elev[i])
    p1=np.load(count[i])
    p1=p1.T
    plt.clf()
    plt.imshow(e1,cmap='gist_rainbow',vmin=16,vmax=24)
    plt.savefig('l%d.png'%i)
    if i==0:
        elev_m=e1
        count_elev=p1
        # bs=int4.bs
        # count_bs=int4.count
    else:   
        out=sum_nan_arrays_v2(elev_m*count_elev,e1*p1)
        elev_m=out/(count_elev+p1)
        count_elev=count_elev+p1

        # out=sum_nan_arrays_v2(bs*count_bs,int4.bs*int4.count)

        # bs=( out )/(count_bs+int4.count)
        # count_bs=count_bs+int4.count        
    





for i in range(0,len(elev)):
    e1=np.load(elev[i])
    p1=np.load(count[i])
    p1=p1.T
    # plt.clf()
    plt.imshow(e1,cmap='gist_rainbow',vmin=16,vmax=24)
    # plt.savefig('l%d.png'%i)
    if i==0:
        elev_m=e1
        count_elev=p1
        # bs=int4.bs
        # count_bs=int4.count
    else:   
        out=sum_nan_arrays_v2(elev_m*count_elev,e1*p1)
        elev_m=out/(count_elev+p1)
        count_elev=count_elev+p1

        # out=sum_nan_arrays_v2(bs*count_bs,int4.bs*int4.count)

        # bs=( out )/(count_bs+int4.count)
        # count_bs=count_bs+int4.count        
    # plt.imshow(elev_m,cmap='gist_rainbow',vmin=16,vmax=24) 
    ix=np.where(np.isfinite(e1))
    xx=np.nanmean(ix[0])
    yy=np.nanmean(ix[1])
    plt.text(yy,xx,'%d'%i)
        
        
# e1=np.load(elev[0])
# e2=np.load(elev[11])
# e3=np.load(elev[55])

# p1=np.load(count[0])
# p2=np.load(count[11])
# p3=np.load(count[55])

# p1=p1.T
# p2=p2.T
# p3=p3.T

# plt.subplot(1,2,1)
# plt.imshow(e1)
# plt.colorbar()

# plt.subplot(1,2,2)
# plt.imshow(e2)
# plt.colorbar()




# plt.imshow(e1,vmin=20.5,vmax=23.5)
# plt.imshow(e2,vmin=20.5,vmax=23.5)
# plt.imshow(e3,vmin=20.5,vmax=23.5)


# out=sum_nan_arrays_v2(e1*p1,e2*p2)
# bs2=out/(p1+p2) 

# plt.imshow(bs2,vmin=20.5,vmax=23.5)
