# MBES-BS
Code for processing multibeam echosounder backscatter data. 

Workflow:
Run processing_6.py
Run convert_to_matlab.py
Run classification.py
Run percentages.py

Notes:
It runs, thats all for now. 
To do's:
 - The output folder should be structured still, you need to define it now in every script on a random position. 
 - Find out how the output should be interpreted. 
  - Clean up the code. 

# Environment
Please use a clean environment, you can clone the environment:
1. Download mbes_env.txt
2. activate your (clean) environment
3. conda install --file mbes_env.txt

# P-drive
The codes that matter (for now) are in the GitLab environment. There is a lot more code and data on the external drive. And for backscatter it is temporarily in P:\430-tgg-data-7\MBES_code_Marios_temp\backscattered. 


